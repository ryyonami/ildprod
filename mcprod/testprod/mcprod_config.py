#
# Config file for ILD MCProduction
#
# Parameters constant to whole set is defined here
# 
# Parameters for the production with v02-02 

MCProd_Sim_Version = "ILCSoft-02-02_cc7"
MCProd_Sim_ILDConfig = "v02-02"
MCProd_Sim_Steering = "ddsim_steer.py"
MCProd_Rec_Version = "ILCSoft-02-02_cc7"
MCProd_Rec_ILDConfig = "v02-02"
MCProd_OverlayInput_ILDConfig = "v02-02"

# Required for DSTM job
MCProd_init_ilcsoft = os.environ["ILCSOFT_INIT_SCRIPT"]

# Default output directory
MCProd_MCOpt_DirTape = "mc-2020"
MCProd_MCOpt_DirDisk = "mc-2020"

# MCProd_OverlayInput_BasePath = "/ilc/prod/ilc/%s/ild/sim" % MCProd_MCOpt_DirTape
MCProd_OverlayInput_BasePath = "/ilc/prod/ilc/mc-2020/ild/sim"


# Machine dependant IP smearing/offset parameters
# See ~/ILDProd/gensamples/20180627-aalowpt/beam_conditions.txt ( a mail from Mikael )
# 
MCProd_ZOffset_And_ZSigma = {}
# 500-TDR_ws
Z0_SZ = { "WW":[0.0, 0.1968], "BB":[0.0, 0.16988], 
           "WB":[-0.04222, 0.186], "BW":[0.00422, 0.186] }

MCProd_ZOffset_And_ZSigma["500-TDR_ws"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 1000-B1b_ws parameters
Z0_SZ = { "WW":[0.0, 0.1468], "BB":[0.0, 0.1260], 
           "WB":[-0.0352, 0.1389], "BW":[0.0357, 0.1394] }

MCProd_ZOffset_And_ZSigma["1000-B1b_ws"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 250-SetA parameters
Z0_SZ = { "WW":[0.0, 0.202], "BB":[0.0, 0.171], 
           "WB":[-0.0389, 0.191], "BW":[0.0380, 0.191] }

MCProd_ZOffset_And_ZSigma["250-SetA"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 91-nobeam parameters ( uses values same as 250-SetA )
MCProd_ZOffset_And_ZSigma["91-nobeam"] = MCProd_ZOffset_And_ZSigma["250-SetA"]


###################################################################################
# Data for background overlay
MCProd_Overlay_Data = {}
# For re-created backgrounds on 31-Jan-2021, 3 times statistics than before.
MCProd_Overlay_Data["250-SetA"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.1256495436,  "ProdID": 15330 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.297459204,   "ProdID": 15325 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.29722665,    "ProdID": 15324 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.829787658,   "ProdID": 15323 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,           "ProdID": 15322 } } }

MCProd_Overlay_Data["91-nobeam"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.1256495436,  "ProdID": 15330, "EMpara":"250-SetA"},
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.297459204,   "ProdID": 15325, "EMpara":"250-SetA"},
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.29722665,    "ProdID": 15324, "EMpara":"250-SetA"},
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.829787658,   "ProdID": 15323, "EMpara":"250-SetA"},
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,           "ProdID": 15322, "EMpara":"250-SetA"} } }

# Background files for the production with v02-02 till 30-Jan-2021
#MCProd_Overlay_Data["250-SetA"] = { 
#  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.1256495436,  "ProdID": 14947 },
#                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.297459204,   "ProdID": 14946 },
#                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.29722665,    "ProdID": 14941 },
#                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.829787658,   "ProdID": 14940 },
#                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,           "ProdID": 14939 } } }

#MCProd_Overlay_Data["91-nobeam"] = { 
#  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.1256495436,  "ProdID": 14947, "EMpara":"250-SetA"},
#                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.297459204,   "ProdID": 14946, "EMpara":"250-SetA"},
#                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.29722665,    "ProdID": 14941, "EMpara":"250-SetA"},
#                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.829787658,   "ProdID": 14940, "EMpara":"250-SetA"},
#                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,           "ProdID": 14939, "EMpara":"250-SetA"} } }
#


MCProd_Overlay_Data["500-TDR_ws"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 10237 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 10241 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 10239 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 10235 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 10233 } },
  "ILD_s5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 10238 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 10242 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 10240 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 10236 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 10234 } } }

MCProd_Overlay_Data["1000-B1b_ws"] = {
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.526,   "ProdID":  10737 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.634,   "ProdID":  10733 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.634,   "ProdID":  10729 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.744,   "ProdID":  10709 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID":  10707 } },
  "ILD_s5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.526,   "ProdID":  10738 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.634,   "ProdID":  10734 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.634,   "ProdID":  10730 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.744,   "ProdID":  10710 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID":  10708 } } }

