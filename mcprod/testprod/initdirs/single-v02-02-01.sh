#!/bin/bash 
#
#  Create scripts for the production of calibration samples.
#

prodname="calib-v02-02-01"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################

cat > ${prodname}-list.txt <<EOF
single.b00
EOF
# uds.b00.0lep

export GENSPLIT_DEFAULT_NPROCS=6

# banned_sites="LCG.UKI-SOUTHGRID-RALPP.uk,LCG.Cracow.pl,LCG.DESYZN.de,LCG.Glasgow.uk,LCG.UKI-LT2-IC-HEP.uk"
banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk"
excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/single.xlsx"

cmd="init_production.py --workdir ${proddir} --excel_file ${excel_file} \
  --prodlist ${prodname}-list.txt \
  --sim_models "ILD_l5_v02" \
  --rec_options o1 \
  --ngenfile_max 1 --split_nbfiles 10 \
  --test --se_for_data KEK-DISK --se_for_gensplit KEK-DISK \
  --se_for_dstm_replicates DESY-SRM \
  --sim_banned_sites ${banned_sites} \
  --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 20 --rec_nbtasks 20 \
  --mcprod_config "mcprod-ilcsoft-v02-02-01.py" \
  --production_type sim:nobg "

echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}


################################################################################
# init_production.py help # See init_production.help
################################################################################
