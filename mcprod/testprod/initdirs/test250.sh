#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="250-SetA-P2"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi


# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
testname="test2"
cat > ${prodname}-list.txt <<EOF
testprod.bWW.1lep:_${testname}/I110205,I110206
EOF

# ( cd ${proddir} && rm -rfv *.bWW.[01]lep_${testname} )

export GENSPLIT_DEFAULT_NPROCS=3

# --noelog
#  --reuse_gensplit 500590 
#  --noelog --reuse_gensplit 500590 \


# %% are converted to -- in task_step4_main.sh  
addopt=" --nodry "
addopt+=" --EnergyParametersFile Config/ParametersUnknownGeV.xml "
addopt+=" --BeamCalBackgroundFile HighLevelReco/BeamCalBackground/BeamCalBackground-E500-B3.5-RealisticNominalAntiDid.root "

cmd="init_production.py --workdir ${proddir} --excel_file prodpara-multitest.xlsx \
  --prodlist ${prodname}-list.txt \
  --test --multi_dir --se_for_data KEK-SRM --se_for_gensplit KEK-SRM --se_for_logfiles KEK-SRM \
  --se_for_dstm_replicates DESY-SRM \
  --dstonly \
  --genmeta_json /home/ilc/miyamoto/ILDProd/gensamples/20190704-genwhiz/genmetaByID.json \
  --nw_perfile 100 --ngenfile_max 2 --split_nbfiles 4 \
  --sim_nbtasks 5 --rec_nbtasks 5 \
  --production_type sim:nobg \
  --step4_options ${addopt} " 

#
# task_step4_main.sh -r o1 --options ' --nodry --DSTonly --SimSelectedFile 2 
#  ' --ptype nobg


echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
