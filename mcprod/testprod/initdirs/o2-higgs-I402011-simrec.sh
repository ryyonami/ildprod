#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="o2-higgs"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
#testname="250-SetA-lowX"
cat > ${prodname}-list.txt <<EOF
higgs_ffh.bWW:_dbg/I402011
EOF


export GENSPLIT_DEFAULT_NPROCS=5

# --noelog
# simprodjson="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/ProdTask/250higgs-A/higgs_ffh.bWW_B/4_subprod/simprod_15094_l5_v02.json"
# addopts=" --SimSelectedFile 5 --nodry --noPrompt "

cmd="init_production.py --workdir ${proddir} \
  --noelog \
  --excel_file  /home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-higgs-B.xlsx \
  --prodlist ${prodname}-list.txt \
  --dstonly --recrate 0.10 \
  --test --se_for_data KEK-DISK --se_for_gensplit KEK-DISK \
  --se_for_logfiles KEK-DISK \
  --split_nseq_from 0 \
  --rec_options o2 \
  --ngenfile_max 1 --split_nbfiles 100 \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 100 --rec_nbtasks 100 \
  --production_type sim:ovl "

#
echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help 
################################################################################
# Config file /home/ilc/miyamoto/ILDProd/mcprod/testprod/mcprod_config.py was executed.
# usage: init_production.py [-h] [-d SPLITDIR] [--excel_file EXCELFILE]
#                           [--genfile_list GENFILE_LIST] [--workdir WORKDIR]
#                           [--prodlist PRODLIST]
#                           [--split_nbfiles SPLIT_NBFILES]
#                           [--ngenfile_max NGENFILE_MAX] [--test]
#                           [--nw_perfile NW_PERFILE]
#                           [--production_type PROD_TYPE]
#                           [--se_for_data SE_FOR_DATA]
#                           [--elogid_old ELOGID_OLD]
#                           [--reuse_gensplit REUSE_GENSPLIT]
#                           [--sim_nbtasks SIM_NBTASKS]
#                           [--rec_nbtasks REC_NBTASKS]
#                           [--sim_banned_sites SIM_BANNED_SITES]
#                           [--rec_banned_sites REC_BANNED_SITES]
#                           [--gen_data_type GEN_DATA_TYPE]
# 
# Generate subdirectories and scripts to do production
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   -d SPLITDIR, --splitdir SPLITDIR
#                         dir names where splitted generator files are written.
#   --excel_file EXCELFILE
#                         Excel file name, where production information is
#                         obtained.
#   --genfile_list GENFILE_LIST
#                         File name of generator file names
#   --workdir WORKDIR     A directory where production set scripts are written
#   --prodlist PRODLIST   A file containing a list of production groupds.
#   --split_nbfiles SPLIT_NBFILES
#                         Number of splitted files to be produced.
#   --ngenfile_max NGENFILE_MAX
#                         Max number of generator file to use.
#   --test                Scripts for test are generated.
#   --nw_perfile NW_PERFILE
#                         number of events per file in gensplit.
#   --production_type PROD_TYPE
#                         Production type, sim, ovl, nobg. Default is sim:ovl
#   --se_for_data SE_FOR_DATA
#                         Output SE for Sim, Rec, DST, DSTMerged files. Log
#                         files are DESY-SRM
#   --elogid_old ELOGID_OLD
#                         Elog ID referenced, where past production information
#                         is obtained
#   --reuse_gensplit REUSE_GENSPLIT
#                         Use this gensplit_id for new simproduction
#   --sim_nbtasks SIM_NBTASKS
#                         Number of tasks of simulaton production
#   --rec_nbtasks REC_NBTASKS
#                         Number of tasks of reconstruction production
#   --sim_banned_sites SIM_BANNED_SITES
#                         Banned sites for simulation production
#   --rec_banned_sites REC_BANNED_SITES
#                         Banned sites for reconstruction
#   --gen_data_type GEN_DATA_TYPE
#                         generator data type, either stdhep or pairs
