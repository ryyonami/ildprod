#!/bin/bash 
#
#  Create scripts for background files.
#


# ###########################################################
# Create production scripts
# Usage:
#   create_scripts [norder] [nstep] [proddir]
# Argeuments:
#   [norder] : Production sequence number
#   [nstep]  : Number of input files processed per production
#   [proddir] : Directories of production
# ###########################################################
create_scripts(){
  norder=$1
  nstep=$2
  nseq_from=$[(${norder}-1)*${nstep}]
  
  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
2f_Z_hadronic.bWW:_I500010_${norder}/I500010
EOF
  # I500010: ngenfile=6399 for 5000 fb-1
  # I500012: ngenfile=3521 for 5000 fb-1

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-2f.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"
  export GENSPLIT_DEFAULT_NPROCS=7
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir} \
    --excel_file ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --ngenfile_max ${nstep} \
    --delfiles gensplit,sim,dst \
    --dstonly --recrate -1 \
    --test --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
    --nw_perfile 10 --split_nbfiles 10 \
    --split_nseq_from ${nseq_from} --ngenfile_max 1 \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}
}


########################################################################
prodname="SM-2fB"
proddir=${PRODTASKDIR}/${prodname}
if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

for i in `seq 3 10` ; do 
  create_scripts ${i} 128 ${proddir}
done

