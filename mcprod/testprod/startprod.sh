#!/bin/bash 

# ############################################################3
# stdecho
# ############################################################3
stdecho()
{
  message=$1
  pre=$2
  [ "x${pre}" == "x" ] && pre="+++"
  dstr=`date +%Y%m%d-%H%M%S`
  echo "${pre} ${dstr} : ${message}"
}

# ############################################################3
# Check DIRAC Proxy
# ############################################################3
check_proxy()
{
  echo "**** Checking proxy info..."
  proxy_register=`dirac-proxy-info | grep "VOMS"`
  if [ "x${proxy_register}" == "x" ]; then
    echo "**** dirac-proxy-init is not initiated."
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    exit
  fi

  proxy_group=`dirac-proxy-info | grep "DIRAC group" | cut -d ':' -f 2 | sed s/\ //g`
  if [ "x${proxy_group}" == "xilc_prod" ]; then
    echo "**** Production role ${proxy_group} is selected."
  else
    echo "**** Selected role is ${proxy_group}"
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    echo "Exit"
    exit
  fi
}


# ############################################################
# Production of one directory, 1_preprod, 3_gensplit and 4_subprod
# ############################################################
do_onedir(){
  rdir=$1
  nsleep=$2
  par_dir=$3
  ( 
    cd ${rdir}
    stdecho "current directory : `pwd`, `hostname`"
    ( cd 1_prepProd  &&  . run_step1.cmd &&  stdecho "task in 1_preProd completed" "###"  )
    ( 
      stdecho "Sleeping ${nsleep} before starting task_step4.sh" 
      sleep ${nsleep}
      stdecho "Wakeup from sleep. task_step4.sh will be executed."
      cd 4_subprod 
      ./task_step4.sh > run.log 2>&1 
      stdecho "task_step4.sh has completed. New transformations begin."
    ) & 
    stdecho "move to 3_gensplit and start splitting."
    ( cd 3_gensplit &&  task_step3.sh > run-task_step3.log 2>&1 )
    stdecho "3_gensplit in ${rdir} has completed."
  )

}

############################################
# is_there_space_for_sim
############################################
is_there_space_for_sim()
{
  jsonnext="`head -1 startprod.list`/1_prepProd/production.json"
  # echo ${jsonnext}
  if [ -e ${jsonnext} ] ; then
     simbase=`jsonread ${jsonnext} __DIRECTORY_SIM`
     simdir=`echo ${simbase}/sim | sed -e "s|//|/|g"`
     simse=`jsonread ${jsonnext} __SIM_SE`
     
     echo "size -l ${simdir}" | dirac-dms-filecatalog-cli | grep ${simse} > temp.txt
     val=`cat temp.txt | while read a b c d ; do ( echo $c ) ; done | sed -e "s/,//g"`
     if [ "x${val}" == "x" ] ; then 
       stdecho "Failed to get storage space of SIM SE ${simse}. Wrong SE name ?"
       return 16
     fi
     giga_byte=$[${val}/1000000000]
     giga_limit=`grep ${simse/-/} startprod.opt | cut -d"=" -f2`
  
     if [ ${giga_byte} -lt ${giga_limit} ] ; then 
         stdecho "available : ${simse} ${giga_byte} GB within limit of ${giga_limit} GB "
         return 0
     else
         stdecho "no space : ${simse} ${giga_byte} GB excedds limit of ${giga_limit} GB "
         return 4
     fi      
  fi
  stdecho "Not exist ${jsonnext}"
  return 16
}

# ###############################################################
#  Wait next production be ready
# wait_next_be_ready 
# parameters are taken from startprod.opt
# ###############################################################
wait_next_be_ready()
{
  source startprod.opt
  stdecho "`hostname` : Checking status of active production : ${max_job_insys} ${max_sim_trans} ${sleep_before_next_loop}"
  print_transformation_status.sh 2>/dev/null | tee transformation_status.log
  jobinsys=`grep total_jobs_insys transformation_status.log | cut -d"=" -f2`
  nsimtrans=`grep -v "#" prodids.list 2>/dev/null | grep simprod | wc -l`
  stdecho "${jobinsys} jobs in transformationis.  There are ${nsimtrans} SIM transformations."
  flag=1
  [ ${jobinsys} -gt ${max_job_insys} ] && flag=0
  [ ${nsimtrans} -ge ${max_sim_trans} ] && flag=0
  is_there_space_for_sim
  rc=$?
  [ ${rc} -eq 16 ] && return 16
  [ ${rc} -ne 0 ] && flag=0

  while [ ${flag} -eq 0 ] ; do
    source startprod.opt
    stdecho "`hostname` : Sleep ${sleep_before_next_loop}. ProcessID=$$"
    sleep ${sleep_before_next_loop}
    stdecho "Initializing DIRAC Proxy with production role" 
    . ${MYPROD_PRODPROD} > init_production.log 2>&1 
    source startprod.opt
    stdecho "`hostname` : Checking status of active production : ${max_job_insys} ${max_sim_trans} ${sleep_before_next_loop}"
    print_transformation_status.sh | tee transformation_status.log
    jobinsys=`grep total_jobs_insys transformation_status.log | cut -d"=" -f2`
    nsimtrans=`grep -v "#" prodids.list 2>/dev/null | grep simprod | wc -l`
    stdecho "${jobinsys} jobs in transformationis.  There are ${nsimtrans} SIM transformations."
    flag=1
    [ ${jobinsys} -gt ${max_job_insys} ] && flag=0
    [ ${nsimtrans} -ge ${max_sim_trans} ] && flag=0
    is_there_space_for_sim
    rc=$?
    [ ${rc} -eq 16 ] && return 16
    [ ${rc} -ne 0 ] && flag=0

  done
  return 0   
}

# ################################################################
# Main part
# ################################################################

max_job_insys=2000
max_sim_trans=3
sleep_before_next_loop=30m

dirlist="startprod.list"
waitmin="10m"
myhost=`hostname`
dstr=`date +%Y%m%d-%H%M%S`
parent_dir=`pwd`

inargs=$1

if [ "x${inargs}" == "x-h" ] ; then 
  echo "./startprod.sh [opt] "
  echo "initiates a sequence of production defined in startprod.list."
  echo "Options"
  echo "  (no argument) : Prompt before start sequence."
  echo "  -h   : print this help"
  echo " "
  exit
fi



if [ `cat ${dirlist} | wc -l` -eq 0 ]; then
  echo "**** Need to edit ${dirlist} to initiate production"
  echo "Exit"
  exit
elif [ "x${inargs}" == "x" ] ; then 
  echo "**** Do you realy start following production?"
  list=`cat ${dirlist}`
  cat ${dirlist}
  echo -n "Continue with these production [y/n]: "
  read ANS

  case $ANS in
    [Yy]* ) echo "**** Start production"; echo ${list} ;;
    * ) echo "**** Stopped" ; exit ;;
  esac
fi

if [ -e startprod.active ] ; then 
  myhost=`hostname`
  dstr=`date +%Y%m%d-%H%M%S`
  echo "### ${dstr} at ${myhost} : startprod.sh is executed as startprod.active exist. " 
  cat startprod.active 
  exit
fi

myecho "startprod.sh is active at `hostname` with PID=$$" > startprod.active
myecho "checking proxy and waiting next be ready " >> startprod.active

check_proxy

wait_next_be_ready


while [ `cat ${dirlist} | wc -l` -ne 0 ] ; do 
  headline=`head -1 ${dirlist}`
  tail -n +2 ${dirlist} > temp-${dirlist}
  mv temp-${dirlist} ${dirlist}
  stdecho "Start ${headline} production at ${myhost} +++" "+++++"
  topdir=`dirname ${headline}`
  wdir=`basename ${headline}`
  dstr=`date +%Y%m%d-%H%M%S`
  echo ${headline} >> ${parent_dir}/startprod.done
  myecho "startprod.sh is active at `hostname` with PID=$$" > startprod.active
  myecho "current directory is ${headline} " >> startprod.active
  ( 
    cd ${topdir}
    stdecho "chdir to ${topdir} +++"
    . ${MYPROD_PRODPROD} 
    do_onedir ${wdir} ${waitmin} ${parent_dir}

  ) 
  stdecho "returned from do_onedir. Now at `pwd` +++" "+++++"

  if [ -e startprod.exit ] ; then
      stdecho "quit the loop of startprod.list by startprod.exit"
      break
  fi 
  while [ -e sleep.cmd ] ; do
      stdecho "found sleep.cmd and will be executed"
      . sleep.cmd
  done
  myecho "startprod.sh is active at `hostname` with PID=$$" > startprod.active
  myecho "waiting next be ready " >> startprod.active

  stdecho "Checking readiness of next production +++"
  wait_next_be_ready 
  if [ $? != 0 ] ; then 
    stdecho "Failed while waiting next production be ready +++"
    break
  fi

  stdecho "Completed wait_next_be_ready. Moving to next production in the queue +++"

done

rm -f startprod.active
stdecho "startprod.active was removed."
stdecho "No more entry in ${dirlist} or quit loop requested."
if [ -e post_exec.sh ] ; then 
  post_exec.sh > post_exec.log 2>&1 &
fi 

stdecho "startprod.sh reached the end of the script."
