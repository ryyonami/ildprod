#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="aa_lowpt"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi


# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
testname="aa_lowpt"
cat > ${prodname}-list.txt <<EOF
aa_lowpt.bBB
aa_lowpt.bBW
aa_lowpt.bWB
aa_lowpt.bWW
EOF

# ( cd ${proddir} && rm -rfv *.bWW.[01]lep_${testname} )

export GENSPLIT_DEFAULT_NPROCS=3

# %% are converted to -- in task_step4_main.sh  
#addopt=" --nodry "
#addopt+=" --EnergyParametersFile Config/ParametersUnknownGeV.xml "
#addopt+=" --BeamCalBackgroundFile HighLevelReco/BeamCalBackground/BeamCalBackground-E500-B3.5-RealisticNominalAntiDid.root "
banned_sites="LCG.UKI-SOUTHGRID-RALPP.uk,LCG.Cracow.pl,LCG.UKI-LT2-IC-HEP.uk,LCG.KEK.jp"

cmd="init_production.py --workdir ${proddir} \
  --excel_file ${HOME}/ILDProd/mcprod/v03-01/initdirs/prodpara/bkg.xlsx \
  --prodlist ${prodname}-list.txt \
  --sim_models "ILD_l5_v02" \
  --sim_banned_sites ${banned_sites} \
  --se_for_gensplit KEK-DISK \
  --sim_nbtasks 100 --rec_nbtasks 100 \
  --production_type sim "


echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
