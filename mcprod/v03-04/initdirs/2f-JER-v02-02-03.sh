#!/bin/bash 
#
#  Create scripts for the production of calibration samples.
#

prodname="calib-v02-02-03"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################

cat > ${prodname}-list.txt <<EOF
2f-JER.b00
EOF
# uds.b00.0lep

export GENSPLIT_DEFAULT_NPROCS=8

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk"
prodpara_dir="/home/ilc/miyamoto/ILDProd/mcprod/v03-04/initdirs/prodpara"
prodpara="${prodpara_dir}/2f-JER-10k.xlsx"

cmd="init_production.py --workdir ${proddir} 
  --excel_file ${prodpara} \
  --prodlist ${prodname}-list.txt \
  --sim_models "ILD_l5_v02" \
  --sim_banned_sites ${banned_sites} \
  --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 100 --rec_nbtasks 100 \
  --production_type sim:nobg "

echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

#testopt="  --test --noelog \
#  --se_for_data KEK-SRM --se_for_gensplit KEK-SRM --se_for_logfiles KEK-SRM \
#  --se_for_dstm_replicates DESY-SRM \
#  --nw_perfile 20 --ngenfile_max 2 --split_nbfiles 5 "

################################################################################
# init_production.py help # See init_production.help
################################################################################
