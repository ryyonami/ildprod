#!/bin/bash 

for d in mILD* ; do 
  ( 
  cd $d
  jobid=`jsonread ddsimjob.json SubmitReturn Value`
  dirac-wms-job-get-output ${jobid}
  dirac-wms-job-delete ${jobid}
  )
done

