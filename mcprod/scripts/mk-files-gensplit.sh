#!/bin/bash 

decode_inputlist()
{
  aline=$1
  tststr=`echo ${aline} | grep ","`
  if [ "x${tststr}" == "x" ] ; then 
     fname=$1
  else 
     fname=`echo ${aline} | cut -d"," -f1` 
     outf="gensplit.opt"
     rm -f ${outf}
     echo ${aline} | cut -d"," -f2- | while read f ; do echo $f ; done >> ${outf}
  fi
  echo ${fname}
}


######################################################################
# Create a script, gensplit-upload-addmeta.sh
######################################################################
make_gensplit_script()
{
    genline=$1

    filename=`decode_inputlist ${genline}`
    fileinfo=`echo ${genline} | cut -d"," -f1`
    filename=`basename ${fileinfo}`
    procid=`getMetaFromFilename ${filename} I`
    nser=`getMetaFromFilename ${filename} n`

    d="I${procid}.${nser}"

    mkdir -v $d 
    ( 
      cd $d 
      if [ -e "../gensplit.opt" ] ; then mv -v ../gensplit.opt . ; fi 
    )
}

######################################################################
# Run with multidir option
######################################################################
make_gensplit_multidir()
{
    genline=$1

    filename=`decode_inputlist ${genline}`
    fileinfo=`echo ${genline} | cut -d"," -f1`
    filename=`basename ${fileinfo}`
    dirname=`dirname ${fileinfo}`	

    procid=`getMetaFromFilename ${filename} I`
    nser=`getMetaFromFilename ${filename} n`

    d="I${procid}.${nser}"

    mkdir -v $d
    (
      cd $d
      if [ -e "../gensplit.opt" ] ; then mv -v ../gensplit.opt . ; fi
    )
}



# #####################################################
#  Main part of this script
# #####################################################
multidir=$1


cat ../1_prepProd/input_genfiles.list | 
  while read genline ; do ( 
    if [ "x${multidir}" == "x" ] ; then 
       make_gensplit_script ${genline}
    else
       make_gensplit_multidir ${genline}
    fi
  )
done
