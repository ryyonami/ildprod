#!/bin/bash
#
# A script for step4 tasks, submit production
#

simmodels=""
recoptions=""
ptype=""
# options=" --nodry --noPrompt "

options=" --dry "
# options=" --dry --devel --noPrompt "
# options=" --NewPath=/ilc/prod/ilc/ild/test --OptionFile testoptions.json "
noelog=0
dstonly=0
nsub=-1
selected_file_recdst=2
selected_file_dstonly=3

stfile="status.log"
echo "### subprod started" 2>&1 | tee ${stfile}
( LANG=C && date && hostname ) 2>&1 | tee -a ${stfile}
echo "### input arguments were " 2>&1 | tee -a ${stfile}
echo $* 2>&1 | tee -a ${stfile}

if [ -f task_step4.opt ] ; then 
  . task_step4.opt
  echo "### task_step4.opt file presents " 2>&1 | tee -a ${stfile}
  cat task_step4.opt 2>&1 | tee -a ${stfile}
fi

while [ $# -ne 0 ] 
do
  case "$1" in 
    -s) shift ; simmodels=$1 ;;
    -r) shift ; recoptions=$1 ;;
    -N) shift ; nsub=$1 ;;
    --selected_file_recdst) shift  ; selected_file_recdst=$1 ;;
    --selected_file_dstonly) shift  ; selected_file_dstonly=$1 ;;
    --ptype) shift ; ptype=$1 ;;
    --options) shift ; options=`echo $1 | sed -e "s| %%| --|g"` ;;
    --noelog)  noelog=1 ;;
    --dstonly) dstonly=1 ;;
    *) echo "ERROR : Undefined option, $1, is given."
       exit
  esac
  shift
done

noelog_chk=`jsonread production.json __ELOG_NOELOG 2>/dev/null`
recrate=`jsonread production.json __RECRATE 2>/dev/null`
[ "x${noelog_chk}" == "xTRUE" ] && noelog=1

# ##################### Create simulation transformation

if [ `echo ${ptype} | grep -c "sim"` -ne 0 ] ; then 
  python ILDProdBase.py --submitType sim --simModels ${simmodels} ${options} 2>&1 | tee submit-sim-$$.log
  if [ ${noelog} == 0 ] ; then 
      putelog.py subprod --prodlog submit-sim-$$.log 2>&1 | tee -a ${stfile}
      outfile=`grep /ilc/prod/ilc submit-sim-$$.log | grep "searchable" | cut -d":" -f2 | sed -e "s| ||g"`
      elogid=`jsonread production.json __ELOG_ID`
      putelog.py text -i ${elogid} -m "SIM files are stored at ${outfile}"
      delsim=`jsonread production.json __DELFILES 2>/dev/null | grep sim`
      if [ "x${delsim}" != "x" ] ; then 
        simse=`jsonread production.json __SIM_SE 2>/dev/null`
        putelog.py text -i ${elogid} -m "SIM files are written to ${simse}, but remove after DST-Merge"
      fi
  fi
fi

# ######### Create No-background reconstruction transformation

ovlopt=""
rectype="none"
if [ `echo ${ptype} | grep -c "nobg"` -ne 0 ] ; then 
  rectype="nobg"
elif [ `echo ${ptype} | grep -c "ovl"` -ne 0 ] ; then 
  rectype="ovl"
  ovlopt=" --doOverlay "

fi

if [ ${rectype} != "none" ] ; then
    recset="std"
    if [ ${dstonly} == 0 ] ; then 
      recset="std"
      python ILDProdBase.py --submitType ${rectype} ${ovlopt} --recOptions ${recoptions} ${options} 2>&1 | tee submit-${rectype}-$$.log
      if [ ${noelog} == 0 ] ; then 
         putelog.py subprod --prodlog submit-${rectype}-$$.log 2>&1 | tee -a ${stfile}
         elogid=`jsonread production.json __ELOG_ID`
         recfile=`grep /ilc/prod/ilc submit-${rectype}-$$.log | grep "searchable" | grep rec | cut -d":" -f2 | sed -e "s| ||g"`
         dstfile=`grep /ilc/prod/ilc submit-${rectype}-$$.log | grep "searchable" | grep dst | cut -d":" -f2 | sed -e "s| ||g"`
         putelog.py text -i ${elogid} -m "REC files are stored at ${recfile}"
         putelog.py text -i ${elogid} -m "DST files are stored at ${dstfile}"
      fi
    
    elif [ "x${recrate}" == "x-1" ] ; then 
      recset="dstalone"
      python ILDProdBase.py --submitType ${rectype} ${ovlopt} --recOptions ${recoptions} --DSTonly ${options} 2>&1 | \
             tee -a submit-${rectype}-$$.log | tee dstonly-${rectype}-$$.log
      if [ ${noelog} == 0 ] ; then 
         putelog.py subprod --prodlog submit-${rectype}-$$.log 2>&1 | tee -a ${stfile}
         elogid=`jsonread production.json __ELOG_ID`
         dst2file=`grep /ilc/prod/ilc dstonly-${rectype}-$$.log | grep "searchable" | grep dst | cut -d":" -f2 | sed -e "s| ||g"`
         putelog.py text -i ${elogid} -m "REC files are not saved.<br>DST files are stored at ${dst2file}"
      fi
    
    else 
      recset="partrec"
      python ILDProdBase.py --submitType ${rectype} ${ovlopt} --recOptions ${recoptions} \
             --SimSelectedFile ${selected_file_recdst} ${options} 2>&1 | \
             tee submit-${rectype}-$$.log | tee recdst-${rectype}-$$.log
      recdst_json=`tail -1 recdst-${rectype}-$$.log | cut -d" " -f9 | sed -e "s/\.json\./\.json/"`
      python ILDProdBase.py --submitType ${rectype} ${ovlopt} --recOptions ${recoptions} --DSTonly \
             --SimSelectedFile ${selected_file_dstonly} ${options} 2>&1 | \
             tee -a submit-${rectype}-$$.log | tee dstonly-${rectype}-$$.log
      dstonly_json=`tail -1 dstonly-${rectype}-$$.log | cut -d" " -f9 | sed -e "s/\.json\./\.json/"`
      update_recprod_json.py ${recdst_json} ${dstonly_json} \
             --selected_file_recdst ${selected_file_recdst} --selected_file_dstonly ${selected_file_dstonly}
      if [ ${noelog} == 0 ] ; then 
         putelog.py subprod --prodlog submit-${rectype}-$$.log 2>&1 | tee -a ${stfile}
         elogid=`jsonread production.json __ELOG_ID`
         recfile=`grep /ilc/prod/ilc recdst-${rectype}-$$.log | grep "searchable" | grep rec | cut -d":" -f2 | sed -e "s| ||g"`
         dst1file=`grep /ilc/prod/ilc recdst-${rectype}-$$.log | grep "searchable" | grep dst | cut -d":" -f2 | sed -e "s| ||g"`
         dst2file=`grep /ilc/prod/ilc dstonly-${rectype}-$$.log | grep "searchable" | grep dst | cut -d":" -f2 | sed -e "s| ||g"`
         putelog.py text -i ${elogid} -m "REC files are stored at ${recfile}"
         putelog.py text -i ${elogid} -m "DST files are stored at ${dst1file}<br>and ${dst2file}"
         putelog.py text -i ${elogid} -m "REC events stored vs DST events stored will be ${recrate}, but maximum 500 files."
      fi
      # setselectedfile.sh is executed only when REC files are stored partially
      if [ -e ../3_gensplit ] ; then 
        setselectedfile.sh --selected_file_recdst ${selected_file_recdst} \
                         --selected_file_dstonly ${selected_file_dstonly} >> ${stfile} 2>&1 &
      fi
    fi
    
fi


# ========================================================================

njson=`( /bin/ls [rs]*prod_*.json | grep -v 12345 ) 2>/dev/null | wc -l `
if [ "x${njson}" != "x0" ] ; then 
  add_to_prodids-list.sh | tee -a ${stfile}
fi

date 2>&1 | tee -a ${stfile}

echo "### subprod completed" 2>&1 | tee -a ${stfile}



exit


