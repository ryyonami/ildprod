#!/bin/bash
# 
# Wait completion of dst-merge jobs. 
# When all completed, download job logs and move to the next step.

# dstmstat=`grep dstm_sub.py dstm-step2.log | grep "has completed" | wc -l`
dstmstat=`grep "###### Completed submit loop of DST merge job" dstm-step2.log | wc -l`
echo "Found ${dstmstat} completed job submission loops." 2>&1 | tee -a dstm-step3-check.log

if [ ${dstmstat} -eq 0 ] ; then
  echo "No submission loop in dstm-step2.sh  completed." 2>&1 | tee -a dstm-step3-check.log
  exit 5
fi

# ####################################################

logf="dstm-step3.log"
if [ -e ${logf} ] ; then 
   dstr=`date +%Y%m%d-%H%M%S`
   mv -v ${logf} ${logf}.${dstr}
fi
# rm -f ${logf}
export LANG=C
echo "## Start dstm-step3 ( checking job completion. ) at `hostname` `date`" | tee -a ${logf}

# Output jobout 
grep "job_group" dstm-step2.log | cut -d" " -f1 | sort | uniq | cut -d"=" -f2 > jobgroup.list
ngroup=`cat jobgroup.list | wc -l`
echo "### Found ${ngroup} job groups:" | tee -a  ${logf}
cat jobgroup.list

# ### Confirm all jobs in DONE status
jobstatus="jobstatus.log"
rm -f ${jobstatus}
dstr=`date +%Y%m%d-%H%M%S`
cat jobgroup.list | while read f ; do
  echo "### ${dstr} : Getting job status of ${f} in a file, ${jobstatus} " | tee -a ${logf}
  dirac-wms-job-status -g ${f} >> ${jobstatus}
done

nstatus=`grep "Status=" ${jobstatus} | wc -l `
nstatus_done=`grep "Status=Done" ${jobstatus} | wc -l`
nstatus_failed=`grep "Status=Failed;" ${jobstatus} | wc -l`
nstatus_staging=`grep "Status=Staging;" ${jobstatus} | wc -l`
if [ "x${nstatus}" == "x0" -o "x${nstatus}" != "x${nstatus_done}" ] ; then 
  dstr=`date +%Y%m%d-%H%M%S`
  echo "### ${dstr} : not all job group completed. Submit ${nstatus} jobs, ${nstatus_done} DONE jobs. ${nstatus_failed} failed. ${nstatus_staging} staging" | tee -a ${logf}
  if [ ${nstatus_failed} -gt 0 ] ; then 
      dstr=`date +%Y%m%d-%H%M%S`
      echo "### ${dstr} : Resubmitting failed jobs with user role." | tee -a ${logf}
      source ${MYPROD_USERPROD} > /dev/null
      mkdir -p rescheduled
      ( cd rescheduled && ( [ -e sub.log ] && rm -f sub.log ) )
      grep "Status=Failed;" ${jobstatus} | cut -d" " -f1 | cut -d"=" -f2 | \
      while read f ; do  
        (  dstr=`date +%Y%m%d-%H%M%S`
          if [ -e rescheduled/$f ] ; then 
            nresch=`cat rescheduled/${f} | wc -l`
          else
            nresch=0
          fi

          if [ ${nresch} -gt 3 ] ; then 
            echo "${dstr} : DSTM job $f was rescheduled more than 3. No more rescheduling." | tee -a rescheduled/$f | tee -a ${logf}
          else
            dirac-wms-job-reschedule $f | tee -a ${logf}
            echo "${dstr} : DSTM job $f has rescheduled." | tee -a rescheduled/$f | tee -a rescheduled/sub.log | tee -a ${logf} 
          fi    
        )
      done         
      source ${MYPROD_PRODPROD} > /dev/null 
      echo "### Completed job rescheduling. ${nstatus} submit/${nstatus_done} done.  `cat rescheduled/sub.log | wc -l` rescheduled."
  fi
  exit 12
fi
echo "### dstm-step3 jobs Passed done test" | tee -a ${logf}

recprod=`/bin/ls recprod_*.json 2>/dev/null | head -1`
elogid=`jsonread ${recprod} conf __ELOG_ID 2>/dev/null`
if [ "x${elogid}" != "x" ] ; then 
   putelog.py -i ${elogid} -m "DST-merge. All job completed. Downloading job logs and cleaning up spool." text | tee -a ${logf}
fi

####### Doneload joblog files and remove files in spool.

cat jobgroup.list | while read f ; do 
( 
 if [ ! -e job-get-${f}.done ] ; then 
    echo "### Downloading jobout of $f " | tee -a ${logf}
    dirac-wms-job-get-output -g $f 2>&1 | tee -a ${logf}
    jobchk=`grep "No jobs selected" ${logf} | grep $f | grep "Failed"`
    if [ "x${jobchk}" != "x" ] ; then 
       echo "### Completed job-get-output Done $f" | tee -a ${logf}
       echo "### job-get-output-complete " > job-get-${f}.done      
       rm -fv job-get-${f}.notyet
    else
       echo "### job-get-output not yet " > job-get-${f}.notyet
    fi
  fi
)
done
ndone=`/bin/ls job-get-*.done | grep -v "No such file or directory" | wc -l`

if [ ${ndone} -ne ${ngroup} ] ; then 
   echo "### found only ${igrp} job-group completed. Not go to next step." | tee -a ${logf}
   exit 10
fi

# ###################################### 
cat jobgroup.list | while read f ; do 
  if [ ! -e delete-${f}.log ] ; then 
    echo "### Removing job output of jobgroup $f" | tee -a ${logf}
    dirac-wms-job-delete -g ${f} 2>&1 | tee -a delete-${f}.log | tee -a ${logf}
  fi
done

# ############## prepare to do replication.
echo "### Donwload dstm job logfile completes" | tee -a ${logf}
echo "### Updating Elog with nevents information " | tee -a ${logf}

prodid=`head -1 jobgroup.list | sed -e "s/ILD-DSTM-prod//"`
recprod=`/bin/ls recprod_${prodid}_*.json`
noelog=`jsonread ${recprod}  conf __ELOG_NOELOG 2>/dev/null`
if [ "x${noelog}" != "xTRUE" ] ; then 
  echo "  Get ELOGID from a file, $recprod"
  putelog.py -c ${recprod} dstm_nevents 2>&1 | tee -a ${logf}
  if [ $? -ne 0 ] ; then 
    echo "### Some error happens while updating ELOG record " | tee -a ${logf}
    exit 6
  fi
fi

# #######################################
echo "### ELOG update completes.  DO dstm-replicate.sh next." | tee -a ${logf}
echo "### All dstm-step3.sh completed. " | tee -a ${logf}

exit 0

