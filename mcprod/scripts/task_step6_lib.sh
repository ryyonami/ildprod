#!/bin/bash
#
# A script for step6, create dst-merged files
#

stfile="status.log"

######################################################
my_echo()
{
  dstr=`date +%Y%m%d-%H%M%S`
  hostname=`hostname`
  echo "### task_step6.sh ${dstr} ${hostname} : $@" | tee -a ${stfile}
}

######################################################
has_elog()
{
  recprod_json=$1
  noelog=`jsonread ${recprod_json} conf __ELOG_NOELOG 2>/dev/null`
  if [ "x${noelog}" == "xTRUE" ] ; then
    return 1
  fi
  return 0
}



######################################################
check_status()
{
  # echo "check_status....."

  [[ ! -e ${stfile} ]] && my_echo "${stfile} is created."

  [[ -e task_step6.done ]] && \
     { my_echo "Done already. task_step6.done exists. " ; exit 0 ; }

  [[ ! -e prodstatus.log ]] && \
     { my_echo "Production did not complete. prodstaus.log not exist." ; exit 10 ; }

  [[ -z `grep "production done" prodstatus.log` ]] && \
     { my_echo "Production did not completed yet. " ; exit 10 ; }

  my_echo "checking system loads "

  runtest_by_load.sh || \
      { my_echo "Found too large load on this system. quit further execution" ; exit 20 ; }

  # echo "Passed all test."
}

##################################################################
# Create files to upload dstm-job-logs to ILCDirac
##################################################################
dstm_make_log()
{
  jsonfile=`find . -name "dstm-jobmaker-*.json" -print | head -1`
  elogid=`jsonread ${jsonfile} conf __ELOG_ID`
  reclog=`jsonread ${jsonfile} conf __DIRECTORY_RECLOG`
  dstmlog=`jsonread ${jsonfile} conf __DIRECTORY_DSTMLOG 2>/dev/null`
  if [ "x${dstmlog}" == "x" ] ; then
    dstmlog=`echo ${reclog} | sed -e "s|reclog|dstmlog|g" -e "s|dsk\.dsk|dsk|g"`
  fi
  prodids=`find . -maxdepth 1 -name "dstmjobs-*" -type d -print | cut -d"-" -f2 | sed -e "/[0-9]*/i-" | tail -n +2  | tr -d "\n"`
  dirname="dstmerge-logs.elog${elogid}.prod${prodids}"
  targz=${dirname}.tar.gz
  my_echo "dstmerge-logs targz=${targz}"

  copylog="copy-file.log"
  mkdir -pv ${dirname} > ${copylog} 2>&1
  cp -av dstmjobs-* ${dirname}  >> ${copylog} 2>&1
  cp -av ILD-DSTM-prod* ${dirname}  >> ${copylog} 2>&1
  cp -av dstm-*.log ${dirname}  >> ${copylog} 2>&1
  cp -av dstm-step3.log.20* ${dirname}  >> ${copylog} 2>&1
  cp -av jobmaker-*.log ${dirname}  >> ${copylog} 2>&1
  cp -av dstm-scan-*.json ${dirname}  >> ${copylog} 2>&1
  cp -av remove-* ${dirname}  >> ${copylog} 2>&1
  my_echo "Copied log file to ${dirname}"
  cp -av ${stfile} ${dirname} >> ${copylog} 2>&1
  cp -av ${copylog} ${dirname}
  tar zcf ${targz} ${dirname} 2>&1 | tee -a ${stfile}
  rm -rf ${dirname}
  my_echo "dstmerge-logs created. targz=${targz}"

  ildconfig=`jsonread ${jsonfile} dstm meta ILDConfig`
  evttype=`jsonread ${jsonfile} dstm meta EvtType`
  energyMachinepara=`jsonread ${jsonfile} energyMachinepara`
  logpath=`echo ${dstmlog}/${energyMachinepara}/${evttype}/${ildconfig}/${targz} | sed -e "s|//|/|g"`
  cmd="dirac-dms-add-file -ddd ${logpath} ${targz} DESY-SRM"
  dstr=`date +%Y%m%d-%H%M%S`
  echo "${cmd}" > logupload-cmd.sh
  chmod +x logupload-cmd.sh
  my_echo "dstmjob log upload command was created in ${cmd} "
}

##################################################################
# Check status based on the file in stdir directory
##################################################################
get_status()
{
  step=$1
  
  [[ -e stdir/${step}.running ]] && run="R" || run="_"
  [[ -e stdir/${step}.done ]] && done="D" || done="_"
  [[ -e stdir/${step}.error ]] && error="E" || error="_"

  rcode=${error}${done}${run}
  echo ${rcode}
  return

}



##################################################################
# Create files to upload dstm-job-logs to ILCDirac
##################################################################
is_step2_ready()
{
  st1=`get_status step1`
  if [ "${st1}" != "_D_" ] ; then 
    my_echo "dstm step1.done not exist yet."
    return 1
  fi 
  ###########################################################################
  stword=`grep "dstm-step1.sh completed." dstm-step1.log`
  if [ "x${stword}" == "x" ] ; then
    my_echo "dstm-step1.sh does not completed yet." 
    return 1
  fi
  
  ###########################################################################
  njobmaker=`/bin/ls jobmaker-*.log 2>/dev/null | wc -l`
  if [ "x${njobmaker}" == "x0" ] ; then
    my_echo "No Job_Maker log file yet." 
    return 1
  fi
  
  njobmaker_done=`cat jobmaker-*.log 2>/dev/null | grep -e "### ILD_DSTM_Job_Maker completed with code 0" -e "### ILD_DSTM_Job_Maker completed with code 2" 2>/dev/null | wc -l`
  if [ "x${njobmaker}" != "x${njobmaker_done}" ] ; then
    nfiles=`cat jobmaker-*.log | grep -e slcio -e stdhep | wc -l`
    my_echo "${njobmaker} Job_Maker logs, but completed ${njobmaker_done} log files. Searched ${nfiles} slcio/stdheps "
    return 1
  fi
  return 0
  
}

###########################################################################
# Check step3 status
###########################################################################
is_step3_ready()
{
  
  st2=`get_status step2`
  if [ "${st2}" != "_D_" ] ; then 
    my_echo "dstm step2.done not exist yet."
    return 1
  fi 

  stword=`grep "dstm-step2.sh has completed" dstm-step2.log`
  if [ "x${stword}" == "x" ] ; then
    my_echo "dstm-step2.sh does not completed yet."
    return 1
  fi
  return 0
}


###########################################################################
# Check replication readiness
###########################################################################
is_replication_ready()
{
  st3=`get_status step3`
  if [ "${st3}" != "_D_" ] ; then 
    my_echo "dstm step3.done not exist yet."
    return 1
  fi

  stword=`grep "### All dstm-step3.sh completed" dstm-step3.log`
  if [ "x${stword}" == "x" ] ; then
    my_echo "dstm-step3.sh does not complete yet"
    return 1
  fi
  return 0

}

##########################################################################
# check status of dstm-replication.
##########################################################################
done_replicate()
{
  strep=`get_status rep_sub`
  if [ ${strep} != "_D_" ] ; then 
     my_echo "dstm-replicate.sh does not complete yet." 
     return 1
  fi
  stword=`grep " : dirac-rms-request ALL DONE detected." dstm-replicate.log 2>/dev/null`
  if [ "x${stword}" == "x" ] ; then
    # request_name=`grep "Request " dstm-replicate.log | grep "has been put" | cut -d" " -f2 | cut -d"'" -f2`
    # dirac-rms-request ${request_name} > request-status.log
  
    get_replicate_status.sh > request-status.log
  
    nreq_done=`grep "Status='Done' " request-status.log | grep LFN | wc -l`
    nfiles_req=`cat dstm-replicate-list.txt | wc -l`
    nfiles_req_twice=$[${nfiles_req}*2]
    if [ ${nreq_done} -ne ${nfiles_req} -a ${nreq_done} -ne ${nfiles_req_twice} ] ; then
      my_echo "replicate not complete, ${nreq_done} DONE not equal neither ${nfiles_req} requested not ${nfiles_req_twice}."
      return 1
    fi
    my_echo "dirac-rms-request ALL DONE detected. " | tee -a dstm-replicate.log 
    recprod=`/bin/ls recprod_*.json 2>/dev/null | head -1`
    has_elog ${recprod} && {
      elogid=`jsonread ${recprod} conf __ELOG_ID`
      putelog.py -i ${elogid} -m "Replication of DST merged files have completed." text | tee -a ${stfile}
    } 
  fi
  return 0
}

###########################################################################
# Declare dstm_replicate_done for manual declararation to recover from error
# should be executed in 6_dstmerge directory
###########################################################################
declare_dstm_replicate_done(){
    my_echo "dirac-rms-request ALL DONE detected ( by task_step6_lib.sh/declare_dstm_replicate_done ) " | tee -a dstm-replicate.log 
    recprod=`/bin/ls recprod_*.json 2>/dev/null | head -1`
    has_elog ${recprod} && {
      elogid=`jsonread ${recprod} conf __ELOG_ID`
      stfile="status.log"
      putelog.py -i ${elogid} -m "Replication of DST merged files have completed." text | tee -a ${stfile}
    }
    return 0
}

###########################################################################
# Check readiness of log_upload
###########################################################################
is_upload_ready()
{
   stlog=`get_status log_make`
   if [ ${stlog} != "_D_" ] ; then 
     my_echo "DSTM joblog files are not created yet." 
     return 1
   fi
   return 0
}

###########################################################################
# Remove files of data type defined in __DELFILES of config file.
###########################################################################
dellist()
{
# Create a list of data file and prodID to be removed.
#
  jsonfile=`/bin/ls recprod_*.json | head -1`
  delfiles=`jsonread ${jsonfile} conf __DELFILES`
  rm -f delfiles.list
  touch delfiles.list
  if [ "x${delfiles}" == "x" ] ; then
     my_echo "Removal of no data type is requested."
     return 0
  fi
  my_echo "Removal of data type, ${delfiles}, is requested."
  declare -A dtype=( ["gensplit"]="sim" ["sim"]="rec" ["dst"]="dstm" )
  echo "${delfiles}" | sed -e "s/,/\n/g" | while read dreq; do
      ( for f in `/bin/ls recprod_*.json` ; do
         deltype=${dtype[${dreq}]}
         delid=`jsonread ${f} ${deltype} meta ProdID`
         echo "${dreq} ${delid}"
      done ) | sort | uniq | tee -a delfiles.list
  done
  return 0
}

###########################################################################
# Create remove requests 
###########################################################################
create_removal_request()
{
  jsonfile=`/bin/ls recprod_*.json | head -1`
  declare -A basedir
  basedir["gensplit"]=`jsonread ${jsonfile} conf __DIRECTORY_STDHEPSPLIT`
  basedir["sim"]=`jsonread ${jsonfile} conf __DIRECTORY_SIM`
  basedir["dst"]=`jsonread ${jsonfile} conf __DIRECTORY_DST`

  findlfns="remove-findlfns.cmd"
  cat delfiles.list | while read dtype prodid ; do
     basepath=${basedir[${dtype}]}
     rmlist="remove-${dtype}-${prodid}.list"
     echo "echo \" ### Finding lfns of ${dtype}, ProdID=${prodid}\" "
     echo "dirac-dms-find-lfns Path=${basepath}/${dtype} ProdID=${prodid} > ${rmlist} " 
  done > ${findlfns}

  delfiles=`jsonread ${jsonfile} conf __DELFILES`
  elogid=`jsonread ${jsonfile} conf __ELOG_ID`
  crreq="remove-create-request.cmd"
  cat delfiles.list | while read dtype prodid ; do
     basepath=${basedir[${dtype}]}
     rmlist="remove-${dtype}-${prodid}.list"
     echo "echo \" ### Finding lfns of ${dtype}, ProdID=${prodid}\" "
     echo "dirac-dms-create-removal-request All ${rmlist} " 
  done > ${crreq}
  has_elog ${jsonfile} && {
     echo "putelog.py -i ${elogid} -m \"Created removal requests of ${delfiles} data type\" text " >> ${crreq}
  }

  cmdall="remove-request.cmd"
  echo "source \${SCRIPTS_DIR}/task_step6_lib.sh " > ${cmdall}
  echo "my_echo \" ### finds lfns of data type to be removed \" " >> ${cmdall}
  cat ${findlfns} >> ${cmdall}
  echo "   " >> ${cmdall}
  echo "my_echo \" ### Creating removal requests of requested data type \" " >> ${cmdall}
  cat ${crreq} >> ${cmdall}
  echo "my_echo \" ### Creating removal requests DONE \" " >> ${cmdall}

  has_elog ${jsonfile} && {
    wd=`pwd | cut -d"/" -f5-`
    putelog.py -i ${elogid} -m "A script to remove ${delfiles} data type is created as ${wd}/${cmdall}." text 
  } 
  
}

# ###################################################################
# Check completion of file removal.
# ###################################################################
done_delfiles(){
  ndel=`cat delfiles.list | wc -l`
  [ ${ndel} -eq 0 ] && return 0
  ndone=`grep "Creating removal requests DONE" remove-request.log`
  
  if [ "x${ndone}" == "x" ] ; then
    my_echo "### Creating removal-request does not complete yet."
    return 4
  fi

  cat delfiles-request-ids.list | while read f ; do 
     icnt=$[$icnt+1] 
     [ $(($icnt%30)) -eq 0 ] && echo "$f" || echo -n "$f,"  
  done > temp-ids.list
  my_echo "### Getting status of removal-request"
  cat temp-ids.list | while read f ; do
     dirac-rms-request $f 
  done > delfiles-request-status.log
  nfiles=`grep "LFN=" delfiles-request-status.log | wc -l`
  notdone=`grep "LFN=" delfiles-request-status.log | grep -v Done | wc -l`
  my_echo "### Removal requested ${nfiles} files, remaining ${notdone} files."
  if [ ${notdone} -ne 0 ] ; then
     dstr=`date +%Y%m%d-%H%M%S`
     cp delfiles-request-ids.list delfiles-request-ids.list-${dstr}
     grep "Request name" delfiles-request-status.log | grep -v "Done" | while read line ; do
        newid=`echo $line | cut -d" " -f3 | cut -d"=" -f2` 
        echo ${newid}
     done > delfiles-request-ids.list
     return 4
  fi
  jsonfile=`/bin/ls recprod_*.json | head -1`
  has_elog ${jsonfile} && {
    delfiles=`jsonread ${jsonfile} conf __DELFILES`
    elogid=`jsonread ${jsonfile} conf __ELOG_ID`
    putelog.py -i ${elogid} -m "Requests to remove data type, ${delfiles}, is completed." text 
  } 
  my_echo "### Request of removal completed." 
  return 0
}

