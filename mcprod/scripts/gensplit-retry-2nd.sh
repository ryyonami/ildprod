#!/bin/bash 

# Number of files on DIRAC is consistent with those exist locally ?
# If not, upload and set meta.

# Can found same number of files by InputDataQuery, 
# If not set meta data to files not found by InputDataQuery.


stfile="gensplit-retry-2nd.log"
dstr=`date +%Y%m%d-%H%M%S`
hostname=`hostname`


echo "### Start gensplit-retry-2nd at ${hostname} from ${dstr} #######" | tee -a ${stfile}

logdir="recover-1"
if [ -e ${logdir} ] ; then 
  lastser=`/bin/ls -vd recover-* | tail -1 | sed -e "s/recover-//g"`
  let nextser=${lastser}+1
  if [ $nextser -gt 20 ] ; then
     echo "### Too many retry-2nd. Further retry abandoned." | tee -a ${stfile}
     exit -1
  fi
  logdir="recover-${nextser}"
fi
mkdir -v ${logdir}

if [ ${lendirac} -lt ${lenlocal} ] ; then 
# Get list of files.
find . -type f -name "E*.I*.slcio" -print > ${logdir}/localfile.list

gensplit_dir=`jsonread production.json __DIRECTORY_STDHEPSPLIT`
prodid=`jsonread production.json __PRODID_FOR_GENSPLIT`
dirac-dms-find-lfns Path=${gensplit_dir} ProdID=${prodid} > ${logdir}/diracfile.list

lenlocal=`cat ${logdir}/localfile.list | wc -l`
lendirac=`cat ${logdir}/diracfile.list | wc -l`

dstr=`date +%Y%m%d-%H%M%S`
echo "### ${dstr} : gensplit ${lenlocal} localfiles, ${lendirac} on dirac catalog " | tee -a ${stfile}

   cat ${logdir}/diracfile.list | while read f ; do ( basename $f ) ; done | sort > ${logdir}/diracslcio.list
   cat ${logdir}/localfile.list | while read f ; do ( basename $f ) ; done | sort > ${logdir}/localslcio.list
   ( cd ${logdir} && diff localslcio.list diracslcio.list ) > ${logdir}/diffslcio.list
   grep "< " ${logdir}/diffslcio.list | cut -d" " -f2 > ${logdir}/missing.list

   cat ${logdir}/missing.list | while read f ; do 
      procid=`getMetaFromFilename $f I`
      serstr=`getMetaFromFilename $f n`
      subser=`echo ${serstr} | cut -d"_" -f2`
      sernum=`echo ${serstr} | cut -d"_" -f1`
      if [ "${sernum}" == "000" ] ; then 
         ser=0
      else
         ser=`echo ${sernum} | sed -e "s/^0*//"`
      fi
      tdir=I${procid}.${ser}
      path_to_local=`grep ${f} ${logdir}/localfile.list`
      local_dir=`dirname ${path_to_local}`
      prodjson=${PWD}/production.json
      cddir=${local_dir/\/gensplit/}
      echo "  ++ cd ${cddir} to add ${f} " | tee -a ${stfile}
      ( cd ${cddir} 
        ln -sf ${prodjson} . 
        setMetaGenSplit -d ${tdir} ${subser} 2>&1 | tee -a setmeta.log ) | tee -a ${stfile}
   done
   echo ""

   dstr=`date +%Y%m%d-%H%M%S`
   echo "### ${dstr} : Updating files for catalog file check." | tee -a ${stfile}
   gensplit_dir=`jsonread production.json __DIRECTORY_STDHEPSPLIT`
   prodid=`jsonread production.json __PRODID_FOR_GENSPLIT`
   dirac-dms-find-lfns Path=${gensplit_dir} ProdID=${prodid} > ${logdir}/diracfile.list

   dstr=`date +%Y%m%d-%H%M%S`
   echo "### ${dstr} : gensplit ${lenlocal} localfiles, ${lendirac} on dirac catalog " | tee -a ${stfile}
   cat ${logdir}/diracfile.list | while read f ; do ( basename $f ) ; done | sort > ${logdir}/diracslcio.list
   ( cd ${logdir} && diff localslcio.list diracslcio.list ) > ${logdir}/diffslcio.list
   grep "< " ${logdir}/diffslcio.list | cut -d" " -f2 > ${logdir}/missing.list
   dstr=`date +%Y%m%d-%H%M%S`
   echo "### ${dstr} : Completed adding missing file." | tee -a ${stfile}
fi


#
# Make sure that all gensplit files can be found by InputDataQuery
#
gensplit_dir=`jsonread production.json __DIRECTORY_STDHEPSPLIT`
prodid=`jsonread production.json __PRODID_FOR_GENSPLIT`
enepara=`jsonread production.json __ENERGY_MACHINEPARA`
stdhepdir=`jsonread production.json __STDHEP_UPLOAD_DIR`
rm -f ${logdir}/found_by_IDQ.list

rm -f ${logdir}/setmeta-command.cli
# Find files not in sub-directory and check SelectedFile meta data
dstr=`date +%Y%m%d-%H%M%S`
echo "### ${dstr} : Finding meta data of files not in the sub-director" | tee -a ${stfile}
( cd ${logdir} && grep -v "${stdhepdir}/I" diracfile.list ) > ${logdir}/nosub.list
( cd ${logdir} && rm -f nosub-nometa.list nosub-hasmeta.list )
cat ${logdir}/nosub.list | while read f ; do 
   metaSF=`echo meta get ${f} | dirac-dms-filecatalog-cli | grep "SelectedFile" ` 
   if [ "x${metaSF}" == "x" ]; then 
     echo ${f} >> ${logdir}/nosub-nometa.list
     fbase=`basename ${f}`
     procid=`getMetaFromFilename $fbase I`
     serstr=`getMetaFromFilename $fbase n`
     subser=`echo ${serstr} | cut -d"_" -f2`
     sernum=`echo ${serstr} | cut -d"_" -f1`
     if [ "${sernum}" == "000" ] ; then 
        ser=0
     else
        ser=`echo ${sernum} | sed -e "s/^0*//"`
     fi
     subsernum=`echo ${subser} | sed -e "s/^0*//"`
     [ ${subser} == "000" ] && subsernum=0
     echo "( cd I${procid}.${ser} && setMetaGenSplit -u ${subsernum} 2>&1 | tee -a setmeta.log ) " >> ${logdir}/setmeta-command.cli
   else
     echo ${f} >> ${logdir}/nosub-hasmeta.list
     
   fi
done


dstr=`date +%Y%m%d-%H%M%S`
echo "### ${dstr} : Searching slcio files not have meta data." | tee -a ${stfile}
rm -f ${logdir}/setmeta-command.cli
touch ${logdir}/setmeta-command.cli
/bin/ls -vd I* | while read d ; do 
   dirac-dms-find-lfns Path=${stdhepdir}/${d} ProdID=${prodid} SelectedFile=1 > ${logdir}/${d}.list
   nfind=`cat ${logdir}/${d}.list | wc -l`
   nlocal=`/bin/ls ${d}/gensplit/*.slcio | wc -l`
   echo "${nfind}(${nlocal}) found(in_local) for ${d}" >> ${logdir}/found_by_IDQ.list
   if [ ${nfind} -lt ${nlocal} ] ; then 
      echo " ++ A possible missing metadata in ${d} found." | tee -a ${stfile}
      cat ${logdir}/${d}.list | while read f ; do 
        basename $f 
      done | sort > ${logdir}/filename-catalog-${d}.list
      ( cd ${d}/gensplit && /bin/ls *.slcio ) | sort > ${logdir}/filename-gensplit-${d}.list
      ( cd ${logdir} && diff filename-gensplit-${d}.list filename-catalog-${d}.list ) | \
        grep "<" | cut -d" " -f2 > ${logdir}/nometa-${d}.list

      cat ${logdir}/nometa-${d}.list | while read fn ; do 
        in_nosub=`grep ${fn} ${logdir}/nosub-hasmeta.list`
        if [ "x${in_nosub}" == "x" ] ; then 
          fbase=`basename ${fn}`
          serstr=`getMetaFromFilename $fbase n`
          subser=`echo ${serstr} | cut -d"_" -f2`
          subsernum=`echo ${subser} | sed -e "s/^0*//"`
          [ ${subser} == "000" ] && subsernum=0
          echo "( cd ${d} && setMetaGenSplit -u -d ${d} ${subsernum} 2>&1 | tee -a setmeta.log ) " >> ${logdir}/setmeta-command.cli
        fi
      done
   fi

done

echo "### `date +%Y%m%d-%H%M%S` : `wc -l ${logdir}/setmeta-command.cli` files without meta data " | tee -a ${stfile}
echo "### `date +%Y%m%d-%H%M%S` : A command to set meta is created in ${logdir}/setmeta-command.cli" | tee -a ${stfile}

# Get list of files.
find . -type f -name "E*.I*.slcio" -print > ${logdir}/localfile-last.list

gensplit_dir=`jsonread production.json __DIRECTORY_STDHEPSPLIT`
prodid=`jsonread production.json __PRODID_FOR_GENSPLIT`
dirac-dms-find-lfns Path=${gensplit_dir} ProdID=${prodid} > ${logdir}/diracfile-last.list

lenlocal=`cat ${logdir}/localfile-last.list | wc -l`
lendirac=`cat ${logdir}/diracfile-last.list | wc -l`

dstr=`date +%Y%m%d-%H%M%S`
echo "### ${dstr} : After gensplit  2nd retry, ${lenlocal} local and ${lendirac} dirac files. " | tee -a ${stfile}

elogflag=`jsonread production.json __ELOG_NOELOG` 
if [ "x${elogflag}" != "FALSE" ] ; then
   elogid=`jsonread production.json __ELOG_ID`
   putelog.py text -i ${elogid} -m "After gensplit 2nd retry, found ${lenlocal} local and ${lendirac} dirac files. "
fi

dstr=`date +%Y%m%d-%H%M%S`
echo "### ${dstr} : Completed meta data checking of gensplit files in DIRAC catalog." | tee -a ${stfile}

