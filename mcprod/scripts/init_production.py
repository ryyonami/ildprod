#!/usr/bin/env python 

from __future__ import print_function
import os
import sys
import pprint
import json
from datetime import datetime
import PreGenSplit
import ExcelTools
import ILDIDTool
from openpyxl import Workbook
from openpyxl import load_workbook
import argparse
import subprocess
import pycolor

# FAIL = '\033[91m'
# ENDC = '\033[0m'
_print_level = 0

pyc = pycolor.pycolor()

_allsheet = None

##########################################################
def create_prodinfo(prod_set, ngenfile_max=0, nw_perfile=0, pid_select="", 
                    split_nbfiles=0, multi_dir=False, split_nseq_from=1 ):
    ''' pickup information for the production from excel file. '''
    
    sheet_procid = _allsheets["byProcID"]

    # pprint.pprint(sheet_procid)

    # get proc_IDs for this prod_set
    procsel = {}
    procinfo = {}
    # Loop over all processIDs and select those which has the sampe subgroup value
    pid_list = []
    total_events = 0
    pol_list = []
    datadir = ""    
    pid_subevents = []
    pid_nbsplits = []
    dir_list = {}

#
# sortkey --> subgroup
# process_type --> EvtType
#

    for k,v in sheet_procid.items():
        doskip = False
        if len(pid_select)>0 and v["process_id"] not in pid_select:
           doskip = True
        if _print_level > 15:
            print( "v[subgroup]=" + str(v["subgroup"]) + ", prod_set="+str(prod_set) + ", doskip="+str(doskip))
        if not doskip and str(v["subgroup"]) == str(prod_set):
            if "EvtType" not in procinfo:
                procinfo["EvtClass"] = v["evtclass"]
                procinfo["EvtType"] = v["evttype"]
                procinfo["nbevts_perjob"] = v["nbevts_perjob"]
                procinfo["genname"] = v["genname"]
 
            if _print_level > 15:
                print( v["process_id"] + " matched." )

            process_id = str(v["process_id"]).replace("I","")
            filedir = v["filedir"].replace("lfn:/ilc/", "/ilc/").replace(" ","").replace("\n","")
            if not filedir.endswith("/"):
               filedir += "/"
            if not multi_dir :
                if len(datadir) > 0 and datadir != filedir:
                    print( pyc.c["red"] + "ERROR : Data from different directory is involved." + pyc.c["end"] )
                    print( datadir )
                    print( filedir )
                    exit(10)
                datadir = filedir

            dir_list[process_id] = filedir[:-1] if filedir.endswith("/") else filedir

            filename = v["filename"].replace(" ","")
            ngenfile = v["nbfile_genuse"]
            if ngenfile_max > 0 and ngenfile_max < int(v["nbfile_genuse"]):
                ngenfile = ngenfile_max
            nbevts = v["nbevts_perjob"] if nw_perfile < 1 else nw_perfile
            this_totev_submit = long(v["totev_submit"])
            if nw_perfile > 0 and split_nbfiles > 0:
               this_totev_submit = nw_perfile * ngenfile * split_nbfiles
            this_nbsplit_submit = 1 if this_totev_submit < nbevts else int(float(this_totev_submit)/float(nbevts))

            pid_list.append(process_id)
            pid_subevents.append(process_id+":"+str(this_totev_submit))
            pid_nbsplits.append(process_id+":"+str(this_nbsplit_submit))
            total_events += this_totev_submit
            if v["pol"] not in pol_list:
                pol_list.append(v["pol"])

            flist = []
            (fpre, nser, ftype ) = filename.rsplit('.',2)
            lnser = len(nser)
            prenser = ""
            if "n" in nser:
               lnser -= 1
               prenser = "n"
            subdir="" if multi_dir == "" else dir_list[process_id]
            for i in range(0, ngenfile):
               ffmt = "%s." + prenser + "%" + str(lnser)+"."+str(lnser)+"d.%s"
               fname = ffmt % ( fpre, i+split_nseq_from, ftype )
               if multi_dir:
                  fname = filedir + ffmt % ( fpre, i+split_nseq_from, ftype )
               entry = [fname, "nw_per_file="+str(nbevts)]  # nw_per_file is file-by-file differnt 
               flist.append(','.join(entry))

            procsel[k] = {"procinfo":v, "genfiles":flist, "subgroup":prod_set}

    if len(pid_list) == 0:
        print( pyc.c["red"] + "ERROR : " + pyc.c["end"] +
               " subgroup " + prod_set + " not found in the excel file." )
        exit(-1)

    procinfo["ProcessIDList"] = pid_list
    procinfo["TotalEvents"] = total_events
    procinfo["PID_SubEvents"] = ",".join(pid_subevents)
    procinfo["PID_NbSplits"] = ",".join(pid_nbsplits)
    procinfo["PolList"] = pol_list
    procinfo["selected"] = procsel
    procinfo["datadir"] = dir_list

    # pprint.pprint(procinfo)

    return procinfo

##########################################################
def get_script_information():
    ''' Get script name, path, of parent process, namely init.sh file. '''

    res = {}

    ppid = os.getppid()
    ps = subprocess.Popen( ('ps', 'p', str(ppid), 'eo', 'comm'), stdout=subprocess.PIPE )
    script = ""
    for line in ps.communicate()[0].split("\n"):
       line0 = line.replace("\n","")
       if line0 != "COMMAND" and script == "":
          script = line0
    res["SCRIPT"] = script

    res["ARGS"] = " ".join(sys.argv)

    return res

##########################################################
def write_script(cmd, sfile):
    fout=open(sfile, "w")
    fout.write(cmd)
    fout.close()
    lcmd = subprocess.check_output("chmod +x %s" % sfile, shell=True, stderr=subprocess.STDOUT)
    
    return

##########################################################
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Generate subdirectories and scripts to do production")
    parser.add_argument("--doc",help="Show example and more help",dest="helphelp", action="store_true", default=False)
    parser.add_argument("-d","--splitdir", help="dir names where splitted generator files are written.", 
                        dest="splitdir", action="store", default="undef")
    parser.add_argument("--excel_file", help="Excel file name, where production information is obtained.",
                        dest="excelfile", action="store", default="prodpara-phys.xlsx")
    parser.add_argument("--genfile_list", help="File name of generator file names",
                        dest="genfile_list", action="store", default="input_genfiles.list")
    parser.add_argument("--workdir", help="A directory where production set scripts are written", 
                        dest="workdir", action="store", default="undef")
    parser.add_argument("--prodlist", help="A file containing a list of production groupds.", 
                        dest="prodlist", action="store", default="physlist.txt")
    # prodlist should be
    # subgroup:[<sub_dir>/]Id1,Id2,Id3,...
    # If [<sub_dir>/] exist, <sub_dir> is attached to the directory name, otherwise subgroup is used as sub_dir name
    # Id1,Id2,... comma separated list of PIds

    parser.add_argument("--split_nbfiles", help="Number of splitted files to be produced.", 
                        dest="split_nbfiles", action="store", default="0")
    parser.add_argument("--ngenfile_max", help="Max number of generator file to use.", 
                        dest="ngenfile_max", action="store", default="0")
    parser.add_argument("--nw_perfile", help="number of events per file in gensplit.", 
                        dest="nw_perfile", action="store", default="0")
    parser.add_argument("--split_nseq_from", help="Sequence number of first file for gensplit", 
                        dest="split_nseq_from", action="store", default="1")
    parser.add_argument("--split_skip_nevents", help="Number of events to skip before starting gensplit", 
                        dest="split_skip_nevents", action="store", default="0")

    parser.add_argument("--production_type", help="Production type, sim, ovl, nobg. Default is sim:ovl", 
                        dest="prod_type", action="store", default="sim:ovl")
    parser.add_argument("--se_for_data", help="Output SE for Sim, Rec, DST, DSTMerged files. Log files are DESY-SRM",
                        dest="se_for_data", action="store", default="DESY-SRM")
    parser.add_argument("--se_for_sim", help="Output SE for SIM. Overwrite se_for_data setting",
                        dest="se_for_sim", action="store", default="")
    parser.add_argument("--se_for_rec", help="Output SE for REC. Overwrite se_for_data setting",
                        dest="se_for_rec", action="store", default="")
    parser.add_argument("--se_for_dstmerged", help="Output SE for DSTMerged. Overwrite se_for_data setting",
                        dest="se_for_dstmerged", action="store", default="")
   
    parser.add_argument("--se_for_gensplit", help="SE for gensplit files. ",
                    dest="se_for_gensplit", action="store", default="")
    parser.add_argument("--se_for_logfiles", help="SE for log files.",
                        dest="se_for_logfiles", action="store", default="")
    parser.add_argument("--se_for_dstm_replicates", help="SE(s) where to replicates DST-Merged files. \",\" separated list for multiple SEs (untested).",
                        dest="se_for_dstm_replicates", action="store", default="")
    parser.add_argument("--elogid_old", help="Elog ID referenced, where past production information is obtained",
                        dest="elogid_old", action="store", default="0")
    parser.add_argument("--reuse_gensplit", help="Use this gensplit_id for new simproduction",
                        dest="reuse_gensplit", action="store", default="0")
    parser.add_argument("--sim_nbtasks", help="Number of tasks of simulaton production",
                        dest="sim_nbtasks", action="store", default="-1")
    parser.add_argument("--rec_nbtasks", help="Number of tasks of reconstruction production",
                        dest="rec_nbtasks", action="store", default="-1")
    parser.add_argument("--sim_banned_sites", help="Banned sites for simulation production",
                        dest="sim_banned_sites", action="store", default="")
    parser.add_argument("--rec_banned_sites", help="Banned sites for reconstruction",
                        dest="rec_banned_sites", action="store", default="")
    parser.add_argument("--gen_data_type", help="generator data type, either stdhep or pairs",
                        dest="gen_data_type", action="store", default="stdhep")
    parser.add_argument("--sim_models", help="detector model for simulation ( or : separated list of it )",
                        dest="sim_models", action="store", default="undef")
    parser.add_argument("--rec_options", help="options for reconstruction ( or : separated list of it )",
                        dest="rec_options", action="store", default="undef")
    parser.add_argument("--topdir", help="If specified, it is the basedir to save sim/rec files. /ilc/prod/ilc/mc-opt for example. If MCProd_MCOpt_DirTape=mc-opt, mc-opt is replaced to MCProd_MCOpt_DirDisk for dst/log files.",
                        dest="topdir", action="store", default="")
    parser.add_argument("--mcprod_config", help="A file for 4_subprod/ThisProduction.py to over-write mcprod_config.py",
                        dest="thisprod_config", action="store", default="")
    parser.add_argument("--noelog", help="Do not generate ELOG data. An option for test scripts.", 
                        dest="noelog", action="store_true", default=False)
    parser.add_argument("--test", help="Scripts for a test production are generated.", 
                        dest="dotest", action="store_true", default=False)
    parser.add_argument("--multi_dir", help="If specified, generator files in different directories are used.",
                        dest="multi_dir", action="store_true", default=False)
    parser.add_argument("--genmeta_json", help="genmetaByID.json file used for production",
                        dest="genmeta_json", action="store", default="")
    parser.add_argument("--dstonly", help="Output DST files only, except a small number of jobs to write rec files.",
                        dest="dstonly", action="store_true", default=False)
    parser.add_argument("--simprod_json", help="Json file of sim production, which is used as an input of rec only production.",
                        dest="simprod_json", action="store", default=None)
    parser.add_argument("--recrate", help="If 0<r<1.0, fraction of REC files to keep; If 1<= r(integer), rec files matched to r generator files; r<0 not keep REF files at all. Valid when dstonly is TRUE.",
                        dest="recrate", default="-1")
    parser.add_argument("--xml_serial", help="First serial number attached to xml file name(default=0)",
                        dest="xml_serial", action="store", default="0")
    parser.add_argument("--delfiles", help="Comma separated list of file types to be removed at the end of the production.( gensplit,sim,dst )",
                        dest="delfiles", action="store", default="")
    parser.add_argument("--selected_file_recdst", 
          help="SelectedFile value for RECDST(keep REC) reconstruction. default=2",
          dest="selected_file_recdst", action="store", default="2")
    parser.add_argument("--selected_file_dstonly", 
          help="SelectedFile value for DSTONLY(rm REC) reconstruction. default=3",
          dest="selected_file_dstonly", action="store", default="3")
    parser.add_argument("--gensplit_numprocs", 
          help="Default number of paralle tasks for gensplit upload and setmeta",
          dest="gensplit_numprocs", action="store", default="8")
    parser.add_argument("--step4_options", help="Option string passed to ILDProdBase. Should be last options given",
                        dest="prodopts", nargs=argparse.REMAINDER, default=[])

    args = parser.parse_args()

    if args.helphelp:
       script_dir=os.environ["SCRIPTS_DIR"]
       for line in open(script_dir+"/init_production.help"):
          print(line.replace("\n",""))
       exit(0)

    # prod_set = args.prod_set
    splitdir_args= args.splitdir
    prodpara_excel = args.excelfile
    genfile_list = args.genfile_list
    ngenfile_max = int(args.ngenfile_max)

    split_nseq_from = int(args.split_nseq_from)
    split_skip_nevents = long(args.split_skip_nevents)

    prodlist = args.prodlist
    workdir = args.workdir
    nw_perfile = int(args.nw_perfile)
    split_nbfiles = int(args.split_nbfiles)
    prod_type = args.prod_type
    se_for_data = args.se_for_data
    se_for_sim = args.se_for_sim
    se_for_rec = args.se_for_rec
    se_for_dstmerged = args.se_for_dstmerged
    elogid_old = int(args.elogid_old)
    reuse_gensplit = int(args.reuse_gensplit)
    sim_nbtasks = int(args.sim_nbtasks)
    rec_nbtasks = int(args.rec_nbtasks)
    sim_banned_sites = args.sim_banned_sites
    for seprat in [":", ";", ","]:
        if seprat in sim_banned_sites:
            sim_banned_sites = args.sim_banned_sites.split(seprat)
    rec_banned_sites = args.rec_banned_sites
    for seprat in [":", ";", ","]:
        if seprat in rec_banned_sites:
            rec_banned_sites = args.rec_banned_sites.split(seprat)
    gen_data_type = args.gen_data_type
    multi_dir = args.multi_dir
    genmeta_json = args.genmeta_json
    se_for_gensplit = args.se_for_gensplit
    se_for_logfiles = args.se_for_logfiles
    se_for_dstm_replicates = args.se_for_dstm_replicates
    dstonly = args.dstonly
    sim_models = args.sim_models
    rec_options = args.rec_options    
    topdir = args.topdir
    simprod_json = args.simprod_json
    recrate = args.recrate
    delfiles = args.delfiles
    selected_file_recdst = int(args.selected_file_recdst)
    selected_file_dstonly = int(args.selected_file_dstonly)
    thisprod_config = "" if args.thisprod_config == "" else os.path.abspath(args.thisprod_config)
    gensplit_numprocs = args.gensplit_numprocs

    prodopts = " ".join(args.prodopts) if len(args.prodopts) > 0 else " --nodry --noPrompt "

    print( "prodopts is " + str(prodopts) )

    do_test = args.dotest
    noelog = args.noelog

    process_info = get_script_information()
    pprint.pprint(process_info)

    if os.path.exists(prodlist):
        print( "### Production sets are obtained from a file %s" % prodlist )
        prodset = []
        for line in open(prodlist):
            if line[0:1] != "#":
                prodset.append(line[:-1])

    # prodset = ["uds.b00", "PDGpm13.b00", "PDG22.b00"]

    workerlist = {"miyamoto":"A.Miyamoto", "ono":"H.Ono", "yonamine":"R.Yonamine"}

    # Special setting for test mode.
    setid = 0
    # do_test = True # If False, define full production parameters. Else only small number of events are produced.
    # do_test = False # If False, define full production parameters. Else only small number of events are produced.
    if noelog == True:
        print( pyc.c["yellow"] + "noelog is specified. ELOG data is NOT generated" + pyc.c["end"] )
  
    do_step = { # "setid_from_excel":False, 
                "setmeta":True, "split":True, "upload":True, "subsim":True, "subovl":["None"],
                "nbfiles":split_nbfiles,   "simModels":sim_models,   "productionType":prod_type}
    do_step["prodopts"] = "\' -N %d "+prodopts+" \'"
    do_step["recOptions"] = rec_options if rec_options != "undef" else "o1"

    if not do_test:
        do_step["simModels"] = sim_models if sim_models != "undef" else "ILD_l5_v02" 
        do_step["productionType"] = prod_type
    else:
        do_step["simModels"] = sim_models if sim_models != "undef" else "ILD_l5_v02"

    if simprod_json != None:
       if "sim" in do_step["productionType"]:
          print("Error: Inconsistent argument. --simprod_json was specified, but --production_type includes sim")
          exit(13)
       if not os.path.exists(simprod_json):
          print("Error: simprod_json file (%s) does not exist." % simprod_json)
          exit(13)

    # ======================= no need to touch following lines ==============================================

    # Load excel as dictionary
    # sheetinfo = [{"sheet":"byProcID", "keycol":"process_id"}, {"sheet":"summary", "keycol":"subgroup"}]
    sheetinfo = [{"sheet":"byProcID", "keycol":"process_id"}]
    if not os.path.exists(prodpara_excel):
        print( pyc.c["red"] + "ERROR : Excel file, %s does not exist." % prodpara_excel + pyc.c["end"] )
        exit(12)

    _allsheets = ExcelTools.load_excel_sheets(prodpara_excel, sheetinfo)

    list_of_genfiles = "input_genfiles.list"
    confjson="production.json"

    # Check existance of workdir
    if not os.path.exists(workdir):
        print( pyc.c["red"] + "ERROR : %s does not exists.  create it first." % workdir + pyc.c["end"] )
        exit(11)
 
#     if os.path.exists("%s/../production.setup" % workdir ): # Not necessary, keep it as backword compatibility
#        cmdout = subprocess.check_output("cd %s && ln -s ../production.setup . " % workdir , shell=True, stderr=subprocess.STDOUT)

    cwd = os.getcwd()
    scriptsdir = os.getenv('SCRIPTS_DIR')
    prodset_made = []
    numset = int(args.xml_serial)
    dirsubname = ""
    for prodset_line in prodset:
        prod_group = prodset_line
        pid_select = ""
        if ":" in prod_group:
           ( prod_group, pid_select_temp ) = prodset_line.split(":",1)
           if "/" in pid_select_temp:
              (dirsubname, pid_select ) = pid_select_temp.split("/",1)
           else:
              dirsubname = pid_select_temp
              pid_select = ""
        elif "/" in prod_group:
           ( prod_group, pid_select ) = prodset_line("/",1)
           
        print( pyc.c["light cyan"] + "### Making directories for %s " % prodset_line + pyc.c["end"] )
        prodset_made.append(prod_group)
        os.chdir(cwd)
        procinfo = create_prodinfo( prod_group, ngenfile_max=ngenfile_max, 
                                    nw_perfile=nw_perfile, pid_select=pid_select, split_nbfiles=do_step["nbfiles"],
                                    multi_dir=multi_dir, split_nseq_from=split_nseq_from )
    
        proddir = workdir + "/" + prod_group
        if dirsubname != "":
          proddir +=  dirsubname
        if os.path.exists(proddir):
            print( proddir + " exists already. Remove it first." )
            exit(10)

        evtclass = procinfo["EvtClass"]  # subdirectory of generator files
        evttype = procinfo["EvtType"]  # considered as output EvtType if multi_dir is set.
        generator = procinfo["genname"]
        setid += 1
        # if do_step["setid_from_excel"]:
        #     setid = _allsheets["summary"][prod_group]["nrow"]
        splitid = -1
        if reuse_gensplit > 0:
            splitid = reuse_gensplit
        elif "sim" in do_step["productionType"]:
            proddir_w = proddir.replace(os.environ["ILDPROD"]+"/","") if "ILDPROD" in os.environ else proddir
            res = ILDIDTool.get_nextid( idtype="gensplit", 
               logtext="init_production by %s for prod in %s" % ( process_info["SCRIPT"], proddir_w ) )

            if res["OK"] == False:
               print( "Some error happened in getting next ID number of gensplit" )
               exit(10)
            elif "NEXTID" in res:
               splitid = res["NEXTID"]
            else:
               print( "Some error happens. Return information fro get_nextid is" )
               print( res )
               exit(10)

        # print "splitdir="+str(splitdir)
        print( "splitid="+str(splitid) )
        print( "splitdir_args="+str(splitdir_args) )

        splitdir = str(splitid) if splitdir_args == "undef" else splitdir_args 

        
        os.mkdir(proddir)
        os.chdir(proddir)
        dirlevel0 = os.path.basename(os.environ["PRODTASKDIR"])
        dirlevel1 = proddir.split("/")[-2]
        dirlevel2 = proddir.split("/")[-1]
        startprod = "/".join([dirlevel0, dirlevel1, dirlevel2])
        # Step 1 : Prepare production
        step1dir = "1_prepProd"
        os.mkdir(step1dir)
        os.chdir(step1dir)

        fout=open("startprod.list","w")
        fout.write(startprod+"\n")
        fout.close()

        # Create a file, input_genfiles.list  list_of_genfiles
        energy = ""
        machinePara = ""
        fout = open(genfile_list, "w")
        for pid, v in sorted(procinfo["selected"].items()):
            files = v["genfiles"]
            for fline in files:
                if "," in fline:
                   (f, fdummy) = fline.split(",",1)
                else:
                   f = fline
                fout.write(fline+"\n")
                if energy == "":  # Decide energy and machine para from the first filename.
                   print( f )
                   fbase = os.path.basename(f)
                   energymachine = fbase.split('.')[0]
                   (energy, machinePara) = energymachine[1:].split('-')
                   energy = str(int(energy))
        fout.close()
        # pprint.pprint(procinfo)

        args = {"evttype":evttype, "evtclass":evtclass, "splitdir":splitdir, "splitid":splitid, "generator":generator, 
                "doTest":do_test, "se_for_data":se_for_data, "energy":energy, "machinePara":machinePara,
                "nbfiles":do_step["nbfiles"], "sim_nbtasks":sim_nbtasks, "rec_nbtasks":rec_nbtasks, 
                "split_nseq_from":split_nseq_from, "split_skip_nevents":split_skip_nevents, 
                "sim_banned_sites":sim_banned_sites, "rec_banned_sites":rec_banned_sites}
        if se_for_sim != "": 
           args["se_for_sim"] = se_for_sim 
        if se_for_rec != "": 
           args["se_for_rec"] = se_for_rec 
        if se_for_dstmerged != "": 
           args["se_for_dstmerged"] = se_for_dstmerged 
        if se_for_gensplit != "": 
           args["se_for_gensplit"] = se_for_gensplit 
        if se_for_logfiles != "": 
           args["se_for_logfiles"] = se_for_logfiles 
        if se_for_dstm_replicates != "":
           args["se_for_dstm_replicates"] = se_for_dstm_replicates
        if topdir != "":
           args["topdir"] = topdir
        if thisprod_config != "":
           args["thisprod_config"] = thisprod_config

        conf_default = PreGenSplit.prepareConfFile(**args)

        # Over-write default setting
        # conf_default["__SPLIT_MAX_READ"] = 0
        
        #############/#############################################################
        # FIX ME ( datadir )
        ##########################################################################

        if gen_data_type == "stdhep":
            if multi_dir:
                conf = PreGenSplit.make_stdhepsplit(conf_default, list_of_genfiles)
            else:
                
                adir_key = procinfo["datadir"].keys()[0]
                conf = PreGenSplit.make_stdhepsplit(conf_default, list_of_genfiles, 
		       datadir=procinfo["datadir"][adir_key])

        else:
            conf = conf_default
        if "sim" in do_step["productionType"]:
            PreGenSplit.set_dir_and_meta(conf)

        ### Add additional information for ELOG
        logname = os.environ["USER"]
        if noelog:
            conf["__ELOG_NOELOG"] = "TRUE"
        else:
            conf["__ELOG_NOELOG"] = "FALSE"
            conf["__ELOG_WORKER"] = workerlist[logname]
            conf["__ELOG_PRODUCTION_SETID"] = prod_group
            conf["__ELOG_PROCNAME"] = prod_group.split('.')[0]
            conf["__ELOG_PROCIDS"] = ','.join(sorted(procinfo["ProcessIDList"]))
            conf["__ELOG_POL"] = ','.join(sorted(procinfo["PolList"]))
            conf["__ELOG_NEVENTS"] = procinfo["TotalEvents"]
        if reuse_gensplit > 0:
           conf["__GENSPLIT_REUSE"] = reuse_gensplit
           conf["__ELOGID_OLD"] = elogid_old
        # Process information
        conf["____INIT_ARGS"] = process_info["ARGS"]
        conf["____INIT_SCRIPT"] = process_info["SCRIPT"]
        conf["____PID_SUBEvents"] = procinfo["PID_SubEvents"]
        conf["____PID_NbSplits"] = procinfo["PID_NbSplits"]
        conf["____MCPROD_TOP"] = os.environ["MCPROD_TOP"]
        if genmeta_json != "":
            conf["__GENMETA_JSON"] = genmeta_json

        if dstonly:
            conf["__DSTONLY"] = "TRUE"
            conf["__RECRATE"] = recrate

        conf["__DO_STEP"] = do_step
        conf["__DELFILES"] = delfiles
	conf["__GENSPLIT_NUMPROCS"] = gensplit_numprocs

        # Prepare json file for rec-only production
        reconly_json = ""
        if simprod_json != None:
          simprod=json.load(open(simprod_json))
          if simprod["status"]["ProdType"] != "sim":
             print("Error: simprod_json file is not a Sim production's json file.")
             exit(13)
          simprodid=simprod["status"]["ProdID"]
          simprodelog=simprod["conf"]["__ELOG_ID"]
          reconly_json = "reconly_sim%d_%s.json" % ( simprodid, simprod["sim"]["detectorModel"].replace("ILD_","") )
          conf["__SIMPROD_ID"] = simprodid
          conf["__SIMPROD_ELOG"] = simprodelog
          conf["__SIMPROD_JSON"] = reconly_json
          simprod["conf"] = conf
          json.dump(simprod, open(reconly_json, "w") )

        json.dump(conf, open(confjson, "w"))
        print( "### Configuration file is written in "+'/'.join([proddir, step1dir, confjson]) )
        outcmd1 = ["# Source this file to do step1 task", "task_step1.sh 2>&1 | tee run-task_step1.log"]
        fout1=open("run_step1.cmd","w")
        fout1.write("\n".join(outcmd1))
        fout1.close()
        
        os.chdir("../")

        # Step 3: Prepare for gensplit
        if "sim" in do_step["productionType"]:
          # testcmd = "Stdhep2LcioSplit" if do_step["split"] else "echo"
          # testcmd += " setMetaGenSplit" if do_step["upload"] else " echo"
          multi_dir_opt = " multi_dir" if multi_dir else ""
  
          step3dir = "3_gensplit"
          step3cmd = "ln -sf ../1_prepProd/production.json . && mk-files-gensplit.sh " + \
  		multi_dir_opt + " > mk-files-gensplit.log 2>&1 "
          step3cmd += "\n echo \"# Source this file to do step3 work \n task_step3.sh > run-task_step3.log 2>&1 \n \" > run_step3.cmd "
  
          os.mkdir(step3dir)
          os.chdir(step3dir)
          print( "Execute step3 cmd files: " + step3cmd )
          print( "### Be patient. It may take long to prepare files for many GenProcessID. ###" )
          lcmd = subprocess.check_output(step3cmd, shell=True, stderr=subprocess.STDOUT)
          os.chdir("..")

        # Step 4: Script to submit production with overlay 
        step4dir = "4_subprod"
        os.mkdir(step4dir)
        os.chdir(step4dir)
        step4cmd = " ".join(["ln -sfv "+scriptsdir+"/ILDProdBase.py . && ", 
                             "ln -sfv ../"+step1dir+"/production.json ."])
        prod_options = do_step["prodopts"] % numset

        numset += 1
        if simprod_json != None:
           temp = prod_options[:-2]
           prod_options = temp + " --SimProdJson " + reconly_json + " \'"
           step4cmd += " && ln -sfv ../" + step1dir + "/%s" % reconly_json + " . "
        step4cmd += "&& echo \"#!/bin/bash\" > task_step4.sh "
        step4cmd += "&& echo \"task_step4_main.sh -s " + do_step["simModels"] + " -r " + do_step["recOptions"]
        step4cmd += " --options " + prod_options 
        if noelog:
           step4cmd += " --noelog "
        if dstonly:
           step4cmd += " --dstonly "
        if selected_file_recdst != 2:
           step4cmd += " --selected_file_recdst %d " % selected_file_recdst 
        if selected_file_dstonly != 3:
           step4cmd += " --selected_file_dstonly %d " % selected_file_dstonly 
        prodtype = do_step["productionType"]
        if "aa_lowpt" == prod_group[:len("aa_lowpt")] or "seeablepairs" == prod_group[:len("seeablepairs")]:
            prodtype = "sim"
            print( "Only sim_production for the prod_group="+prod_group )
        if int(energy) == 1 :
            prodtype = do_step["productionType"].replace("ovl","nobg")
            print( "Reconstruction without background overlay for the prod_group="+prod_group )
        step4cmd += " --ptype " + prodtype + "\" >> task_step4.sh " 
        step4cmd += "&& chmod +x task_step4.sh " 

        if thisprod_config != "":
            step4cmd += "&& cp -v " + thisprod_config + " ThisProd_config.py"

        print( step4cmd )
        lcmd = subprocess.check_output(step4cmd, shell=True, stderr=subprocess.STDOUT)
        
        # Step 6: Create DSTMerge file at KEK
        step6dir = "6_dstmerge"
        os.chdir("..")
        os.mkdir(step6dir)
        os.chdir(step6dir)

        # Step 7: Log upload
        step7dir = "7_logupload"
        os.chdir("..")
        os.mkdir(step7dir)
        os.chdir(step7dir)
        
        os.chdir("..")
        
        # fout = open("do_all.sh","w")
        # fout.write('\n'.join(doallcmd))
        # fout.close()
        # lcmd = subprocess.check_output("chmod +x do_all.sh", shell=True, stderr=subprocess.STDOUT)

    os.chdir(cwd)
    os.chdir(workdir)
    flist = open("prodset.list","a")
    flist.write("\n".join(prodset_made))
    flist.write("\n")
    flist.close()

    
