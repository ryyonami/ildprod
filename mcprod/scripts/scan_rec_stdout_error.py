#!/bin/env python

import glob
import json
import pprint

if __name__ == "__main__":
  misslibname=[]
  inputfiles=[]
  inputfiles134=[]
  inputfiles1=[]
  jobinfo_data={}
  stat={"misslib":[], "exit134":[], "exit139":[], "exit1":[], "upload_error":[], "unknown":[], "failed_getovl":0}
  # for stdin in open("stdout.list"):
  for stdout in glob.glob("jobout/*/std.out"):
    # stdout = stdin.replace("\n","")
    (jobdir, ndir, stdoutname ) = stdout.replace("\n","").split("/",2)
    # ndir=stdout[:-1].split("/")[1]
    jobinfo={"jobid":ndir, "misslib":0, "status":-1, "exit1":0, "exit139":0, "exit134":0, "upload_error":0, "inputfile":"dummy", "failed_getovl":0}
    for line in open(stdout):
      if "INFO: Status after the application execution" in line[:-1]:
         (dumy, ret) = line[:-1].rsplit(" ",1)
         jobinfo["status"]=ret

      elif "ERROR: marlin exited With Status 134" in line[:-1]:
         jobinfo["exit134"] = 1

      elif "ERROR: marlin exited With Status 139" in line[:-1]:
         jobinfo["exit139"] = 1

      elif "ERROR: marlin exited With Status 1" in line[:-1]:
         jobinfo["exit1"] = 1

      elif "ERROR: Something went wrong with XML generation because Could not find any overlay files" in line[:-1]:
         jobinfo["failed_getovl"] = 1

      elif "failover SEs Failed to upload output data file" in line[:-1]:
         jobinfo["upload_error"]=1

      elif "is running at site" in line[:-1]:
         ( dummy, site ) = line[:-1].strip().rsplit(" ",1)
         jobinfo["site"] = site
      elif "ResolveInputFiles" in line[:-1] and "INFO: Will look for:" in line[:-1]:
         (dummy, inputfile )=line[:-1].rsplit(":",1)
         jobinfo["inputfile"] = inputfile 


    # pprint.pprint(jobinfo, width=255)
    # stat[ndir] = jobinfo 
    jobinfo_data[jobinfo["jobid"]] = jobinfo
    if jobinfo["exit139"] == 1:
       stat["exit139"].append(ndir)
       if jobinfo["inputfile"] not in inputfiles:
         inputfiles.append(inputfile)
    elif jobinfo["exit134"] == 1:
       stat["exit134"].append(ndir)
       if jobinfo["inputfile"] not in inputfiles134:
         inputfiles134.append(inputfile)

    elif jobinfo["exit1"] == 1:
       stat["exit1"].append(ndir)
       if jobinfo["inputfile"] not in inputfiles1:
         inputfiles1.append(inputfile)

    elif jobinfo["failed_getovl"] == 1:
       stat["failed_getovl"] +=1
    
    elif jobinfo["upload_error"] == 1:
       stat["upload_error"].append(ndir)
       if jobinfo["site"] in stat:
          stat[jobinfo["site"]] += 1
       else:
          stat[jobinfo["site"]] = 1
    else:
       stat["unknown"].append(ndir)    

  json.dump(jobinfo_data, open("jobinfo_data.json","w"))

  json.dump(stat, open("errsum.json","w"))
  fout = open("exit139_jobid.list","w")
  fout.write("\n".join(sorted(stat["exit139"])))
  fout.close()

  fout = open("exit134_jobid.list","w")
  fout.write("\n".join(sorted(stat["exit134"])))
  fout.close()

  fout = open("exit1_jobid.list","w")
  fout.write("\n".join(sorted(stat["exit1"])))
  fout.close()

  fout = open("unknown_jobid.list","w")
  fout.write("\n".join(sorted(stat["unknown"])))
  fout.close()

  print( "Exit139 : " + str(len(stat["exit139"]))+ " : File names in exit139_files.list" )
  fout = open("exit139_files.list","w")
  fout.write("\n".join(sorted(inputfiles)))
  fout.close()  
  print( "Exit134 : " + str(len(stat["exit134"]))+ " : File names in exit134_files.list" )
  fout = open("exit134_files.list","w")
  fout.write("\n".join(sorted(inputfiles)))
  fout.close()  
  print( "Exit1 : " + str(len(stat["exit1"]))+ " : File names in exit1_files.list" )
  fout = open("exit1_files.list","w")
  fout.write("\n".join(sorted(inputfiles)))
  fout.close()  
  print( "Failed_get_overlay : " + str(stat["failed_getovl"]) )
  print( "upload_error : " + str(len(stat["upload_error"])) )
  print( "unknown : " + str(len(stat["unknown"])) )
  # print "unknown id = " + " ".join(stat["unknown"])
