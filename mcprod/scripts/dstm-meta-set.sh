#!/bin/bash 


help()
{
  cat <<EOF
Usage: dstm-meta-set.sh [options] <ene_machine> <evt_type> [<detector_model> [<ildconfig>]] 
Function: 
   Set directory meta data to directories for DST-Merged files.

Example:
   dstm-meta-set.sh -s E500-TDR_ws 4f_WW_hadronic

Mandatory Arguments:
   <ene_machine> : Energy-MachinePara.  Example: 500-TDR_ws
   <evt_type>    : EvtType.  Example: 4f_WW_hadronic

Optional Arguments:
   <detector_model> : Detector model. Example: ILD_l5_o1_v02
   <ildconfig>      : ILDConfig.  Example: v02-00-01

   If these arguments are not given, meta data of all directories below <evt_type>
   are set. If <detector_model> ( and <ildconfig> ) is specified after <evt_type>, 
   meta data of directories below <detector_model> ( <detector_model>/<ild_config> ) 
   are set.

[Options]
   -h : Show this help
   -s : Do set meta. Default is dry run, i.e. not set meta, but output meta set 
        command for dirac-dms-filecatalog-cli
   -t : Base directory. If not given, /ilc/prod/ilc/mc-opt-3/ild/dst-merged
EOF

}


NODRY=0
BASE_DIR="/ilc/prod/ilc/mc-opt-3/ild/dst-merged"

ene_machine=
evt_type=
dmodel=
ildconfig=

while [ $# -ne 0 ] ; do 
  case "$1" in 
    -s) NODRY=1 ;;
    -t) shift ; BASE_DIR=$1 ;;
    -h) help ; exit 0 ;;
    *) if [ -z ${ene_machine} ] ; then 
         ene_machine=$1
       elif [ -z ${evt_type} ] ; then 
         evt_type=$1
       elif [ -z ${dmodel} ] ; then 
         dmodel=$1
       elif [ -z ${ildconfig} ] ; then 
         ildconfig=$1
       else
         echo "ERROR : Too many input argument. Last one was $1"
         help
         exit
       fi ;;
  esac
  shift
done

if [ -z ${evt_type} ] ; then 
  help
  exit
fi


# echo "ene_machine=${ene_machine}"
# echo "evt_type=${evt_type}"
# echo "detector_model=${dmodel}"
# echo "ildconfig=${ildconfig}"
# echo "BASE_DIR=${BASE_DIR}"
# echo "NODRY=${NODRY}"

searchdir=${BASE_DIR}/${ene_machine}/${evt_type}
if [ ! -z ${dmodel} ] ; then 
   searchdir="${searchdir}/${dmodel}"
fi
if [ ! -z ${ildconfig} ] ; then 
   searchdir="${searchdir}/${ildconfig}"
fi
searchdir=`echo ${searchdir} | sed -e "s|//|/|g"`

tempfile=metaset-$$.cli
metaname=("dummy" "EvtType" "DetectorModel" "ILDConfig")
metaget="find -D ${searchdir} "
echo ${metaget} | dirac-dms-filecatalog-cli | grep "${ene_machine}/${evt_type}" | sort | uniq | while read f ; do 
  maskdir=`dirname ${searchdir}`
  tdir=`echo $f | sed -e "s|${maskdir}/||g"`
  tline=`echo $tdir | sed -e "s|/|\n|g" | wc -l`
  bline=$[4-${tline}]
  for i in `seq 1 ${tline}` ; do 
     adir=`echo ${tdir} | cut -d"/" -f1-${i}`
     bdir=`basename ${adir}` 
     ipos=$[$bline+$i-1]
     echo "meta set ${maskdir}/${adir} ${metaname[${ipos}]} ${bdir}"
  done
done > ${tempfile}


if [ $NODRY -ne 1 ] ; then 
  echo "Following command will be executed."
  cat ${tempfile}
else
  cat ${tempfile} | dirac-dms-filecatalog-cli
fi
rm -f ${tempfile}

exit

  
    
