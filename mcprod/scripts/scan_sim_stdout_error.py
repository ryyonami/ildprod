#!/bin/env python

from __future__ import print_function
import glob
import json
import pprint

if __name__ == "__main__":
  misslibname=[]
  inputfiles=[]
  inputfiles126=[]
  jobinfo_data={}
  stat={"misslib":[], "exit134":[], "exit126":[], "upload_error":[], "unknown":[]}
  # for stdin in open("stdout.list"):
  for stdout in glob.glob("jobout/*/std.out"):
    # stdout = stdin.replace("\n","")
    (jobdir, ndir, stdoutname ) = stdout.replace("\n","").split("/",2)
    # ndir=stdout[:-1].split("/")[1]
    jobinfo={"jobid":ndir, "misslib":0, "status":-1, "exit134":0, "exit126":0, "upload_error":0, "inputfile":"dummy"}
    for line in open(stdout):
      if "INFO: Status after the application execution" in line[:-1]:
         (dumy, ret) = line[:-1].rsplit(" ",1)
         jobinfo["status"]=ret

      elif "cling::DynamicLibraryManager::loadLibrary()" in line[:-1] and "Failed to load DDG4 library" in line[:-1]:
         jobinfo["misslib"]=1            
         tokens = line[:-1].split()
         libname=""
         for i in range(0, len(tokens)):
            if "cling::DynamicLibraryManager::loadLibrary()" in tokens[i]:
               libname=tokens[i+1].replace(":","")
               if libname not in misslibname:
                 misslibname.append(libname)

      elif "ERROR: ddsim exited With Status 126" in line[:-1]:
         jobinfo["exit126"] = 1

      elif "ERROR: ddsim exited With Status 134" in line[:-1]:
         jobinfo["exit134"] = 1

      elif "failover SEs Failed to upload output data file" in line[:-1]:
         jobinfo["upload_error"]=1

      elif "is running at site" in line[:-1]:
         ( dummy, site ) = line[:-1].strip().rsplit(" ",1)
         jobinfo["site"] = site
      elif "ResolveInputFiles" in line[:-1] and "INFO: Will look for:" in line[:-1]:
         (dummy, inputfile )=line[:-1].rsplit(":",1)
         jobinfo["inputfile"] = inputfile 


    # pprint.pprint(jobinfo, width=255)
    # stat[ndir] = jobinfo 
    jobinfo_data[jobinfo["jobid"]] = jobinfo
    if jobinfo["misslib"] == 1 : 
       stat["misslib"].append(ndir)
    elif jobinfo["exit126"] == 1:
       stat["exit126"].append(ndir)
       if jobinfo["inputfile"] not in inputfiles126:
         inputfiles126.append(inputfile)
    elif jobinfo["exit134"] == 1:
       stat["exit134"].append(ndir)
       if jobinfo["inputfile"] not in inputfiles:
         inputfiles.append(inputfile)
    elif jobinfo["upload_error"] == 1:
       stat["upload_error"].append(ndir)
       if jobinfo["site"] in stat:
          stat[jobinfo["site"]] += 1
       else:
          stat[jobinfo["site"]] = 1
    else:
       stat["unknown"].append(ndir)    

  json.dump(jobinfo_data, open("jobinfo_data.json","w"))

  json.dump(stat, open("errsum.json","w"))

  fout = open("exit126_jobid.list","w")
  fout.write("\n".join(sorted(stat["exit126"])))
  fout.close()
  print( "Exit126 : " + str(len(stat["exit126"]))+ " : File names in exit126_files.list" )
  fout = open("exit126_files.list","w")
  fout.write("\n".join(sorted(inputfiles126)))
  fout.close()  

  fout = open("exit134_jobid.list","w")
  fout.write("\n".join(sorted(stat["exit134"])))
  fout.close()

  print( "Exit134 : " + str(len(stat["exit134"]))+ " : File names in exit134_files.list" )
  fout = open("exit134_files.list","w")
  fout.write("\n".join(sorted(inputfiles)))
  fout.close()  

  print( "misslib : " + str(len(stat["misslib"])) + ": misslibname=", end="")
  for mln in misslibname:
    print( " "+mln, end="")
  print( "" )
  print( "upload_error : " + str(len(stat["upload_error"])) )
  print( "unknown : " + str(len(stat["unknown"])) )
  print( "unknown id = " + " ".join(stat["unknown"]) )

