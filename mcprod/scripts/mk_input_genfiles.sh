#!/bin/bash

# A script used to create input_genfiles.csv, which was used for 
# DDSim of GuineaPig files. (250-SetA, ILD_ls5_v05 )

help()
{
cat <<EOF
Function: Create input_genfiles.csv, which is used by Userjob production (subDDSim.py)

Usage: mk_input_genfiles.sh <evsum> <ndivev> <nmax>

Mandatory arguments:
  <evsum> : A file containing the gensplitted file. Each line contains a space
           separated filename, number of gile, number of events, number of particle in
           original file.  This file is created by gp-get-nevents.sh, which reads a log file
           created by gp-split.sh.
  <ndivev> : A default number of particles per event.
  <nmax>   : Maximum number of sim files of each BX's.

Example in the case of 250-SetA 
  mk_input_genfiles.sh evsum.txt 400 2 | tee  input_genfiles.csv  
  
EOF
exit
}

# ###################  MAIN ##################################

if [ $# -lt 3 ] ; then 
  help
  exit
fi

evsum=$1
ndivev=$2
nmax=$3
echo "genfile,subseq,nb_skip,nevents"
grep -v "#" ${evsum} | while read a b c d ; do 
(
   nev1=1
   nevlast=${c}
   for iser in `seq 0 $[${nmax}-1]` ; do
   (
      evbegin=$[${ndivev}*${iser}+1]
      nevend=$[${ndivev}*(${iser}+1)]
      if [ ${iser} -eq $[${nmax}-1] ] ; then 
          nevend=${nevlast}
      fi 
      if [ ${nevend} -gt ${nevlast} ] ; then
         nevend=${nevlast}
      fi
      numevt=$[${nevend}-${evbegin}+1]
      nser=$[${iser}+1]
      if [ ${numevt} -gt 0 ] ; then
        nbskip=$[${evbegin}-1]
        echo "${a},${nser},${nbskip},${numevt}"
      fi
   )
   done
)
done

