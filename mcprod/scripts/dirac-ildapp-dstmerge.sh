#!/bin/bash

##########################################################
# Main part of merging dst files
##########################################################
do_lcio_merge()
{
  job_setup=$1
  dstfile_list=$2

  source ${job_setup}
  # source ${_merge_job_info}
  
  export MARLIN_DLL=
  this_init_ilcsoft=${_init_ilcsoft}
  if [ -e /etc/redhat-release ] ; then 
     hosttype=`cat /etc/redhat-release | cut -d"." -f1`
     if [ "x${hosttype}" == "xScientific Linux release 6" ] ; then 
        this_init_ilcsoft=`echo ${this_init_ilcsoft} | sed -e "s|centos7|sl6|g"`
     elif [ "x${hosttype}" == "xCentOS Linux release 7" ] ; then 
        this_init_ilcsoft=`echo ${this_init_ilcsoft} | sed -e "s|sl6|centos7|g"`
     fi
  fi

  echo "ilcsoft is initialized by ${this_init_ilcsoft}"
  source ${this_init_ilcsoft}
  export MARLIN_DLL=${ROOTSYS}/lib/libTreePlayer.so:${ROOTSYS}/lib/libGui.so:${ROOTSYS}/lib/libGed.so:${MARLIN_DLL} 
  maxrec=0
  marlinsteer=marlin_merge.xml
  marlin_log=marlin_lcio_merge.log


  # cp ${_gear_dir}/${_gear_file} ${_gear_file}
  cat ${_marlin_template}  | \
  sed -e "/%%LIST_MERGE_INPUT_FILES%%/r${dstfile_list}" -e "/%%LIST_MERGE_INPUT_FILES%%/d" \
      -e "s|%%MERGE_OUTPUT_FILE%%|${_dstm_file_name}|" > \
          ${marlinsteer}

   Marlin ${marlinsteer} 2>&1 | tee ${marlin_log}
#   Marlin ${marlinsteer}
  ret=$?
  echo "### Marlin completed with RET=$ret : `date` "  2>&1 | tee -a ${marlin_log}
  return ${ret}  

}


########################################
# Initialize job parameters
########################################

echo "### dst-merge job started : `date` "
echo "Executing a script, $0, argument=$*"
if [ "x$1" != "x" ] ; then 
  tar -z -x -f $1
  ainp=`basename $1 .tar.gz`
  mv -vf ${ainp}/* .
else
  echo "No argument is given to the current script, $0 "
fi

marlin_log=marlin_lcio_merge.log
job_setup="jobinfo.txt"
source ${job_setup}
if [ -f ${_unpack_tgz} ] ; then 
  tar -z -x -f ${_unpack_tgz}
fi
DO_DOWNLOAD="NO"
########################################
# Download file
########################################
if [ "X${DO_DOWNLOAD}" != "XNO" ] ; then 
  dirac-dms-get-file -ddd ${_dstlist} 2>&1 | tee download.log
  ret=$?
  echo "### Get DST files completed with RET=${ret} : `date` "
else
  echo "### Get DST files by dirac-dms command was skipped."
fi

echo "### List of files in the current directory "
/bin/ls -l

########################################
### Create a list of DST file names to be merged ( omit directory part )
########################################
dstfile_list="dstfilename.list"
rm -f ${dstfile_list}
cat ${_dstlist} | while read f 
do 
  dstfile=`basename $f`       # Omit directory part of path
  echo ${dstfile} >> ${dstfile_list}
done

########################################
# Run lcio_merge job
########################################
echo "### Going to execute do_lcio_merge ${job_sertup} ${dstfile_list} #####"
( do_lcio_merge ${job_setup} ${dstfile_list}  )

if [ $? != 0 ] ; then 
  echo "###ERROR Marlin ended with error. ret=${ret}"
  exit 10
fi

ret2=`grep -c "Marlin will have to be terminated, sorry" marlin_lcio_merge.log`
if [ ${ret2} != 0 ] ; then 
  echo "###ERROR Some error happened in Marlin"
  exit 9
fi

if [ ! -f ${_dstm_file_name} ] ; then 
  echo "###ERROR DST-merged file, ${_dstm_file_name} does not exist"
  exit 8
fi

nevents=`grep MyLCIOOutputProcessor marlin_lcio_merge.log | tail -1 | sed -e "s/ in / % /g" -e "s/ events / % /g" | cut -d"%" -f2 | sed -e "s/ *//g" `


echo "### Marlin merged ${nevents} events "
echo "### Marlin has completed sucessfully.  The merged file will be uploaded."

########################################
# Upload file
########################################

output_dir=`echo ${_dstm_dirname} | sed -e "s|%B|${_output_basedir}|g"`
echo "### Output directory is ${output_dir}"
/bin/ls -l

########################################
# Create a command to set meta data to the DSTMerged file.
########################################

dstfile1=`head -1 dstlist.txt`
echo "meta get ${dstfile1}" | dirac-dms-filecatalog-cli | \
  grep " : " | sed -e "s|FC:/>  ||g" -e "s/ : /=/g" -e "s/ *//g" > dstfile-meta.log
if [ $? -ne 0 ] ; then
  echo "### Error to get meta data from a DST file."
  exit 7
fi

echo "### Meta info extracted from ${dstfile} "
cat dstfile-meta.log

. dstfile-meta.log

dstmfile=${output_dir}/${_dstm_file_name}
set_filemeta_cli="setfilemeta.cli"
cat <<EOF > ${set_filemeta_cli}
meta set ${dstmfile} BeamParticle1 ${BeamParticle1}
meta set ${dstmfile} BeamParticle2 ${BeamParticle2}
meta set ${dstmfile} CrossSection ${CrossSection}
meta set ${dstmfile} CrossSectionError ${CrossSectionError}
meta set ${dstmfile} GenProcessID   ${GenProcessID}
meta set ${dstmfile} GenProcessName  ${GenProcessName}
meta set ${dstmfile} PolarizationB1  ${PolarizationB1}
meta set ${dstmfile} PolarizationB2  ${PolarizationB2}
meta set ${dstmfile} ProcessID  ${GenProcessID}
meta set ${dstmfile} SWPackages  ${SWPackages}
meta set ${dstmfile} SoftwareTag  ${SoftwareTag}
meta set ${dstmfile} NumberOfEvents ${nevents} 
meta get ${dstmfile}
EOF
# meta set ${dstmfile} ProdID  ${ProdID}
cat dstlist.txt | while read f ; do 
  echo ancestorset ${dstmfile} ${f}
done >> ${set_filemeta_cli} 

echo "### Set filemeta cli was created."
cat ${set_filemeta_cli}

########################################
echo "#### Switching to ilc_prod role : `date`"
tar -z -x -f himitsu.cide
source himitsu/purodo2kaeru.sh

which python
echo "import pprint,sys ; pprint.pprint(sys.path) ; import gfal2 ; pprint.pprint(sys.path) " | python -

  dirac-proxy-info
  dirac-dms-add-file -ddd ${output_dir}/${_dstm_file_name} ${_dstm_file_name} ${_output_dstm_se} 2>&1 | tee upload.log
  ret=$?
  echo "###UPLOAD file completed with RET=$ret : `date`"
  retlog=`grep -e dirac_dms_add_file -e dirac-dms-add-file upload.log | grep -c ERROR`

#  if [ "x${_set_dirmeta_cli}" != "x" ] ; then 
#    cat ${_set_dirmeta_cli} | sed -e "s|%B|${_output_basedir}|g" > setdirmeta.cli
#    echo "exit" >> setdirmeta.cli
#    cat setdirmeta.cli | dirac-dms-filecatalog-cli 2>&1 | tee setmeta.log
#  fi

  if [ "x${set_filemeta_cli}" != "x" ] ; then 
    cat ${set_filemeta_cli} | dirac-dms-filecatalog-cli 2>&1 | tee setmeta.log
    retlog_meta=$?
  fi

########################################
# Cleaning up before job termination
########################################
echo "#### Return to to ilc_user role : `date`"
source himitsu/yu-za-2kaeru.sh
dirac-proxy-info
rm -rf himitsu himitsu.*

echo "###UPLOAD file completed with RET=$ret : log-ret=$retlog  : `date`"

if [ $ret != 0 -o $retlog != 0 ] ; then
  echo "###Error in upload file.  ret=$ret, retlog=$retlog"
  exit 2
fi
if [ $retlog_meta -ne 0 ] ; then
   echo "### Error to set meta data to DSTMerged file."
   exit 5
fi

echo "###All step completed."
exit 0

