#!/bin/bash 


hostname=`hostname`

for i in `seq 1 800` ; do

echo "#############################################"
dstr=`date +%Y%m%d-%H%M%S`
echo "### ${hostname} : ${dstr} : seq# $i start setselectedfile.sh ProcessID=$$ "
pwd

dstr=`date +%Y%m%d-%H%M%S`
timeleft=`dirac-proxy-info | grep timeleft `
echo "### ${hostname} : ${dstr} | dirac-proxy timeleft is ${timeleft} at nloop=${nloop}"
tl=`echo ${timeleft} | cut -d":" -f2 | sed -e "s| ||g"`

if [ ${tl} -lt 6 ] ; then
    dstr=`date +%Y%m%d-%H%M%S`
    echo "### ${hostname} : ${dstr} | Remaining time of dirac proxy is less than 6 hours. proxy is initialized again."
    if [ ! -z ${MYPROD_PRODPROD} ] && [ -e ${MYPROD_PRODPROD} ] ; then
       source ${MYPROD_PRODPROD}
    fi
fi

set_sim_selected.py 
if [ $? -ne 8 ] ; then
  head -700 setmeta_seleced_file.cmd | dirac-dms-filecatalog-cli 
fi

dstr=`date +%Y%m%d-%H%M%S`
echo "### ${hostname} : ${dstr} : Done loop $i ##############################"

if [ -e ../6_dstmerge/prodstatus.log ] ; then 
  dstr=`date +%Y%m%d-%H%M%S`
  echo "### ${hostname} : ${dstr} : ../6/dstmerge/prodstatus.log is found. setselectedfile.sh quit."
  exit 0
fi


echo "Done loop $i ### `hostname` `date` ProcessID=$$, sleeps 12 min. ###################"
sleep 12m
echo "Wakeup loop $i ### `hostname` `date` ProcessID=$$ ####################"

done


dstr=`date +%Y%m%d-%H%M%S`
echo "### ${hostname} : ${dstr} : setselectedfile completed all loop ##############################"

