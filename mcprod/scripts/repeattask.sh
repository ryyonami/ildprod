#!/bin/bash 

atask=$1

if [ -z ${atask} ] ; then 
  echo "No argument is given. COmmand format is "
  echo "repeattask.sh [script]"
  echo " "
  exit
fi

hostname=`hostname`
dstr=`date +%Y%m%d-%H%M%S`
echo "### ${hostname} : ${dstr} | repeattask of ${atask} begins."
echo "### Process ID = $$"

maxloop=100
sleeptime=20m
nloop=0

while [ ${nloop} -lt ${maxloop} ] ; do
  let nloop=nloop+1
  dstr=`date +%Y%m%d-%H%M%S`
  timeleft=`dirac-proxy-info | grep timeleft `
  echo "### ${hostname} : ${dstr} | dirac-proxy timeleft is ${timeleft} at nloop=${nloop}"
  tl=`echo ${timeleft} | cut -d":" -f2 | sed -e "s| ||g"`
  
  if [ ${tl} -lt 6 ] ; then 
    dstr=`date +%Y%m%d-%H%M%S`
    echo "### ${hostname} : ${dstr} | Remaining time of dirac proxy is less than 6 hours. proxy is initialized again."
    if [ ! -z ${MYPROD_PRODPROD} ] && [ -e ${MYPROD_PRODPROD} ] ; then 
       source ${MYPROD_PRODPROD}
    else
       echo "### ${hostname} : ${dstr} | A script to initialize ILCDirac proxy is not defined."
       echo "### repeattask will end."
       exit 0
    fi   
  fi
  dstr=`date +%Y%m%d-%H%M%S`
  echo "### ${hostname} : ${dstr} | Executing ${atask}."
  ${atask}

  endtest=`tail -10 status.log | grep "ALL TASKS DONE"`
  if [ "x${endtest}" != "x" ] ; then 
     dstr=`date +%Y%m%d-%H%M%S`
     echo "### ${hostname} : ${dstr} | ALL TASKS DONE of ${atask} detected.  repeattask.sh quits."
     exit 0
  fi

  dstr=`date +%Y%m%d-%H%M%S`
  echo "### ${hostname} : ${dstr} | Fall in sleep ${sleeptime}"

  sleep ${sleeptime}

  dstr=`date +%Y%m%d-%H%M%S`
  echo "### ${hostname} : ${dstr} | Wakeup from sleep ${sleeptime}. Start next loop"


done

echo "### ${hostname} : ${dstr} | ERROR - Repeattask reached end of ${maxloop} loops. "
echo "### repeattask.sh [task] >> repeat.log  to restart after fixing error."
echo "### [task] = task_step6.sh or task_step7.sh"
