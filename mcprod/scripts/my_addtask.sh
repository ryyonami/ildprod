#!/bin/bash

dstr=`date +%Y%m%d-%H%M%S`
echo "Starting my_addtask at `pwd` on ${dstr}"
export LANG=C
#latestlog=`find logs -name "prodmon-*" -type f -print | sort | tail -10 | xargs -I{} grep -Ha "All cronexe.list complete" {} 2>/dev/null | grep "All cronexe" | cut -d":" -f1 | tail -1`
#latestlog="logs/translog-latest.log"
latestlog=logs/`( cd logs && /bin/ls -v translog-*.log | grep -v translog-latest.log | tail -1 )`


prevlog=""
if [ -e addtask-latestlog.log ] ; then
  prevlog=`cut -d":" -f2 addtask-latestlog.log`
fi

if [ "x${latestlog}" == "x${prevlog}" ] ; then 
  echo "${latestlog} was used previously. `cat addtask-latestlog.log` "
  exit
fi  

dstr=`date +%Y%m%d-%H%M%S`
echo "Reading cronlog file ${latestlog}"
echo "${dstr}:${latestlog}"  > addtask-latestlog.log 

maxadd=1000
naddsum=0
maxovlrun=1200
maxovlrunwait=1500
maxsimrun=3500
maxsimrunwait=4000
if [ ! -e addtask.dat ] ; then 
   if [ -e addtask.dat.default ] ; then 
     /usr/bin/cp -v addtask.dat.default addtask.dat
   fi
fi

if [ -e addtask.dat ] ; then 
. addtask.dat
fi

. ${MYPROD_PRODPROD}  > /dev/null
cat ${latestlog} | sed -ne '/production status/,$P' | grep -e " Sim " -e " Ovl " -e " Nobg " > tempadd.log

nsimrun=0
nsimwait=0
nsimrrw=0
novlrun=0
novlwait=0
novlrw=0

while read a ; do 
  prodid=`echo $a | cut -d" " -f1`
  nfiles=`echo $a | cut -d">" -f2 | cut -d" " -f2`
  ntasks=`echo $a | cut -d"|" -f2 | cut -d" " -f2`
  ptype=`echo $a | cut -d" " -f2`
  nwait0=`echo $a | cut -d">" -f3 | cut -d" " -f2`
  nrun0=`echo $a | cut -d">" -f3 | cut -d" " -f3`
  ndone0=`echo $a | cut -d">" -f3 | cut -d" " -f4`
  nrw0=$[${nrun0}+${nwait0}]
  nadd=$[${nfiles}-${ntasks}]
  notproc=$[${ntasks}-${nrun0}-${nwait0}-${ndone0}]

  if [ "${ptype}" == "Sim" ] ; then 
    nsimrun=$[${nsimrun}+${nrun0}]
    nsimwait=$[${nsimwait}+${nwait0}]
    nsimrw=$[${nsimrw}+${nrw0}]
    echo "ProdID=${prodid}, ${ptype} : #File/#Tasks(${nfiles}/${ntasks}) : SUM run=${nsimrun}, wait=${nsimwait}, run+wait=${nsimrw}, nadd0=${nadd}" 
  elif [ "${ptype}" == "Ovl" ] ; then 
    novlrun=$[${novlrun}+${nrun0}]
    novlwait=$[${novlwait}+${nwait0}]
    novlrw=$[${novlrw}+${nrw0}]
    echo "ProdID=${prodid}, ${ptype} : #File/#Tasks(${nfiles}/${ntasks}) : SUM run=${novlrun}, wait=${novlwait}, run+wait=${novlrw}, nadd0=${nadd}" 
  fi
  # echo "Prodid=$prodid nfiles=$nfiles ntask=$ntasks"
  if [ $nfiles -gt $ntasks ] ; then 
     nadd=$[${nfiles}-${ntasks}]
     iaddsum=$[${naddsum}+${nadd}]
     if [ ${iaddsum} -gt ${maxadd} ] ; then 
       nadd=$[${maxadd}-${naddsum}]
     fi
     #
     # When Ovl Production ##################################
     if [ "${ptype}" == "Ovl" ] ; then
       # echo "nadd=$nadd, novlrun=${novlrun} novlrw=${novlrw}: ${maxovlrun}, ${maxovlrunwait}"
       if [ ${novlrun} -ge ${maxovlrun} ] ; then 
          nadd=0
       elif [ $[${novlrun}+${nadd}] -gt ${maxovlrun} ] ; then
          nadd=$[${maxovlrun}-${novlrun}]
       fi
       # echo "nadd=$nadd, novlrun=${novlrun} novlrw=${novlrw}: ${maxovlrun}, ${maxovlrunwait}"
       if [ ${novlrw} -ge ${maxovlrunwait} ] ; then
          nadd=0
       elif [ $[${novlrw}+${nadd}] -gt ${maxovlrunwait} ] ; then 
          nadd=$[${maxovlrunwait}-${novlrw}]
       fi
       nadd=$[${nadd}+10]   # Always add slightly larger number
       if [ ${notproc} -gt ${maxovlrunwait} ] ; then
          nadd=0
       fi
       # echo "nadd=$nadd, novlrun=${novlrun} novlrw=${novlrw}: ${maxovlrun}, ${maxovlrunwait}"
     fi
     #
     # When Sim Production ##################################
     if [ "${ptype}" == "Sim" ] ; then
       # echo "nadd=$nadd, nsimrun=${nsimrun} nsimrw=${nsimrw}: ${maxsimrun}, ${maxsimrunwait}"
       if [ ${nsimrun} -ge ${maxsimrun} ] ; then 
          nadd=0
       elif [ $[${nsimrun}+${nadd}] -gt ${maxsimrun} ] ; then
          nadd=$[${maxsimrun}-${nsimrun}]
       fi
       # echo "nadd=$nadd, nsimrun=${nsimrun} nsimrw=${nsimrw}: ${maxsimrun}, ${maxsimrunwait}"
       if [ ${nsimrw} -ge ${maxsimrunwait} ] ; then
          nadd=0
       elif [ $[${nsimrw}+${nadd}] -gt ${maxsimrunwait} ] ; then 
          nadd=$[${maxsimrunwait}-${nsimrw}]
       fi
       nadd=$[${nadd}+10]   # Always add slightly larger number
       # echo "nadd=$nadd, nsimrun=${nsimrun} nsimrw=${nsimrw}: ${maxsimrun}, ${maxsimrunwait}"
     fi
     naddsum=$[${naddsum}+${nadd}]
     if [ ${nadd} -gt 0 ] ; then 
       echo "--- exec add-tasks ${prodid} nadd=${nadd} : addsum=${naddsum}, max add=${maxadd} sim=${maxsimrunwait}, ovl=${maxovlrunwait}"
       dirac-ilc-add-tasks-to-prod ${prodid} ${nadd} 
     fi
     [ ${naddsum} -ge ${maxadd} ] && break
  fi
done < tempadd.log

