
# ############################################################3
# stdecho
# ############################################################3
stdecho()
{
  message=$1
  pre=$2
  [ "x${pre}" == "x" ] && pre="+++"
  dstr=`date +%Y%m%d-%H%M%S`
  echo "${pre} ${dstr} : ${message}"
}

# ############################################################3
# Check DIRAC Proxy
# ############################################################3
check_proxy()
{
  echo "**** Checking proxy info..."
  proxy_register=`dirac-proxy-info | grep "VOMS"`
  if [ "x${proxy_register}" == "x" ]; then
    echo "**** dirac-proxy-init is not initiated."
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    exit
  fi

  proxy_group=`dirac-proxy-info | grep "DIRAC group" | cut -d ':' -f 2 | sed s/\ //g`
  if [ "x${proxy_group}" == "xilc_prod" ]; then
    echo "**** Production role ${proxy_group} is selected."
  else
    echo "**** Selected role is ${proxy_group}"
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    echo "Exit"
    exit
  fi
}


# ############################################################
# Production of one directory, 1_preprod, 3_gensplit and 4_subprod
# ############################################################
do_onedir(){
  rdir=$1
  nsleep=$2
  par_dir=$3
  if [ -d ${rdir} ] ; then 
    ( 
      cd ${rdir}
      stdecho "current directory : `hostname`, `pwd`"
      ( cd 1_prepProd  &&  . run_step1.cmd &&  stdecho "task in 1_preProd completed" "###"  )
      ( 
        # stdecho "Sleeping ${nsleep} before starting task_step4.sh" 
        sleep ${nsleep}
        stdecho "Wakeup from ${nsleep}. task_step4.sh will be executed."
        cd 4_subprod 
        stdecho "Wakeup. task_step4.sh starts : `hostname`, `pwd`"
        ./task_step4.sh > run.log 2>&1 
        crpid=`grep "Created transformation" run.log | cut -d" " -f3 | while read f ; do ( echo -n "$f," ) ; done`
        chklast=`tail run.log | grep "### subprod completed"`
        if [ "x${chklast}" != "x" ] ; then 
           stdecho "task_step4.sh has completed. Transformations ${crpid} are created."
        else
           stdecho "taks_step4.sh has terminated before the end of script."
        fi
      ) & 
      stdecho "move to 3_gensplit and start splitting."
      ( cd 3_gensplit 
        stdecho "start gensplit : `hostname`, `pwd`" > ${par_dir}/startprod.call_gensplit
        task_step3.sh > run-task_step3.log 2>&1 
        rm -f ${par_dir}/startprod.call_gensplit
      )
      stdecho "3_gensplit in ${rdir} has completed."
    )
  else
    stdecho "ERROR: directory, ${rdir}, does not exist and skipped."
  fi
}

############################################
# is_there_space_for_sim
############################################
is_there_space_for_sim()
{
  jsonnext="`head -1 startprod.list`/1_prepProd/production.json"
  # echo ${jsonnext}
  if [ -e ${jsonnext} ] ; then
     simbase=`jsonread ${jsonnext} __DIRECTORY_SIM`
     simdir=`echo ${simbase}/sim | sed -e "s|//|/|g"`
     simse=`jsonread ${jsonnext} __SIM_SE`
     
     echo "size -l ${simdir}" | dirac-dms-filecatalog-cli | grep ${simse} > temp.txt
     val=`cat temp.txt | while read a b c d ; do ( echo $c ) ; done | sed -e "s/,//g"`
     if [ "x${val}" == "x" ] ; then 
       stdecho "Failed to get storage space of SIM SE ${simse}. Wrong SE name ?"
       return 16
     fi
     giga_byte=$[${val}/1000000000]
     giga_limit=`grep ${simse/-/} startprod.opt | cut -d"=" -f2`
  
     if [ ${giga_byte} -lt ${giga_limit} ] ; then 
         stdecho "available : ${simse} ${giga_byte} GB within limit of ${giga_limit} GB "
         return 0
     else
         stdecho "no space : ${simse} ${giga_byte} GB excedds limit of ${giga_limit} GB "
         return 4
     fi      
  fi
  stdecho "Not exist ${jsonnext}"
  return 16
}

# ###############################################################
#  Wait next production be ready
# wait_next_be_ready 
# parameters are taken from startprod.opt
# ###############################################################
wait_next_be_ready()
{
  source startprod.opt
  stdecho "`hostname` : Checking status of active production : ${max_job_insys} ${max_sim_trans} ${sleep_before_next_loop}"
  print_transformation_status.sh 2>/dev/null | tee transformation_status.log
  jobinsys=`grep total_jobs_insys transformation_status.log | cut -d"=" -f2`
  nsimtrans=`grep -v "#" prodids.list 2>/dev/null | grep simprod | wc -l`
  stdecho "${jobinsys} jobs in transformations.  There are ${nsimtrans} SIM transformations."
  flag=1
  [ ${jobinsys} -gt ${max_job_insys} ] && flag=0
  [ ${nsimtrans} -ge ${max_sim_trans} ] && flag=0
  is_there_space_for_sim
  rc=$?
  [ ${rc} -eq 16 ] && return 16
  [ ${rc} -ne 0 ] && flag=0

  while [ ${flag} -eq 0 ] ; do
    source startprod.opt
    stdecho "`hostname` : Sleep ${sleep_before_next_loop}. ProcessID=$$"
    sleep ${sleep_before_next_loop}
    stdecho "Initializing DIRAC Proxy with production role" 
    . ${MYPROD_PRODPROD} > init_production.log 2>&1 
    source startprod.opt
    stdecho "`hostname` : Checking status of active production : ${max_job_insys} ${max_sim_trans} ${sleep_before_next_loop}"
    print_transformation_status.sh | tee transformation_status.log
    jobinsys=`grep total_jobs_insys transformation_status.log | cut -d"=" -f2`
    nsimtrans=`grep -v "#" prodids.list 2>/dev/null | grep simprod | wc -l`
    stdecho "${jobinsys} jobs in transformations.  There are ${nsimtrans} SIM transformations."
    flag=1
    [ ${jobinsys} -gt ${max_job_insys} ] && flag=0
    [ ${nsimtrans} -ge ${max_sim_trans} ] && flag=0
    is_there_space_for_sim
    rc=$?
    [ ${rc} -eq 16 ] && return 16
    [ ${rc} -ne 0 ] && flag=0

  done
  return 0   
}


