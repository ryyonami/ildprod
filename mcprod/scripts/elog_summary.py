#!/bin/env python 
#
# Create a production summary based on the information on elog
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob
import ssl
from urllib2 import urlopen
import subprocess

import ExcelTools
import ILDLockFile
from openpyxl import Workbook
from openpyxl import load_workbook


_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "dbd-prod"
_do_test = False

_genmeta = None
_prodpara_excel = None
_allsheets = None


# ######################################################
def open_logbook():
    ''' open elog logbook '''
    global _logbook, _elog_url, _elog_subdir, _do_test
   
    if _logbook == None:
        lines = open(os.environ["MY_ELOG_KEY"]).readlines()
        ( auser, apass ) = lines[0][:-1].split(' ')

        _logbook = elog.open("%s/%s/" % (_elog_url, _elog_subdir), user=auser, password=apass)

    return 

# ######################################################
def get_elog_data(an_elogid):
    ''' Obtain one record of elog data '''
    global _logbook, _elog_url, _elog_subdir, _do_test
    
    open_logbook()
    ( message, attributes, attachements) = _logbook.read(an_elogid)

    # pprint.pprint(attributes)
    res = {"attributes":attributes}
    for dlurl in attachements:
       if "dstm-nevents-summary.txt" in dlurl:
           if hasattr(ssl, '_create_unverified_context'):
               ssl._create_default_https_context = ssl._create_unverified_context
           #res["nevts"] = []
           dirinfo = []
           for aline in urlopen(dlurl):
               if len(aline) < 2 : 
                  continue
               # print aline
               if aline[:12] == "## Directory":
                  words = aline.replace('\n','').replace("  "," ").split()
                  dirname = words[3]
                  dirinfo.append({"dirname":dirname})
                  # print "dir="+ dirname
                  # res["nevts"].append(dirname)
                  dirinfo[-1]["dstm"] = []
               elif aline[0:10] != "    ProcID":
                  # print "  = " + aline
                  dstms = aline.replace('\n','').split()
                  dirinfo[-1]["dstm"].append({"ProcID":dstms[0], "NFile":dstms[1], 
                        "NEvents":dstms[2], "Filename":dstms[3]})

           res["files"] = dirinfo

    # Scan text part of elog data
    prodinfo={}
    for line in message.split('\n'):
       if ": sim production," in line:
          words = line.split(',')
          prsim = words[1:-1]
          prodinfo["sim"] = prsim
       elif ": rec ovl production," in line:
          words = line.split(',')
          provl = words[2:-1]
          prodinfo["ovl"] = provl
       elif ": rec nobg production," in line:
          words = line.split(',')
          provl = words[2:-1]
          prodinfo["nobg"] = provl

    res["prod"] = prodinfo
    return res

# ######################################################
def reform_elogdata(elogentry):
    ''' analize eglog data further for easy output to web '''
    outdata = elogentry

    if not os.path.exists("dstm-info"):
       os.mkdir("dstm-info")
    
    outdata = {}    

    for files in elogentry["files"]:
       dirkeys = files["dirname"].split("/")
       ildconfig = dirkeys[-1]
       model = dirkeys[-2]
       evttype = dirkeys[-3]
       (energy, machine ) = dirkeys[-4].split("-")
       datatype = dirkeys[-5]

       # print "++++"
       # print model+" "+evttype+" " + ildconfig
       # pprint.pprint(files["dstm"])
       for dstms in files["dstm"]:
          filekeys = {}
          for val in dstms["Filename"].split("."):
             if val != "slcio" and val != "*":
                 filekeys[val[0]] = val[1:]
               
          procname = filekeys["P"]
          (ecm, machine) = filekeys["E"].split('-')
          beampol = "e"+filekeys["e"]+".p"+filekeys["p"]
          dtypes = filekeys["d"].split("_")
          recid = dtypes[2]
          dmodel = filekeys["m"]
          procid = dstms["ProcID"]          

          outfile = "dstm-info/%s.m%s.P%s.txt" % ( ildconfig, dmodel, procid )
          if not os.path.exists(outfile):
              findcmd = "dirac-dms-find-lfns Path=%s DetectorModel=%s ILDConfig=%s GenProcessID=%s > %s 2>&1 " % \
                   ( files["dirname"], dmodel, ildconfig, procid, outfile)
              print( "Execute "+findcmd )
              lcmd = subprocess.check_output(findcmd, shell=True, stderr=subprocess.STDOUT)
              # print "  command return was "+lcmd

          dstmfiles = []
          outfiledata = open(outfile).readlines()  
          # pprint.pprint(outfiledata)

          for line in outfiledata:
            dstmadd=line.replace("\n","")
            if len(dstmadd) > 0:
                dstmfiles.append(dstmadd)
          if len(dstmfiles) == 0:
             print( "ERROR : no DSTM files found in "+outfile+". Probably directory meta information was not set property" )
             print( "dstm-meta-set.sh is an example to create command for dirac-dms-filecatalog-cli utility" )
             print( "and remove files in a directory dstm-info before re-run this command" )
             exit(20)

          outkey = "%s.m%s.P%s" % ( ildconfig, dmodel, procid)
          outdata[outkey] = { "ProcID":dstms["ProcID"], 
              "EvtType":evttype, "ProcName":procname, "BeamPol":beampol, "ILDConfig":ildconfig, 
              "RecID":recid, "NFile":dstms["NFile"], "NEvents":dstms["NEvents"], "dstm-info":outfile, 
              "DetectorModel":dmodel, "ecm":int(ecm), "machine":machine,
              "ndstm":len(dstmfiles), "dstm_files":dstmfiles, "Datatype":"DSTM"}

          xsect = float(_genmeta[dstms["ProcID"]]["cross_section_in_fb"])
          intlum = float(dstms["NEvents"])/xsect if xsect > 0.0 else 0.0
          outdata[outkey]["xsect"] = xsect
          outdata[outkey]["intlumi"] = intlum


          totev_submit = _allsheets["byProcID"]["I"+dstms["ProcID"]]["totev_submit"]
          outdata[outkey]["totev_submit"] = totev_submit

    return outdata

# =======================================================
if __name__ == "__main__":


    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description='''\
    Create production summary from ELOG. (outjson), web html(webhtml) and data for opt-data database(elogdbs)
    Example:
        ./elog_summary.py 240-246 --prodpara ../initdirs/prodpara-ng-lowmee.xlsx --elogdata_dir elog-dbd-prod
    ''')
    parser.add_argument("elog_ids", help="ElogIDs of dbs-prod database which are summarized. 1 ID or range using -IDs")
    parser.add_argument("--ildconfig", help="ILDConfig of samples", dest="ildconfig", action="store", default="v02-00-01")
    # parser.add_argument("--elogdata", help="Json file where Elog summary is written", 
    #     dest="elogdata", action="store", default="elog-dbd-prod.json")
    parser.add_argument("--elogdbs", help="Json file to be used by write-opt-data.py", 
        dest="elogdbs", action="store", default="elogdbs.json")
    parser.add_argument("--replace", help="Replace json fileif the same key exists.", 
	dest="replace", action="store_true", default="false")
    parser.add_argument("--genmeta", help="genmeta data file", dest="genmetaByID", action="store", 
        default="https://ild.ngt.ndu.ac.jp/CDS/files/genmetaByID.json")
    #     default="/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta/genmetaByID.json")
    parser.add_argument("--webhtml", help="html file name for output", dest="webhtml", action="store", 
        default="summary-by-evttype.html")
    parser.add_argument("--prodpara", help="prodpara excel file. summary sheet in the excel file is used to get the number of event planed.",
        dest="prodpara", action="store", default="prodpara.xlsx")
    parser.add_argument("--elogdata_dir", help="Output each elog data separately to the directory given here.", 
        dest="elogdata_dir", action="store", default="elog-dbd-prod")



    args = parser.parse_args()
    
    elogids = args.elog_ids
    # elogid_list = [elogids]
    if "-" in elogids:
      (minid, maxid) = elogids.split("-")
      elogid_list = range(int(minid), int(maxid)+1)
    else:
      elogid_list=[int(elogids)]

    print( "Scan elog data from ID %d to %d " % (elogid_list[0], elogid_list[-1]) )

    ildconfig = args.ildconfig    
    # elogdata_json = args.elogdata
    elogdbs_json = args.elogdbs
    genmetaByID = args.genmetaByID
    _prodpara_excel = args.prodpara
   
    replace = args.replace
    elogdata_dir = args.elogdata_dir

    fmeta = None
    if genmetaByID[0:len("http://")] == "http://":
        fmeta = urlopen(genmetaByID)

    elif genmetaByID[0:len("https://")] == "https://":
        if hasattr(ssl, '_create_unverified_context'):
            ssl._create_default_https_context = ssl._create_unverified_context
        fmeta = urlopen(genmetaByID)

    else:
        fmeta = open(genmetaByID)

    _genmeta = json.load(fmeta)
    fmeta.close()
    print( "genmeta file was loaded from " + genmetaByID )

    sheetinfo = [{"sheet":"byProcID", "keycol":"process_id"}, {"sheet":"summary", "keycol":"sortkey"}]
    if not os.path.exists(_prodpara_excel):
        print( "ERROR : " + _prodpara_excel + " does not exist." )
        exit(20)

    _allsheets = ExcelTools.load_excel_sheets(_prodpara_excel, sheetinfo)
    open_logbook()

    elogdbs = {}
    for elogid in elogid_list:
       print( "### Getting Elog data of id " + str(elogid) + " from the ELOG web serber." )
       res = get_elog_data(elogid)
       # pprint.pprint(res)
       if "files" in res:
           elogdata = reform_elogdata(res)
           # pprint.pprint(elogdata[elogid])
           for k, v in elogdata.items():
               dbskey = "t%s.I%s" % (v["RecID"], v["ProcID"])
               elogdbs[dbskey] = v
               elogdbs[dbskey]["elogid"] = elogid 

           elogdata_json = elogdata_dir + "/elog-dbd-prod-%s.json" % str(elogid)
           json.dump(elogdata,open(elogdata_json,"w"))

    json.dump(elogdbs, open(elogdbs_json,"w"))

    exit(0)


