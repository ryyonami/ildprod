#!/bin/env python

import os
import json
import glob
import pprint
import argparse

# ##############################################
# def get_genfile_list(dirname):


# ##############################################
if __name__ == "__main__":

   prodjson = "production.json"
   if os.path.exists(prodjson):
      prjson = json.load(open(prodjson))
   else:
      print("### production.json does not exist. Currently at " + os.getcwd())
      exit(15)
     

   rkeep = -1.0
   parser = argparse.ArgumentParser(description="Decide whether to keep REC file or not for each gensplit file.")
   parser.add_argument("-r",help="Keep rate. Default is from production.json. <1 for a fraction of total gensplit files. >=1 for original generator files.",
          dest="rkeep", action="store", default="-1")
   parser.add_argument("--norec", help="No REC files are saved.", dest="norec", action="store_true", 
          default="false")
   parser.add_argument("--maxkeep", help="Max number of REC files to keep.", dest="maxkeep", action="store", 
          default="500")

   args = parser.parse_args()
   rkeep = float(args.rkeep)
   norec = args.norec 
   maxkeep = long(args.maxkeep)  

   if rkeep < 0.0:
        if "__RECRATE" in prjson:
           rkeep = float(prjson["__RECRATE"])

   if not norec and rkeep < 0.0:
      print("ERROR in set_selected_file.py : __RECRATE is not defined in production.json and not given in argument.")
      exit(-1)

   pid_nbsplits = {}
   for splits in prjson["____PID_NbSplits"].split(","):
      temp = splits.split(":")
      pid_nbsplits[temp[0]] = temp[1]

   print("### Executing set_selected_file.py to make a list of gensplit files. rec_keep_rate="+str(rkeep))
   procids = {}
   for d in glob.glob("I*/lciosplit.json") + glob.glob("*/I*/lciosplit.json"):
      print("reading "+d)
      procid = d.split("/")[-2].split(".")[0]
      fseq = int(d.split("/")[-2].split(".")[1])
      lciosplits = json.load(open(d))
      ofiles = lciosplits["OutFiles"]
      if procid not in procids:
         procids[procid] = []
      for data in ofiles:
         procids[procid].append([fseq, int(data["split_number"]), data["file_name"] ])     

   metadata = {}
   for key in procids:
      files = sorted(procids[key])

      nfiles = len(files)
      outvec = []
      if not norec:
          for kf in range(0, nfiles):
            outvec.append(files[kf] + ["dstonly"])
      else:
          if rkeep < 1.0:
            nbsplit = pid_nbsplits[key[1:]]
            nkeep = int(float(nbsplit)*float(rkeep))
            if nkeep > maxkeep:
               nkeep = maxkeep
            nkeep = 1 if nkeep < 1 else nkeep
            nkeep_r = nkeep if nkeep < nfiles else nfiles
            print("### PID=%d : %d(%d now) rec files out of %d(%d now)" % (int(key[1:]), int(nkeep), int(nkeep_r), int(nbsplit), int(nfiles))+" gensplit files will be kept.")
            for kf in range(0, nkeep_r):
               outvec.append(files[kf] + ["recdst"])
            if nfiles > nkeep_r :
              for kf in range(nkeep_r, nfiles):
                 outvec.append(files[kf] + ["dstonly"])
          else:
            noutser = [files[0][0]]
            nkeep = int(rkeep)
            print("### PID=%d : rec files corresponding to %d generator files" % (int(key[1:]), int(nkeep))+" will be kept.")
            for kf in range(0, nfiles):
               iser = files[kf][0]
               if iser not in noutser and len(noutser) < nkeep:
                  noutser.append(iser)
               if iser in noutser:
                  outvec.append(files[kf] + ["recdst"])
               else:
                  outvec.append(files[kf] + ["dstonly"])
                  
      metadata[key] = outvec

#   pprint.pprint(metadata, width=140)
   json.dump(metadata,open("select_reckeep_byPID.json","w"))
   print("### A file for setting SelectedFile of SIM was saved in select_reckeep_byPID.json")


        
