#!/usr/bin/env python 
# Prepare log upload
# 

from __future__ import print_function
import os
import glob
import subprocess
import argparse
import shutil
import datetime
import json

# #####################################################################
def logfiles_dl_n_targz(json_file, no_confirm, isTest):
  no_confirm_new = no_confirm
  param = json.load(open(json_file))
  prtype = 'sim'
  prmeta = param['rec']
  topdir = param['conf']['__DIRECTORY_SIMLOG']
  log_se = param['conf']['__LOG_SE'] if '__LOG_SE' in param['conf'] else "DESY-SRM"
  if 'dstm' in param:
    prmeta = param['dstm']
    prtype = 'rec'
    topdir = param['conf']['__DIRECTORY_RECLOG']
    
  prodid = prmeta['meta']['ProdID']
  detector = prmeta['meta']['DetectorModel']
  if isTest:
    topdir = topdir.replace('/ilc/prod/ilc/mc', '/ilc/prod/ilc/test/ild/mc')
    log_se = 'DESY-SRM'

  prm_m=prmeta['meta']
  log_ul_dir = '%s/%s-%s/%s/%s/%s' % ( topdir, prm_m['Energy'], prm_m['MachineParams'], prm_m['EvtType'], \
               prm_m['DetectorModel'], prm_m['ILDConfig'] )

  print( '### Download log files of detector %s, %s production of ID %s ' % ( detector, prtype, str(prodid)) )
  print( '    json parameter file : %s ' % json_file )
  print( '    Log output dir : %s ' % log_ul_dir )
  print( '    Log output SE  : %s ' % log_se )

  postprod_log = "postprod-%s-%s.log" % ( str(prodid), detector )
  dlcmd = 'postprod-new.sh %s > %s 2>&1 ' % ( prodid, postprod_log )  
  now = datetime.datetime.now()
  print( "### Downloading log files for %s by postprod-new.sh command : %s " % ( json_file.replace('.json',''), now) )
  print( "cmd=%s" % dlcmd )
  if not no_confirm_new:
    print( 'continue ? (y/n/c) : ', end="")
    ans = raw_input().lower()
    if ans[0] == 'c':
      no_confirm_new = True 
    if ans[0] not in ['y', 'c']:
      exit(0)

  dlcmd_out = subprocess.check_output(dlcmd, shell=True, stderr=subprocess.STDOUT)
  print(dlcmd_out)
  #
  # move, rename, commands
  logdir_top = 'prod%s' % str(prodid)
  if os.path.exists( postprod_log ):
    shutil.move(postprod_log, '%s/%s' % ( logdir_top, postprod_log ) )
  origdir = os.getcwd()
  upload_list = []
  os.chdir( logdir_top )

  print( 'Log file download completed. Next step is to create tar.gz files.' )
  if not no_confirm_new:
    print( 'continue ? (y/n/c) : ', end="")
    ans = raw_input().lower()
    if ans[0] == 'c':
      no_confirm_new = True
    if ans[0] not in ['y', 'c']:
      exit(0)

  for f in glob.glob('*.log')+glob.glob('*.sh'):
    shutil.move(f, 'log/000')
  os.chdir('log')
  for d in glob.glob('[0-9]*'):
    logdir_now='%s-%s' % ( logdir_top, str(d) )
    shutil.move(d, logdir_now)
    targz_cmd='tar zcf %s.tar.gz %s ' % ( logdir_now, logdir_now )
    upload_list += ['%s/%s.tar.gz ' % ( log_ul_dir, logdir_now) + '%s/log/%s.tar.gz ' % ( logdir_top, logdir_now) + log_se ]
    print( 'doing ... %s ' % targz_cmd )
    targz_out = subprocess.check_output( targz_cmd, shell=True, stderr=subprocess.STDOUT)
    print( targz_out )

  os.chdir(origdir)

  return { 'OK':True, 'List':upload_list, 'no_confirm':no_confirm_new }

# ###############################################################################
if __name__ == '__main__':

  default_glob = '../4_subprod/[rec|sim]*.json'
  parser = argparse.ArgumentParser(description="Download rec/sim job logs and upload them to DIRAC. default_glob could be ../4_subprod/rec*.json")
  parser.add_argument("-d", help="Glob string to search input json files.", dest='glob_string', action="store", default=default_glob)
  parser.add_argument("-v", help="verbercity", dest="verb", action="store_false", default=True)
  parser.add_argument("--test", help="test mode, upload to test directory", dest="isTest", action="store_true", default=False)
  parser.add_argument("--no_confirm", help="proceed without any confirmation to terminal", dest="no_confirm", action="store_true", default=False)
  parser.add_argument("-l", "--list",help="List of json files of producton jobs.", dest="jsonlist", action="store", default="")

  args = parser.parse_args()
  origdir = os.getcwd()
  glob_string = args.glob_string
  isTest = args.isTest
  no_confirm = args.no_confirm

  # Remove _12345_ from the list  
  list_of_json=[]
  if args.jsonlist != "":
     for line in open(args.jsonlist):
        list_of_json.append(line[:-1])
  else:
     list_of_json = glob.glob(glob_string)
  
  fndel = ''
  for fn in list_of_json:
    if fn.find('prod_12345_') != -1:
      fndel = fn
  if fndel != '':
    list_of_json.remove(fndel)

  print( "### get production information from following files." )
  print( list_of_json )
  
  ul_list_unsort = []
  # Download log files and create tar gz of them for upload
  for json_file in list_of_json:
    ret = logfiles_dl_n_targz(json_file, no_confirm, isTest)
    if not ret['OK']:
      print( 'Error to exec logfiles_dl_n_targz for %s ' % json_file )
      exit(1)
    ul_list_unsort += ret['List']
    no_confirm = ret['no_confirm']

  # Write command to upload files.
  ul_list = sorted(ul_list_unsort)
  flist = open('log_upload_list.txt', 'w')
  flist.write('\n'.join(ul_list)+'\n')
  flist.close()

  bash_cmd = [ "#!/bin/bash",
               'dirac-dms-add-file -ddd log_upload_list.txt 2>&1 | tee log_upload_list.log',
	       'if [ $? -ne 0 ] ; then ',
               '  echo "ERROR dirac-dms-add-file did not completed with non 0 return status" | tee -a log_upload_list.log',
               'fi',
               'echo "### log upload completed. `hostname` `date`" | tee -a log_upload_list.log']
              
  fbash = open('log_upload_list.sh','w')
  fbash.write('\n'.join(bash_cmd)+'\n')
  fbash.close()
  os.chmod('log_upload_list.sh',0755)
  hostname = os.environ["HOSTNAME"] if "HOSTNAME" in os.environ else os.uname()[1]
   
  print( '### all done : log_upload_list.sh was created. At ' + hostname + ' date/time ' + str(datetime.datetime.now()) )

  exit(0)
 
               



