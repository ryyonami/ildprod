#!/bin/env python
#
# Update json file produced by rec-production job submission when DSTOnly mode,
# to save Production IDs of dstonly production and recanddst production in each 
# json files.
#
from __future__ import print_function
import os
import json
import argparse
import datetime
import shutil

# ============================================================

if __name__ == "__main__":

    msg="\n".join(["In the case of DST-only production, add production ID of REC&DST production",
                   "and DST only production to each re-production json files."])
    parser = argparse.ArgumentParser(description=msg, 
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("json1", help="first rec-production json file")
    parser.add_argument("json2", help="second rec-production json file")
    parser.add_argument("--selected_file_recdst", 
          help="SelectedFile value for RECDST(keep REC) reconstruction. default=2",
          dest="selected_file_recdst", action="store", default="2")
    parser.add_argument("--selected_file_dstonly", 
          help="SelectedFile value for DSTONLY(rm REC) reconstruction. default=3",
          dest="selected_file_dstonly", action="store", default="3")

    args = parser.parse_args()

    print( args.json1 )
    print( args.json2 )
    

    injson = [ {"name":args.json1}, {"name":args.json2} ]
    selected_file_recdst  = args.selected_file_recdst
    selected_file_dstonly = args.selected_file_dstonly

    outjson = []

    rectype = {}
    recdst = {}
    for jsfile in injson:
        filename=jsfile["name"]
        newjs = {"name":filename}
        js = json.load(open(filename))
        selectedfile = js["rec"]["meta"]["SelectedFile"]
        prodid = js["status"]["ProdID"]
        newjs.update({"selected":selectedfile, "ProdID":prodid, "json":js})
        outjson.append(newjs)
        if int(selectedfile) == selected_file_recdst:
           recdst["RECDST"]={"ProdID":prodid, "jsonfile":filename, "SelectedFile":selectedfile}
        elif int(selectedfile) == selected_file_dstonly:
           recdst["DSTOnly"]={"ProdID":prodid, "jsonfile":filename, "SelectedFile":selectedfile}
        else:
           recdst["ProdID_Unknown"]=prodid
        
    now = datetime.datetime.now()
    dtstr = "%4.4d%2.2d%2.2d-%2.2d%2.2d%2.2d" % (now.year, now.month, now.day, now.hour, now.minute, now.second )
    backdir = "recprod-back-%s" % dtstr 
    os.makedirs(backdir)
    for jsfile in injson:
        mvfile=jsfile["name"]
        shutil.move( mvfile, backdir)
        print( "Original "+mvfile+" was moved to "+backdir )

    for jsinfo in outjson:
        jsfile=jsinfo["json"]
        jsfile["rec"]["recdst"]=recdst
        outfile=jsinfo["name"]
        json.dump(jsfile,open(outfile,"w"))
        print( "REC/DST information was added to "+outfile )

    
