#!/usr/bin/env python
#
import os
import argparse
import glob
import elog
from ElogTools import *
import pprint
import datetime
import copy

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Create a production directory from the existing one.")
    parser.add_argument("origdir",help="Original production directory using which new directories are created.")
    parser.add_argument("-s",help="Sim detector model to be re-used, eigther l5, s5, l5:s5", dest="simdetector",
                             action="store", default="l5:s5")
    parser.add_argument("-o",help="Reconstruction option to be produced.", dest="recoption", 
                             action="store", default="o1")
    parser.add_argument("-p",help="Production type to run, ovl, nobg, nobg:ovl", dest="prodtype",
                             action="store", default="ovl")
    parser.add_argument("-d",help="If specified, run do_test mode", dest="do_test",
                             action="store_true", default=False)
    parser.add_argument("--outdir", help="Output directory, where 4_subprod, 6_dstmerge, etc are created.",
                             dest="outdir", action="store", default="")
    parser.add_argument("--no_elog", help="Run without writing to ELOG. For debugging.",
                             dest="no_elog", action="store_true", default=False)

    args = parser.parse_args()
    
    do_test = args.do_test
    origdir = "../6f_ttbartest.bWW"
    origdir = args.origdir
    no_elog = args.no_elog
    jsonprod = json.load(open(origdir+"/1_prepProd/production.json"))
    simdir = origdir+"/4_subprod"
    if not os.path.exists(simdir):
       print( "ERROR : %s does not exist." % simdir )
       exit(16)

    outdir = ( args.outdir + "/" ).replace("//","/")
    print( "Creating directories in %s for rec-only production." % outdir )
    print( "Getting simulation information from " + simdir )
    simdetector = "s5" # simdetector should be "l5", "s5", "l5:s5"
    recoption = "o1"
    prodtype = "ovl"
    simdetector = args.simdetector
    # if ":" in args.simdetector:
    #    simdetector=args.simdetector.split(":")
    recoption = args.recoption
    prodtype = args.prodtype
    print( "-- simdetector " + str(simdetector) )
    print( "-- recoption " + recoption )
    print( "-- prodtype " + prodtype )

    # correct 
    jsonprod["__DIRECTORY_SIMLOG"] = jsonprod["__DIRECTORY_SIMLOG"].replace("dsk.dsk","dsk")
    jsonprod["__DIRECTORY_RECLOG"] = jsonprod["__DIRECTORY_RECLOG"].replace("dsk.dsk","dsk")
    jsonprod["__WORKDIR"] = os.getcwd() + "/1_prepProd"

    # Found prodID of previous production.
    simpids = []
    simjsons = []
    simprod_jsons = glob.glob(simdir + "/simprod_*.json")
    if len(simprod_jsons) == 0:
        print( "ERROR : simprod_*.json files not found in %s" % simdir )
        exit(16)

    for sp in simprod_jsons:
        simjfile = os.path.basename(sp)
        (simprod, simpid, model, vers) = simjfile.split(".")[0].split('_')
        print( sp )
        if simpid != "12345" :
            if model in simdetector:
                simpids.append(simpid)
                simjsons.append(simjfile)

    if len(simpids) == 0:
        print( "ERROR: no matching simprod_*.json file found for sim_detector %s" % str(simdetector) )
        exit(16)

    oldelogid = jsonprod["__ELOG_ID"]
    oldelog = getelog(oldelogid)
    oldsimid = ",".join(simpids)
    # pprint.pprint(oldelog["attributes"])
    print( "Obtained ELOG data of ID " + str(oldelogid) + "for original production data" )

    newatt = copy.deepcopy(oldelog["attributes"])
    newatt["JobStatus"] = "preparation"
    newatt["Log_RecJob"] = ""
    newatt["Log_SimJob"] = ""
    newatt["RecID"] = ""
    # newatt["SimID"] = ",".join(simpids)
    newatt["SimID"] = ""
    # print newatt["Detector"]
    if simdetector.replace("5:","") not in newatt["Detector"]:
       print( "ERROR: detector model requested is not same as the requested Elog data (%s). Please correct input." % (simdetector, newatt["Detector"]) )
       exit(15)

    ### TODO
    print( "Following records are written to ELOG" )
    pprint.pprint(newatt)

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)
    hlink = "<a href=\"https://ild.ngt.ndu.ac.jp/elog/dbd-prod/%d\">%d</a>" % ( int(oldelogid), int(oldelogid) )
    msglist = ["<ul>",
               "<li>%s %s " % ( dstr, tstr ) + 
               ": New elog entry was crated for re-reconstruction using sim. production (SimID=%s) of Elog " % oldsimid + 
               hlink + "</li>" ,
               "</ul>"]
    message = "\n".join(msglist)

    elog_data = None
    if not do_test:
        if not no_elog:
            elogid = putelog_by_attrib(message, newatt) 
        else:
            elogid = int(oldelogid) + 1000     
            print( "--no_elog is requested.  ELOG data is not registered. Dummy ELOG ID %s was assigned." % str(elogid) )
            elog_data = { "message":message, "attrib":newatt }
    else:
        elogid = int(oldelogid) + 1000
    print( "New ELOG entry for reconstruction only production to ELOG ID "+str(elogid) )

    print( "Creating sub-directories and relevant files." )
    jsonprod["__ELOG_ID"] = int(elogid)
    if not os.path.exists(outdir + "1_prepProd"):
        os.mkdir(outdir + "1_prepProd")

    json.dump(jsonprod, open(outdir + "1_prepProd/production.json","w"))
    print( "production.json was created in " + outdir + "1_prepProd" )
    if elog_data != None:
        json.dump(elog_data, open(outdir + "1_prepProd/elogdata.json","w"))
        print( "New elog data was written to 1_prepProd/elogdata.json" )

    if not os.path.exists(outdir + "4_subprod"):
        os.mkdir(outdir + "4_subprod")
    os.chdir(outdir + "4_subprod")
    scriptsdir = os.environ["SCRIPTS_DIR"]
    for fsh in ["ILDProdBase.py"]:
        os.symlink(scriptsdir + "/"+fsh, fsh)

    os.symlink("../1_prepProd/production.json", "production.json")
    for sj in simjsons:
        sjbase = os.path.basename(sj)
        os.symlink(simdir + "/" + sjbase, sjbase)
    bashfile = ["#!/bin/bash", "task_step4_main.sh -r %s --ptype %s" % (recoption, prodtype)]
    fout = open("task_step4.sh","w")
    fout.write("\n".join(bashfile))
    fout.close()
    os.chmod("task_step4.sh", 0755)
    os.chdir("../")
    
    if not os.path.exists(outdir+"6_dstmerge"):
        os.mkdir(outdir+"6_dstmerge")

    if not os.path.exists(outdir+"7_logupload"):
        os.mkdir(outdir+"7_logupload")
  


