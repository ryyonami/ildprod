import glob
import pprint
from ILD_DSTM_Submit import *

if __name__ == "__main__":

  for d in glob.glob("dstmjobs-*-ILD*/dstm.I250050.n004.j2[56]"):
    jobtop=d.split("/")[0]
    jobmaker_full=glob.glob(jobtop+"/dstm-jobmaker-*.json")[0]
    dstmmaker_json=os.path.basename(jobmaker_full)

    dirselect = d
    jobmeta = set_params(jobtop, dstmmaker_json)
    # do_job_submit(jobmeta, dirselect, do_local=True)
    do_job_submit(jobmeta, dirselect)

