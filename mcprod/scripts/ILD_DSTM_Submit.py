
from __future__ import print_function

from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin
from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
import pprint
import glob
import os
import json
import sys

# ##################################################################
def getInputDataList(list_file):
  '''
  Build a list object of DST file name from a file.
  '''
  dsts=[]
  for adst in open(list_file):
    dsts.append(adst.replace('\n',''))

  return dsts

######################################################
def readConfig(conffile):
  ret = {}
  for line in open(conffile):
    if "=" in line:
      (key, val) = line[:-1].split("=",1)
      ret[key] = val
  return ret

# ######################################################
def submit_job(diracilc, jobmeta, debug=False):
  prodid = jobmeta["ProdID"]
  job_name = "dst-merge"
  if "job_name" in jobmeta:
    job_name = jobmeta["job_name"]
  if "JobNumber" in jobmeta:
    jobnb = jobmeta["JobNumber"]
    job_name = "merge%d-%3.3d" % (int(prodid), int(jobnb))  

  job_group = "ILD-DSTM-prod%d" % int(prodid)

  print( "job_group="+job_group + " job_name=" + job_name )
  # print "inputSandbox="
  # print jobmeta["inputSandbox"]

  dstlist_file = jobmeta["list_of_dstfiles"]
  dstlist = getInputDataList(dstlist_file)

  j = UserJob()
  j.setJobGroup(job_group)
  j.setName(job_name)
  j.setInputData(dstlist)
  j.setInputSandbox(jobmeta["inputSandbox"])
 
  if "Destination" in jobmeta:
    j.setDestination(jobmeta["Destination"])

  #  Define application     
  app = GenericApplication()
  app.setScript(jobmeta["jobScript"])
  res = j.append(app)
  if not res['OK']:
    print( "Failed to append %s to job %s" % (jobmeta["jobScript"], job_name) )
    print( res['Message'] )
    exit(1)

  j.setOutputSandbox( jobmeta["outputSandbox"])
  j.dontPromptMe()
  
  res = None
  if debug:
    res = j.submit(diracilc, mode='local')
#    res={"OK":True, "Value":0000}
  else: 
#    res={"OK":True, "Value":0000}
     res = j.submit(diracilc)
  if not res['OK']:
    print( "Failed to submit job %s " % (job_name) )
    pprint.pprint(res)
    exit(1)

  print( "A job %s submitted. jobid=%s" % (job_name, str(res['Value'])) )

  jobinfo = {"JobGroup":job_group, "JobName":job_name, "jobid":res['Value'] } 
  jobmeta.update({"submit":jobinfo})

  fjson = open("submit.json","w")
  json.dump(jobmeta, fjson)
  fjson.close()
  print( "Job info. was written to submit.json" )

  del j

  return {"OK":True, "Value":jobmeta}

# #################################################################
def set_params(jobtop, dstmjson):

  inputSandbox=["dirac-ildapp-dstmerge.sh"]
  inputSandbox.extend(["dstlist.txt", "jobinfo.txt", "marlin_merge.xml.in"])
  prodtools = os.getenv("MYPROD_TOOLS")
  inputSandbox.extend([prodtools])
  outputSandbox=["*.out","*.log","*.xml","*.sh","*.py","*.txt", "*.cli"]

  jobscript="dirac-ildapp-dstmerge.sh"
  
  dstmjson = json.load(open(jobtop+'/'+dstmjson))
  prodid = dstmjson['dstm']['meta']['ProdID'] 
 
  #if "DSTM_JOB_DESTINATION" not in os.environ:
  #   jobDestination=["LCG.DESY-HH.de", "LCG.CERN.ch"]
  #elif len(os.environ["DSTM_JOB_DESTINATION"]) > 0:
  #   jobDestination=os.environ["DSTM_JOB_DESTINATION"].split(":") 
  #
  jobDestination=["LCG.DESY-HH.de", "LCG.CERN.ch"]

  topdir=os.getcwd()
  jobmeta = {"topdir":topdir, "jobdir":"dummy", 
             "ProdID":prodid, "job_name":"dst-merge", 
             "inputSandbox":inputSandbox, "outputSandbox":outputSandbox, 
             "jobScript":jobscript,
             "Destination":jobDestination, 
             "list_of_dstfiles":"dstlist.txt"}

  print("Job destination of DST merge job is "+",".join(jobDestination)) 
  
  return jobmeta

# ################################################################
def do_job_submit(jobmeta, dirselect, do_local=False):
  topdir = os.getcwd()
  d = None

  for jobdir in sorted(glob.glob(dirselect)):
    dirlevel = jobdir.split('/')
    if d == None:
      repofile = topdir + '/' + dirlevel[0] + '/repo.rep'  
      d = DiracILC(True, repofile) 
       
    jseq = dirlevel[-1]
    jobmeta['job_name'] =  jseq.replace('.', '-')   
    jobmeta['jobdir'] = jobdir
    os.chdir(jobdir)
  
    print( jobdir )
    if not do_local:
      subres = submit_job(d, jobmeta)  
    else:
      subres = submit_job(d, jobmeta, debug=do_local)
    if not subres["OK"]:
      print( "Failed to submit job" )
      exit(1)
  
    os.chdir(topdir)

###################################################################
# Start of main part
####################################################################
if __name__ == '__main__':
 
#  jobtop = "jobs"
#  dstmmaker_json = "ild-dstm-job-maker.json"
#  dirselect = jobtop + "/dstm-*"

#  jobmeta = set_params(jobtop, dstmaker_json)
#  do_job_submit(jobmeta, dirselect)
  
   sysarg = sys.argv
   if len(sysarg) != 3: 
     print("Usage   : python ILD_DSTM_Submit.py <jobtop> <job_number>")
     print("Function: Submit one DST merge job.")
     print(" ")
     print("<jobtop> : Job directory, dstmjobs-15156-ILD_l5_o1_v02 for example.")
     print("<job_number> : Job number. Searched by <jobtop>/dstm.I*.n*.j<job_number>")
     exit(-1)

   jobtop = sys.argv[1]
   job_number = sys.argv[2]

   dstmmaker_json_v = glob.glob(jobtop+"/dstm-jobmaker-*.json")
   if len(dstmmaker_json_v) != 1:
      print("Error ... Found %d dstmmaker_json found in %s" % (len(dstmmaker_json_v), jobtop))
      exit(-1)

   dirselect = jobtop + "/dstm.I*.n*.j%d" % int(job_number)

   subdir=glob.glob(dirselect)[0]
   if os.path.exists(subdir+"/submit.json"):
     print("Error ... DST merge job in %s is not submitted because it had been submitted already." % subdir)
     print("  remove submit.json to re-submit")
     exit(-1)

   dstmmaker_json = os.path.basename(dstmmaker_json_v[0])
   print("Submitting DST merge job in %s" % subdir)

   jobmeta = set_params(jobtop, dstmmaker_json)
   do_job_submit(jobmeta, dirselect)

    

