#!/bin/bash 
#

# . ${SCRIPTS_DIR}/prod.bashrc
# #########################################
# Utility functions
# #########################################
my_abort ()
{
  echo "$@" 1>&2
  exit 1
}


check_proxy()
{
  echo "**** Checking proxy info..."
  proxy_register=`dirac-proxy-info | grep "VOMS"`
  if [ "x${proxy_register}" == "x" ]; then
    echo "**** dirac-proxy-init is not initiated."
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    exit
  fi
  proxy_group=`dirac-proxy-info | grep "DIRAC group" | cut -d ':' -f 2 | sed s/\ //g`
  if [ "x${proxy_group}" == "xilc_prod" ]; then
    echo "**** Production role ${proxy_group} is selected."
  else 
    echo "**** Selected role is ${proxy_group}"
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    echo "Exit"
    exit
  fi
}

print_pids()
{
  ipos=0
  cat prodids.list | while read line ; do 
    if [ ${line:0:1} == "#" ] ; then
      ipos=$[${ipos}+1]
      [ ${ipos} -gt 1 ] && echo ""
      echo -n "${ipos} $line : "
    else
      echo -n "`echo ${line} | cut -d":" -f1`,"
    fi
  done
  echo " "
}

get_prodtype()
{
  prodid=$1
  prodjson=`grep json prodids.list | grep "_${prodid}_"`
  prtype="unknown"
  if [ "x`echo ${prodjson} | grep simprod`" != "x" ] ; then 
     prtype="sim"
  else
     prtype="rec"
  fi
  echo ${prtype}
}

infile="prodids.list"
donedir="bkg/aa_lowpt.bBW"

if [ ! -e ${infile} ] ; then 
  echo "${infile} does not found in the current directory."
  echo "This command should be executed at \${MCPROD_TOP}"
  exit
fi

prdir=$1

if [ "x${prdir}" == "x" ] ; then 
  echo "done_production.sh [number]"
  echo "function:"
  echo "Stop production running in [number]. Enter number to select directory."
  print_pids
  exit
fi

check_proxy

ipos=$1
# prdir=`grep "#" prodids.list | sed -n -e "${ipos}p" | cut -d" " -f2` 
prdir0=`print_pids | sed -n -e "${ipos}p"`

echo "Stopping production running in ${prdir0}"
prdir=`echo ${prdir0} | cut -d" " -f3`
echo ${prdir}

donedir=`echo ${prdir} | sed -e "s|ProdTask/||"`

[ -e ProdTask/${donedir} ] || my_abort "${donedir} does not exist"
dstr=`date +%Y%m%d-%H%M%S`
donetest="donetest.file"
donefile="donefile.${dstr}.list"
notyetfile="notyetfile.${dstr}.list"
isdone=0
rm -fv ${donefile} ${notyetfile} 2>/dev/null
for line in `cat ${infile} | sed -e "s/ /%/g" ` ; do
  if [ "${line:0:1}" == "#" ] ; then 
    if [ ${isdone} -eq 1 ] ; then 
      isdone=0
    elif [ "x`echo ${line} | grep ${donedir}`" != "x" ] ; then 
      isdone=1
#       donedir=`echo ${line} | sed -e "s/%/ /g" | cut -d" " -f2-`
    fi
  fi
  if [ "${isdone}" == "0" ] ; then 
    echo $line | sed -e "s/%/ /g" >> ${notyetfile}
  else
    echo $line | sed -e "s/%/ /g" >> ${donefile}
  fi
done

ndone=`grep -v "#" ${donefile} 2> /dev/null | wc -l`
if [ "x${ndone}" == "x0" ] ; then 
  echo "No finished production found in ${donefile}"
  cat ${donefile}
  rm -vf ${donefile}
  exit 1
fi

rm -fv do.dstmerge.$$
grep -v "#" ${donefile} | while read line ; do 
  prodid=`echo $line | cut -d":" -f1`
  echo "Stopping production ${prodid}"
  . ${MYPROD_PRODPROD}
  echo "setStatus Stopped ${prodid}" | dirac-transformation-cli 
  prodtype=`get_prodtype ${prodid}`
  if [ "x${prodtype}" == "xrec" ] ; then 
    touch do.dstmerge.$$
  fi
done

cp -v cronexe.list logs/cronexe.list-${dstr}
mv -v prodids.list logs/prodids.list-${dstr}
if [ -e ${notyetfile} ]; then
  mv -v ${notyetfile} prodids.list
fi


if [ -e do.dstmerge.$$ ] ; then 
  # echo "ProdTask/${donedir}/6_dstmerge/task_step6.sh" >> cronexe.list
  echo "ProdTask/${donedir}/6_dstmerge/repeatmon.sh" >> cronexe.list
  echo "production done" > ProdTask/${donedir}/6_dstmerge/prodstatus.log
  rm -fv do.dstmerge.$$
fi
# echo "ProdTask/${donedir}/7_logupload/task_step7.sh" >> cronexe.list
echo "ProdTask/${donedir}/7_logupload/repeatmon.sh" >> cronexe.list
echo "production done" >  ProdTask/${donedir}/7_logupload/prodstatus.log

cp -p active-prod-dir.list logs/active-prod-dir.list-${dstr}
grep -v -w ${donedir} active-prod-dir.list > active-temp
mv active-temp active-prod-dir.list
rm -fv ${donefile}

echo "Production of ${donedir} was stopped on ${dstr} and registered for the post-production task."


