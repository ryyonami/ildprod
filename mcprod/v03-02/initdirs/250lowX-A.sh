#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="250lowX-A"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi


# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
#testname="250-SetA-lowX"
cat > ${prodname}-list.txt <<EOF
aa_4f.bBB
3f.bBW
5f.bBW
3f.bWB
5f.bWB
EOF
# higgs_ffh.bWW.0lep:_${testname}/I106519
# 2f_Z_leptonic.bWW.1lep:_${testname}/I250108

#( cd ${proddir} && rm -rfv *.bWW.[01]lep_${testname} )

export GENSPLIT_DEFAULT_NPROCS=3

# --noelog

cmd="init_production.py --workdir ${proddir} \
  --excel_file  /home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SM_lowX.xlsx \
  --prodlist ${prodname}-list.txt \
  --dstonly --recrate 0.10 \
  --split_nseq_from 0 \
  --sim_nbtasks 100 --rec_nbtasks 100 \
  --production_type sim:ovl "

#  --se_for_data KEK-DISK --se_for_logfiles KEK-DISK \
#  --se_for_dstm_replicates DESY-SRM \
#  --test --multi_dir --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
#  --se_for_dstm_replicates DESY-SRM \
#  --nw_perfile 10 --ngenfile_max 2 --split_nbfiles 5 \
#
echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help 
################################################################################
# Config file /home/ilc/miyamoto/ILDProd/mcprod/testprod/mcprod_config.py was executed.
# usage: init_production.py [-h] [-d SPLITDIR] [--excel_file EXCELFILE]
#                           [--genfile_list GENFILE_LIST] [--workdir WORKDIR]
#                           [--prodlist PRODLIST]
#                           [--split_nbfiles SPLIT_NBFILES]
#                           [--ngenfile_max NGENFILE_MAX] [--test]
#                           [--nw_perfile NW_PERFILE]
#                           [--production_type PROD_TYPE]
#                           [--se_for_data SE_FOR_DATA]
#                           [--elogid_old ELOGID_OLD]
#                           [--reuse_gensplit REUSE_GENSPLIT]
#                           [--sim_nbtasks SIM_NBTASKS]
#                           [--rec_nbtasks REC_NBTASKS]
#                           [--sim_banned_sites SIM_BANNED_SITES]
#                           [--rec_banned_sites REC_BANNED_SITES]
#                           [--gen_data_type GEN_DATA_TYPE]
# 
# Generate subdirectories and scripts to do production
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   -d SPLITDIR, --splitdir SPLITDIR
#                         dir names where splitted generator files are written.
#   --excel_file EXCELFILE
#                         Excel file name, where production information is
#                         obtained.
#   --genfile_list GENFILE_LIST
#                         File name of generator file names
#   --workdir WORKDIR     A directory where production set scripts are written
#   --prodlist PRODLIST   A file containing a list of production groupds.
#   --split_nbfiles SPLIT_NBFILES
#                         Number of splitted files to be produced.
#   --ngenfile_max NGENFILE_MAX
#                         Max number of generator file to use.
#   --test                Scripts for test are generated.
#   --nw_perfile NW_PERFILE
#                         number of events per file in gensplit.
#   --production_type PROD_TYPE
#                         Production type, sim, ovl, nobg. Default is sim:ovl
#   --se_for_data SE_FOR_DATA
#                         Output SE for Sim, Rec, DST, DSTMerged files. Log
#                         files are DESY-SRM
#   --elogid_old ELOGID_OLD
#                         Elog ID referenced, where past production information
#                         is obtained
#   --reuse_gensplit REUSE_GENSPLIT
#                         Use this gensplit_id for new simproduction
#   --sim_nbtasks SIM_NBTASKS
#                         Number of tasks of simulaton production
#   --rec_nbtasks REC_NBTASKS
#                         Number of tasks of reconstruction production
#   --sim_banned_sites SIM_BANNED_SITES
#                         Banned sites for simulation production
#   --rec_banned_sites REC_BANNED_SITES
#                         Banned sites for reconstruction
#   --gen_data_type GEN_DATA_TYPE
#                         generator data type, either stdhep or pairs
