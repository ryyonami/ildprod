#!/bin/bash 
#
#  Create scripts for the production of calibration samples.
#
# This is a script for reconstruction only production, previoustly simulated file as input.
# 3_genslit directory is not created in this case.
#
# If --productionType argument does not include "sim", it is considered as reconstruction only production.
# Json file produced by previous simulation production should be given by --simprod_json argument. 
# --sim_model should be present as well.
#
# This example used --SimSelectedFile 5 option, because only part of simulation data are reconstructed.
# Dirac file catalog meta key, SelectedFile, should be set to 5 manually later for transformation to select
# inputs properly. prodpara/calib-o2.xlsx includes only generator files concerned. The list of them 
# is written in input_genfiles.txt in 1_prepProd and saved in elog.
# 

prodname="o2-4f"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
# testname="test_o2"
cat > ${prodname}-list.txt <<EOF
4f_ZZ_hadronic.bWW:_358/I500064
EOF
# muon 10 GeV, 85deg


export GENSPLIT_DEFAULT_NPROCS=3

simproddir="/group/ilc/users/miyamoto/ildprod/v03-02/SM-4fA/4f_ZZ_hadronic.bWW_RL/4_subprod"
simprodjson="${simproddir}/simprod_15124_l5_v02.json"
banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk"
excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/o2-requests.xlsx"
addopts=" --SimSelectedFile 5 --nodry --noPrompt "

cmd="init_production.py --workdir ${proddir} --excel_file ${excel_file} \
  --prodlist ${prodname}-list.txt \
  --rec_nbtasks 504 \
  --sim_models ILD_l5_v02 --rec_options o2 \
  --production_type ovl \
  --simprod_json ${simprodjson} \
  --rec_banned_sites ${banned_sites} \
  --mcprod_config "mcprod-ilcsoft-v02-02-01.py" \
  --step4_options ${addopts} "

# 
#  --split_nseq_from 0 \
#   --ngenfile_max 1 \
#

echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
