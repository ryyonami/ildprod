#!/bin/bash 
#
#  Create scripts for background files.
#

# ###########################################################
# Create production scripts
# Usage:
#   create_scripts [norder] [nstep] [proddir]
# Argeuments:
#   [evttype] : event type and pol, like 3f.bBW
#   [procid] : ProcessID
#   [norder] : Production sequence number
#   [simse] : SE for SIM data output
#   [proddir] : Directories of production
# ###########################################################
create_scripts(){
  nstep=15
  nseq_from=8
  evttype=$1
  procid=$2
  norder=$3
  simse_now=$4
  proddir_now=$5

  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
${evttype}:_${procid}/${procid}
EOF
# I500046,I500048,I500026,I500028 23(more 15) files for 1000 fb^-1. 

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-3f.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir_now} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${nstep} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir_now}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir_now}
  
}


prodname="SM-3fC"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

#   [evttype] : event type and pol, like 3f.bBW
#   [procid] : ProcessID
#   [norder] : Production sequence number
#   [simse] : SE for SIM data output
#   [proddir] : Directories of production

create_scripts 3f.bWB I500026 1 DESY-SRM ${proddir}

create_scripts 3f.bWB I500028 2 KEK-DISK ${proddir}

create_scripts 3f.bBW I500046 3 DESY-SRM ${proddir}

create_scripts 3f.bBW I500048 4 KEK-DISK ${proddir}

