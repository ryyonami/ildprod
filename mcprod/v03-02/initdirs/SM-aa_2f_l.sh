#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="SM-aa_2f"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk"

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
#testname="250-SetA-lowX"
cat > ${prodname}-list.txt <<EOF
aa_2f_Z_leptonic.bBB
EOF
# higgs_ffh.bWW.0lep:_${testname}/I106519
# 2f_Z_leptonic.bWW.1lep:_${testname}/I250108

#( cd ${proddir} && rm -rfv *.bWW.[01]lep_${testname} )

export GENSPLIT_DEFAULT_NPROCS=6

# --noelog

cmd="init_production.py --workdir ${proddir} \
  --excel_file  /home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-aa_2f_leptonic.xlsx \
  --prodlist ${prodname}-list.txt \
  --dstonly --recrate 0.10 \
  --split_nseq_from 0 --ngenfile_max 76 \
  --se_for_sim KEK-SRM \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 1000 --rec_nbtasks 1000 \
  --production_type sim:ovl "

#  --se_for_data KEK-DISK --se_for_logfiles KEK-DISK \
#  --se_for_dstm_replicates DESY-SRM \
#  --test --multi_dir --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
#  --se_for_dstm_replicates DESY-SRM \
#  --nw_perfile 10 --ngenfile_max 2 --split_nbfiles 5 \
#
echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}


################################################################################
# init_production.py help
################################################################################
# usage: init_production.py [-h] [-d SPLITDIR] [--excel_file EXCELFILE]
#                           [--genfile_list GENFILE_LIST] [--workdir WORKDIR]
#                           [--prodlist PRODLIST]
#                           [--split_nbfiles SPLIT_NBFILES]
#                           [--ngenfile_max NGENFILE_MAX]
#                           [--nw_perfile NW_PERFILE]
#                           [--split_nseq_from SPLIT_NSEQ_FROM]
#                           [--split_skip_nevents SPLIT_SKIP_NEVENTS]
#                           [--production_type PROD_TYPE]
#                           [--se_for_data SE_FOR_DATA]
#                           [--se_for_sim SE_FOR_SIM] [--se_for_rec SE_FOR_REC]
#                           [--se_for_dstmerged SE_FOR_DSTMERGED]
#                           [--se_for_gensplit SE_FOR_GENSPLIT]
#                           [--se_for_logfiles SE_FOR_LOGFILES]
#                           [--se_for_dstm_replicates SE_FOR_DSTM_REPLICATES]
#                           [--elogid_old ELOGID_OLD]
#                           [--reuse_gensplit REUSE_GENSPLIT]
#                           [--sim_nbtasks SIM_NBTASKS]
#                           [--rec_nbtasks REC_NBTASKS]
#                           [--sim_banned_sites SIM_BANNED_SITES]
#                           [--rec_banned_sites REC_BANNED_SITES]
#                           [--gen_data_type GEN_DATA_TYPE]
#                           [--sim_models SIM_MODELS]
#                           [--rec_options REC_OPTIONS] [--topdir TOPDIR]
#                           [--noelog] [--test] [--multi_dir]
#                           [--genmeta_json GENMETA_JSON] [--dstonly]
#                           [--simprod_json SIMPROD_JSON] [--recrate RECRATE]
#                           [--xml_serial XML_SERIAL] [--step4_options ...]
# 
# Generate subdirectories and scripts to do production
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   -d SPLITDIR, --splitdir SPLITDIR
#                         dir names where splitted generator files are written.
#   --excel_file EXCELFILE
#                         Excel file name, where production information is
#                         obtained.
#   --genfile_list GENFILE_LIST
#                         File name of generator file names
#   --workdir WORKDIR     A directory where production set scripts are written
#   --prodlist PRODLIST   A file containing a list of production groupds.
#   --split_nbfiles SPLIT_NBFILES
#                         Number of splitted files to be produced.
#   --ngenfile_max NGENFILE_MAX
#                         Max number of generator file to use.
#   --nw_perfile NW_PERFILE
#                         number of events per file in gensplit.
#   --split_nseq_from SPLIT_NSEQ_FROM
#                         Sequence number of first file for gensplit
#   --split_skip_nevents SPLIT_SKIP_NEVENTS
#                         Number of events to skip before starting gensplit
#   --production_type PROD_TYPE
#                         Production type, sim, ovl, nobg. Default is sim:ovl
#   --se_for_data SE_FOR_DATA
#                         Output SE for Sim, Rec, DST, DSTMerged files. Log
#                         files are DESY-SRM
#   --se_for_sim SE_FOR_SIM
#                         Output SE for SIM. Overwrite se_for_data setting
#   --se_for_rec SE_FOR_REC
#                         Output SE for REC. Overwrite se_for_data setting
#   --se_for_dstmerged SE_FOR_DSTMERGED
#                         Output SE for DSTMerged. Overwrite se_for_data setting
#   --se_for_gensplit SE_FOR_GENSPLIT
#                         SE for gensplit files.
#   --se_for_logfiles SE_FOR_LOGFILES
#                         SE for log files.
#   --se_for_dstm_replicates SE_FOR_DSTM_REPLICATES
#                         SE(s) where to replicates DST-Merged files. ","
#                         separated list for multiple SEs (untested).
#   --elogid_old ELOGID_OLD
#                         Elog ID referenced, where past production information
#                         is obtained
#   --reuse_gensplit REUSE_GENSPLIT
#                         Use this gensplit_id for new simproduction
#   --sim_nbtasks SIM_NBTASKS
#                         Number of tasks of simulaton production
#   --rec_nbtasks REC_NBTASKS
#                         Number of tasks of reconstruction production
#   --sim_banned_sites SIM_BANNED_SITES
#                         Banned sites for simulation production
#   --rec_banned_sites REC_BANNED_SITES
#                         Banned sites for reconstruction
#   --gen_data_type GEN_DATA_TYPE
#                         generator data type, either stdhep or pairs
#   --sim_models SIM_MODELS
#                         detector model for simulation ( or : separated list of
#                         it )
#   --rec_options REC_OPTIONS
#                         options for reconstruction ( or : separated list of it
#                         )
#   --topdir TOPDIR       If specified, it is the basedir to save sim/rec files.
#                         /ilc/prod/ilc/mc-opt for example. If
#                         MCProd_MCOpt_DirTape=mc-opt, mc-opt is replaced to
#                         MCProd_MCOpt_DirDisk for dst/log files.
#   --noelog              Do not generate ELOG data. An option for test scripts.
#   --test                Scripts for a test production are generated.
#   --multi_dir           If specified, generator files in different directories
#                         are used.
#   --genmeta_json GENMETA_JSON
#                         genmetaByID.json file used for production
#   --dstonly             Output DST files only, except a small number of jobs
#                         to write rec files.
#   --simprod_json SIMPROD_JSON
#                         Json file of sim production, which is used as an input
#                         of rec only production.
#   --recrate RECRATE     If 0<r<1.0, fraction of REC files to keep; If 1<=
#                         r(integer), rec files matched to r generator files;
#                         r<0 not keep REF files at all. Valid when dstonly is
#                         TRUE.
#   --xml_serial XML_SERIAL
#                         First serial number attached to xml file
#                         name(default=0)
#   --step4_options ...   Option string passed to ILDProdBase. Should be last
#                         options given
