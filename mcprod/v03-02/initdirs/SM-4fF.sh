#!/bin/bash 
#
#  Create scripts for background files.
#
# ###########################################################
# Create production scripts
# Usage:
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 
# ###########################################################
create_scripts(){
  procname=$1
  processID=$2
  norder=$3
  nseq_from=$4
  ngenfile=$5
  simse_now=$6

  prodname="SM-4fF"
  proddir=${PRODTASKDIR}/${prodname}

  if [ ! -e ${proddir} ] ; then 
    mkdir -v ${proddir} || my_abort "Failed to create directory" 
  fi
  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
${procname}:_I${processID}_${norder}/I${processID}
EOF

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-midhighX.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${ngenfile} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}
  
}

###########################################################
# main part of this script
###########################################################
#  4f Large cross section processes.
#
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 


# 4f_singleZee_leptonic.bWW
### 4f_sze_l    eL.pR I500114  2857fb-1(0-32 files, 27022 jobs) ==> 5000 fb-1(0-57 total)
###             eR.pL I500116  2995fb-1(0-33 files, 28356 jobs) ==> 5000 fb-1(0-56 total)
create_scripts 4f_singleZee_leptonic.bWW 500114 2 33 25 DESY-SRM

create_scripts 4f_singleZee_leptonic.bWW 500116 2 34 23 KEK-DISK

# #######################################################################################
# 4f_singleW_semileptonic.bWW
### 4f_sw_sl    eL.pR I500106.  506fb-1(0-51 files, 26000 jobs) ==> 5000 fb-1(0-513 total)
#
nseq_from0=52
num_files=52
for i in `seq 2 2` ; do
   simse="DESY-SRM"
   [ $[${i}/2*2] -eq ${i} ] || simse="KEK-DISK"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 513 ] && num=$[513-${n0}+1]
   create_scripts 4f_singleW_semileptonic.bWW 500106 ${i} ${n0} ${num} ${simse}
done

# #######################################################################################
# 4f_WW_semileptonic.bWW
### 4f_ww_sl    eL.pR I500082   500fb-1(0-93 files, 23500 jobs) ==> 5000 fb-1(0-938 total)
nseq_from0=94
num_files=94
for i in `seq 2 2` ; do
   simse="KEK-DISK"
   [ $[${i}/2*2] -eq ${i} ] || simse="DESY-SRM"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 938 ] && num=$[938-${n0}+1]
   create_scripts 4f_WW_semileptonic.bWW 500082 ${i} ${n0} ${num} ${simse}
done
#
# #######################################################################################
# 4f_WW_hadronic.bWW
### 4f_ww_h     eL.pR I500066   504fb-1(0-74 files, 37500 jobs) ==> 5000 fb-1(0-743 total)
nseq_from0=75
num_files=75
for i in `seq 2 2` ; do
   simse="DESY-SRM"
   [ $[${i}/2*2] -eq ${i} ] || simse="KEK-DISK"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 743 ] && num=$[743-${n0}+1]
   create_scripts 4f_WW_hadronic.bWW 500066 ${i} ${n0} ${num} ${simse}
done
#
#
# #######################################################################################
# 4f_ZZWWMix_hadronic.bWW
### 4f_zzorww_h eL.pR I500070   500fb-1(0-61 files, 31000 jobs) ==> 5000 fb-1(0-619 total)
nseq_from0=62
num_files=62
for i in `seq 2 2` ; do
   simse="KEK-DISK"
   [ $[${i}/2*2] -eq ${i} ] || simse="DESY-SRM"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 619 ] && num=$[619-${n0}+1]
   create_scripts 4f_ZZWWMix_hadronic.bWW 500070 ${i} ${n0} ${num_files} ${simse}
done


