#!/bin/env python 
# 
# Copy elog from one place to another

import os
import elog
import argparse
import datetime

logbook_in = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
# _elog_subdir = "dbd-prod"
# _elog_subdir = "test-prod"
_do_test = False

if __name__ == "__main__":
  
    parser = argparse.ArgumentParser(description="Copy Elog entry from one Logbook to another")
    parser.add_argument("book_from", help="source logbook name ( ex. dbd-prod)")
    parser.add_argument("src_elog_id", help="Elog ID in source logbook")
    parser.add_argument("book_to", help="destination logbook name. As a default create a new Elog entry ( ex. test-prod )")

    args = parser.parse_args()

    lines = open(os.environ["MY_ELOG_KEY"]).readlines()
    ( auser, apass ) = lines[0][:-1].split(' ')

    from_subdir = args.book_from
    from_id = int(args.src_elog_id)
    logbook_in = elog.open("%s/%s/" % (_elog_url, from_subdir), user=auser, password=apass)

    (message, attrib, attachements ) = logbook_in.read(from_id)

    to_subdir = args.book_to
    print( "ElogID " + str(from_id) + " in " + str(from_subdir) + " will be copied to " + str(to_subdir) )
    if raw_input("Proceed (y/N)").upper() != "Y":
       exit(0)

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)
    msglist = ["<ul>", 
        "<li>%s %s : This entry is a copy of ElogID %s in logbook %s.</li>" % (dstr, tstr, str(from_id), str(from_subdir)),
        "</ul>"]
    message += "\n".join(msglist) 

    logbook_out = elog.open("%s/%s/" % (_elog_url, to_subdir), user=auser, password=apass)
    msg_id = logbook_out.post( message, attributes = attrib, attachements = attachements, encoding="HTML" )
    print( "Copied to ELOG ID="+ str(msg_id)+ " in logbook " + to_subdir )




