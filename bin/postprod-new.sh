#!/bin/bash

if [ "x$1" == "x" ] ; then 
  echo "This command download production job logs."
  echo "Usage: postprod.sh [prodid]"
  exit
fi

prodid=$1

mkdir prod${prodid}
cd prod${prodid}

# /ilc/prod/ilc/mc-dbd/ild/rec/500-TDR_ws/4f_singleZee_leptonic/ILD_o1_v05/v01-16-p05_500/00006838/LOG/001

echo "### Executing dirac-ilc-get-info -p ${prodid} ##########"
dirac-ilc-get-info -p $prodid 2>&1 | tee prodinfo-${prodid}.log
mkdir tgz
mkdir log

(
  cd tgz
  echo "### Executing dirac-ilc-get-prod-log -P ${prodid} -A -N ######"
  dirac-ilc-get-prod-log -P ${prodid} -A -N
  for d in [0-9]* ; do
    ( cd ../log && mkdir $d )
  done
)

wrkdir=`pwd`
(
  cd log
  for w in [0-9]* ; do 
    (
      cd ${w}
      xargsinput="xargs.input"
      joboutcmd="jobout.sh"
      getjobout="xargs-jobout.sh"
      for f in ${wrkdir}/tgz/${w}/*.gz ; do      
        fdir=`basename $f | sed -e "s/.tar.gz//"`
        ( 
          mkdir $fdir
          cd ${fdir}
          tar zxf $f 
        )
      done
          
      echo "### Picking up jobid at `pwd` ####################"
      for d in 0* ; do 
        jobid=`grep DIRACJOBID $d/localEnv.log | cut -d"=" -f2`
        echo "cd $d && dirac-wms-job-get-output ${jobid} " > $d/${joboutcmd}
        chmod +x $d/${joboutcmd}
        echo "${d}/${joboutcmd}" >> ${xargsinput}
      done

      echo "cat ${xargsinput} | xargs -P 15 -I{}  /bin/bash -c {} > getjobout.log 2>&1 " > ${getjobout}
      echo "jobout command was created at ${getjobout}"
    )
  done
)

echo "#!/bin/bash " > suball.sh
echo "hostname " >> suball.sh
for d in log/[0-9]* ; do
  echo "echo $d " >> suball.sh
  echo "date " >> suball.sh
  echo " ( cd $d && source xargs-jobout.sh ) " >> suball.sh
  echo " pwd && ls -l && rm -vrf tgz " >> suball.sh
done

chmod +x suball.sh
hostname
echo "start suball.sh  `date` "
./suball.sh 
echo "All process completed.  `date`"

 


