#!/bin/bash 
# Retry gensplit upload of failed files.
# to be executed in 3_gensplit directory

logfile="gensplit-retry-all.log"
dstr=`date +%Y%m%d-%H%M%S`
hostname=`hostname`

echo "### gensplit-retry-all.sh started at ${hostname} on ${dstr}" | tee ${logfile} 
curdir=`echo ${PWD} | cut -d"/" --complement -f1-6`
echo "### running at ${curdir}" | tee -a ${logfile}
for d in I* ; do 
  ( 
    cd $d
    echo -n "$d : "
    gensplit-retry.sh "nolog"
  )
done 2>&1 | tee -a ${logfile}
echo "gensplit-retry.sh has completed." | tee -a ${logfile}
nfiles=`find . -name "*.slcio" -type f -print | wc -l`
echo "### Total ${nfiles} of slcio files were created here" | tee -a ${logfile}
dstr=`date +%Y%m%d-%H%M%S`
echo "### gensplit-retry-all.sh ended on ${dstr}" | tee -a ${logfile}


