# For py.test
export PYTEST_ADDOPTS="--cov-report html"

# export WEBTEST=no  # Not used since v02-00-02

# Common to all ILDProd tools

# Following parameters should be defined in advance
# export ILDPROD=~/ILDProd
# export ILCSOFT_INIT_SCRIPT=/cvmfs/ilc.desy.de/sw/x86_64_gcc49_sl6/v02-00-02/init_ilcsoft.sh
# export PRODDIR=v02-00-02
# export PRODKEY_FILE=A file to be sourced to setup environment for keys

if [ -z ${PRODKEY_FILE} ] ; then 
  echo "PRODKEY_FILE environment parameter is not defined."
else
  source ${PRODKEY_FILE}
fi

export MCPROD_TOP=${ILDPROD}/mcprod/${PRODDIR}
export SCRIPTS_DIR=${ILDPROD}/mcprod/scripts
export PATH=${ILDPROD}/mcprod/scripts:${ILDPROD}/bin:${PATH}
export PYTHONPATH=${ILDPROD}/python:${ILDPROD}/mcprod/scripts:${PYTHONPATH}

### optprod/bin is not necessary any more. use bin/ild-getid to get gensplit IDs, etc. 
### export PATH=/group/ilc/users/miyamoto/optprod/bin:${PATH}

export PRODTASKDIR=${MCPROD_TOP}/ProdTask
export MCPROD_CONFIG_FILE=${MCPROD_TOP}/mcprod_config.py



