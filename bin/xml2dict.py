#!/usr/bin/env python

import argparse
import xmltodict
import pprint 
import json

# Sample Call: 
if __name__ == "__main__":


    parser = argparse.ArgumentParser(description="Convert excel file to dictionary object")
    parser.add_argument("book_from", help="source excelfile")
    parser.add_argument("-o","--out_file",help="Output json file. If not specified, do pprint",
       dest="outfile", action="store", default="none")

    args = parser.parse_args()
    infile=args.book_from
    outfile=args.outfile    

    filein = open(infile)

    sample = xmltodict.parse(filein)

    if outfile == "none":
       # print json.dumps(sample)
       pprint.pprint(sample)
    else:
       import json
       json.dump(sample, open(outfile,"w"))

