#!/bin/bash

git clone https://github.com/paulscherrerinstitute/py_elog.git py_elog-master -b 1.3.3
wget https://gitlab.cern.ch/amiyamot/ildprod/raw/master/utils/scripts/py_elog-master.patch
( 
  cd py_elog-master 
  patch -p1 -u < ../py_elog-master.patch 
  python setup.py install
)

# Patch file was created by 
#  diff -uprN py_elog-master-orig py_elog-master > py_elog-master.patch
#


