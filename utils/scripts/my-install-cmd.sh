#!/bin/bash 
# For install of ILCDIRAC v32r0p2 with dirac-os-version v1r23, without ILCDirac from git.(2021-08-18)
#
# For Clean install of ILCDIRAC in CentOS7. 
# 
# The default ILCDIRAC is installed following the instruction described at 
# https://ilcdirac-doc.web.cern.ch/DOC/Files/UserGuide/ilcdiracclient.html#id5
# GRID certificates, usercert.pem and userkey.pem are not updated.
# grid-security/vomses, etc are obtained using your grid certificate. 
# For this, password input is promped when "dirac-proxy-init -x -N" is issued.
#
# Then ILCDIRAC is removed, then git clone-ed from gitlab.cern.
# Then installation of additional python packages, such as openpyxl, elog, etc follow.
#
# Usage:
#   . my-install-cmd.sh 2>&1 | tee my-install-cmd.log
#

wget -O dirac-install -np \
	https://raw.githubusercontent.com/DIRACGrid/management/master/dirac-install.py \
	--no-check-certificate
chmod +x dirac-install
./dirac-install -V ILCDIRAC -r v32r0p2 --dirac-os --dirac-os-version=v1r23


# ILCDirac configure. Password is promped
. bashrc
dirac-proxy-init -x -N
dirac-configure -S ILC-Production -C dips://voilcdiracconfig.cern.ch:9135/Configuration/Server --SkipCAChecks

# Install ILCDIRAC by git clone

# rm -rf ILCDIRAC
# git clone  https://gitlab.cern.ch/CLICdp/iLCDirac/ILCDIRAC.git ILCDIRAC
# . bashrc
# dirac-deploy-scripts
# unset REQUESTS_CA_BUNDLE


# Add developper's packages.
# cd ~/ILDDirac
pip install --upgrade pip
pip uninstall -y distribute
pip install --upgrade setuptools
pip install --upgrade setuptools_scm
pip install --upgrade pylint mock MySQL-python pytest-cov pytest-randomly flake8 psutil \
            flake8-docstrings flake8-commas

# Install tools for ILD
pip install openpyxl
pip install xlrd
pip install lxml
pip install braceexpand
pip install xlsxwriter


#( cd ~/soft/python/python2/py_elog-master  && python setup.py install )
(
  echo "Installing python2_elog"
  mkdir -p install_temp-elog-$$
  cd install_temp-elog-$$
  wget https://gitlab.cern.ch/amiyamot/ildprod/raw/master/utils/scripts/py2_elog-install.sh
  chmod +x py2_elog-install.sh
  ./py2_elog-install.sh
  ret=$?
  cd ..
  [ "x${ret}" == "x0" ] && rm -rf install_temp-elog-$$
)

#(
#  echo "Installing XlsxWriter"
#  mkdir -p install_temp-XlsxWriter-$$
#  cd install_temp-XlsxWriter-$$
#  git clone https://github.com/jmcnamara/XlsxWriter.git
#  cd XlsxWriter
#  python setup.py install
#  ret=$?
#  cd ../..
#  [ "x${ret}" == "x0" ] && rm -rf install_temp-XlsxWriter-$$
#)

