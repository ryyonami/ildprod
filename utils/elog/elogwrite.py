#!/bin/env python

### #!/bin/env python3 

import elog
import os
import urllib3

if __name__ == "__main__":

  urllib3.disable_warnings()
  print("HTTPS SSL Warnings for the certificate verification is DISBALED.")
  print("See https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings InsecureRequestWarning")

  my_elog_key_file=os.environ["MY_ELOG_KEY"]
  lines = open(my_elog_key_file).readlines()
  # lines = open(os.environ["MY_PASSWD_FILE"]).readlines()
  auser, apass = lines[0][:-1].split(' ') 
  
  logbook = elog.open("https://ild.ngt.ndu.ac.jp/elog/worklog/", user=auser, password=apass)
  
  elog_record = {"Worker":"A.Miyamoto", "Category":"Elog", "Status":"Info", "Subject":"Input by python"}

  msgtext = '\n'.join(["<ul>",
      "<li>2018-02-02 : elog was modified to run with python2</li>",
      "<li>2018-01-26 : This entry was written by py_elog-master, which requires Python3.6 or higher.</li>",
      "<li>Following files are attached as an example",
      "<ul>",
      " <li>elogread.py : read an elog entry.</li>",
      " <li>elogwrite.py : write an elog entry.</li>",
      "</ul>",
      "<li>If msg_id is specified, existing elog entry is modified.</li>",
      "<li>loggbook.delete(msg_id) is to delete a message.</li>",
      "</ul>"])

  new_msg_id = logbook.post(msgtext, attributes = elog_record, attachments=["elogread.py", "elogwrite.py"], encoding="HTML")

  # new_msg_id = logbook.post(msgtext, attributes = elog_record, msg_id = old_msg_id, attachments=["elogread.py", "elogwrite.py"], encoding="HTML")

  # logbook.delete(delete_msg_id)

  print("Added new elog entry with ID="+str(new_msg_id))



