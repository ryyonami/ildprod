#!/bin/env python

### #!/bin/env python3 

import elog
import os
import urllib3

if __name__ == "__main__":

  urllib3.disable_warnings()
  print("HTTPS SSL Warnings for the certificate verification is DISBALED.")
  print("See https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings InsecureRequestWarning")

  my_elog_key_file=os.environ["MY_ELOG_KEY"]
  lines = open(my_elog_key_file).readlines()
  # lines = open(os.environ["MY_PASSWD_FILE"]).readlines()
  auser, apass = lines[0][:-1].split(' ') 
  
  logbook = elog.open("https://ild.ngt.ndu.ac.jp/elog/genmeta/", user=auser, password=apass)

#   logbook.delete(3588)
#   for elogid in range(3487, 3462, -1):
  for elogid in range(3587, 3488, -1):
    logbook.delete(elogid)
    print("elog "+str(elogid)+" was deleted.")

