#!/bin/env python 
# Decode %{ } and ${ } variables in json file.
#

import os
import json
import pprint
import string

# ########################################################
class TemplatePercent(string.Template):
   delimiter = "%"
   def __init__(self, template):
     super(TemplatePercent, self).__init__(template)

# ########################################################
def Decode_Json(inp):
    ''' expand %{ } and ${ } variable in inp and return '''

    newv = inp

    envkeys = os.environ.keys()
    envdict = {}
    for env in envkeys:
        envdict[env] = os.environ[env]

    for key, value in inp.items():
      if "${" in value:
         orig = string.Template(value)
         conv = orig.substitute(**envdict)
         newv[key] = conv

    # pprint.pprint(newv)
    newv2 = newv
    # Convert only string object
    doRetry = True
    nloop = 0
    while doRetry:
        doRetry = False
        newv = newv2
        nloop += 1
        for key, value in newv.items():
            if "%{" in value:
              orig = TemplatePercent(value)
              table = {}
              for k, v in newv.items():
                  if not isinstance(v, dict):
                      table[k] = v
              conv = orig.safe_substitute(**table)
              newv2[key] = conv
              # print value + " was converted to " + newv2[key]
              if "%{" in conv and nloop < 5 :
                 doRetry = True

    return newv2

# ########################################################
def Create_Job_Scripts(proc, jobinfo):
    ''' Create job scripts to run whizard2 job.'''

    if not os.path.exists("jobs"):
       os.mkdir("jobs")
       print "a directory, jobs, was created."

    jobdir = "/".join([jobinfo["TOPDIR"], "jobs/%s" % proc])
    if not os.path.exists(jobdir):
       os.mkdir(jobdir)
       print jobdir + " was created."

    cwd = os.getcwd()
    os.chdir(jobdir)
    
    convtable = {}
    for k, v in jobinfo.items():
        if not isinstance(v, dict):
            convtable[k] = v
    for k, v in jobinfo["Processes"][proc].items():
        convtable[k] = v
    
    conv2 = {}
    for k,v in convtable.items():
        if "%{" not in v:
           conv2[k] = v
        else:
           templ = TemplatePercent(v)
           conv2[k] = templ.safe_substitute(**convtable)

    # print "#############################"
    # pprint.pprint(conv2)

    runscripts = []
    for line in open(jobinfo["WHIZARD2_TEMPLATE_SCRIPT"]):
        if "%{" not in line:
           runscripts.append(line)
        else:
           templ = TemplatePercent(line)
           runscripts.append(templ.safe_substitute(**conv2))

    jobname = proc[1:].replace(".","").replace("_","")
    conv2["JobName"] = jobname

    fout = open("run_whizard.sh","w")
    fout.write("".join(runscripts))
    fout.close()
    os.system("chmod +x run_whizard.sh")
    json.dump(conv2, open("jobparams.json","w"))
    # print "".join(runscripts) 
    fout = open("bsub.sh", "w")
    fout.write("bsub -o run_whizard.log -q s -J " + jobname + " \"( ./run_whizard.sh > run_whizard.log 2>&1 )\" ")
    fout.close()
    os.chdir(cwd)

    print "Job scripts were written to " + jobdir

    # for proc in inp["Processes"].items():
        

       


# ########################################################
if __name__ == "__main__":

    param = json.load(open("param.json"))

    # pprint.pprint(param)

    # print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    jobinfo = Decode_Json(param)
    # pprint.pprint(jobinfo)

    # Create job scripts
    for proc in jobinfo["Processes"].keys():
        Create_Job_Scripts(proc, jobinfo)



