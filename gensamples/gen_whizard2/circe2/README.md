# Private CIRCE2 file 

Files here are produced by A.M. for test of production system.

## ```ILC.ee.250-TDR_ws.AM-20160527.circe2```

Original data and several figures will be found at 
```
~/work/whizard2/circe2-old/mkcirces2/250/scan/figs-man-5/circe2.man-5
```
The same file is available at 
```
/ilc/user/a/amiyamot/software/whizard2/ILC.ee.E250-TDR_ws.am-20160527.circe2
```
