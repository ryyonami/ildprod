#!/usr/bin/env python

import json
import pprint
import datetime

######################################################################
def collectSize(jdata, tdir, selist):
  ''' collect current size and increase of se in target dirs '''
  dtkeys = sorted(jdata.keys())
  dthajime = dtkeys[-3]
  dtfound = []
  for kdt in reversed(dtkeys):
#    print kdt
    if tdir in jdata[kdt]:
      dtfound.append(kdt)

  dtsort=dtfound
  if len(dtsort) > 1:
    hizuke = [ dtsort[0], dtsort[1], dtsort[-1] ]
  elif len(dtsort) == 1:
    # print(" dtsort of tdir"+tdir)
    # print(" dtsort="+str(dtsort))
    # print(" dtkeys="+str(dtkeys))
    hizuke = [ dtsort[0], dtsort[0], dtsort[0] ]
  else :
    # print(" dtsort of tdir"+tdir)
    # print(" dtsort="+str(dtsort))
    # print(" dtkeys="+str(dtkeys))
    hizuke = [ dtsort[0], dtsort[0], dtsort[0] ]
    allout = { 'date':hizuke, 'SE':ret}
    return allout
  
#   pprint.pprint(hizuke)
  # print hizuke
  hajime = jdata[hizuke[2]][tdir]
  kinou = jdata[hizuke[1]][tdir] if hizuke[1] in jdata else hajime
  kyou = jdata[hizuke[0]][tdir] if hizuke[0] in jdata else kinou

  hundredMB=long(100000000)
  ret = {}
 
  for se in selist:
    if se in kyou['sesize']:
      ikyou = long(kyou['sesize'][se]['size'])
      ikinou = long(kinou['sesize'][se]['size']) if se in kinou['sesize'] else 0
      ihajime = long(hajime['sesize'][se]['size']) if se in hajime['sesize'] else 0
      nkinou = long(kinou['sesize'][se]['replicas']) if se in kinou['sesize'] else 0
      nhajime = long(hajime['sesize'][se]['replicas']) if se in hajime['sesize'] else 0
      ret[se] = {}
      ret[se][0] = float(long(ikyou)/hundredMB)/10000.0
      ret[se][1] = float(long(ikyou-ikinou)/hundredMB)/10000.0
      ret[se][2] = float(long(ikyou-ihajime)/hundredMB)/10000.0
      ret[se][3] = str(long(kyou['sesize'][se]['replicas']))
      ret[se][4] = str(long(kyou['sesize'][se]['replicas']) - nkinou)
      ret[se][5] = str(long(kyou['sesize'][se]['replicas']) - nhajime)
    elif se == 'total':
      ret[se] = {}
      ret[se][0] = float(long(kyou[se]['size'])/hundredMB)/10000.0
      ret[se][1] = float((long(kyou[se]['size']) - long(kinou[se]['size']))/hundredMB)/10000.0
      ret[se][2] = float((long(kyou[se]['size']) - long(hajime[se]['size']))/hundredMB)/10000.0
      ret[se][3] = kyou[se]['replicas']
      ret[se][4] = str(long(kyou[se]['replicas']) - long(kinou[se]['replicas']))
      ret[se][5] = str(long(kyou[se]['replicas']) - long(hajime[se]['replicas']))

    else:
      ret[se] = {}
      for ik in range(0, 6):
        ret[se][ik] = "---"

#  pprint.pprint(ret)
  allout = { 'date':hizuke, 'SE':ret}
  return allout


######################################################################
tdirs = ["mc-2020", 
         "mc-2020/ild/dst-merged",
         "mc-2020/ild/dst",
         "mc-2020/ild/sim", "mc-2020/ild/rec", 
         "mc-2020/generated",
         "mc-dbd/generated", 
         "ild", 
	 "mc-opt-3", "mc-opt.dsk", "mc-opt-2", "mc-opt", 
         "mc-dbd/ild", 
        # "mc-dbd.generated",
        # "mc-dbd.log/ild", 
         "amiyamot"]
topdir = "/ilc/prod/ilc"
tse = ['total', 'DESY-SRM', 'KEK-DISK', 'KEK-SRM', 'IN2P3-SRM', 'CERN-SRM', 'CERN-DST-EOS', 'dcache-se-desy.desy.de']

sizefile = "../fsize.json"
jsondata = json.load(open(sizefile))

summary = {}

for td in tdirs:
  tdir = topdir + '/' + td
  if td == "amiyamot":
     tdir = "/ilc/user/a/amiyamot"
  summary[td] = collectSize(jsondata, tdir, tse)
  
# pprint.pprint(summary)

#### Create SIZE information
html = ""
html += "<h2>SE : # replicas (upper) and data size (TB, lower) </h2>\n"
html += "<table border='1'>\n"
html += "<tr bgcolor=\"palegreen\"><td>Directory</td><td>Date</td>\n"
for se in tse:
  ose = se.replace('dcache-se-desy.desy.de', 'desy-dcache')
  html += "<td>"+ ose + "</td>"
html += "\n"
html += "</tr>\n"

sesum = {}
for se in tse:
  sesum[se] = [0.0, 0.0, 0.0, 0, 0, 0]

for td in tdirs:
  adir = summary[td]["SE"]
  # print(" td="+td+" adir="+str(adir))
  adat = summary[td]["date"]
  datnow = adat[0].split('-')
  html += "<tr bgcolor=\"wheat\">\n"
  html += "<td>"+td+"</td><td align=\"right\">at "+datnow[0]+"</td>\n"
  for se in tse:
    if se in adir:
      out = str(adir[se][0]) if adir[se][0] >= 0.0 else "none"
      html += "<td align=\"right\">" + str(adir[se][3]) + "<br>" + out + "</td>"
      if str(adir[se][3]) != "---":
        if "mc-2020/" not in td: 
          sesum[se][0] += adir[se][0]
          sesum[se][3] += long(adir[se][3])
          # print( "sesum["+se+"][0] added "+str(adir[se][0])+" for td="+str(td)+" sum="+str(sesum[se][0]))


  html += "\n</tr>\n"
  for itype in [1, 2]:
    dateref = adat[itype].split('-')
    html += "<tr><td></td><td align=\"right\">+ from "+dateref[0]+"</td>\n"
    for se in tse:
      if se in adir:
        out = str(adir[se][itype]) if adir[se][itype] >= 0.0 else "none"
        html += "<td align=\"right\">" + str(adir[se][itype+3]) + "<br>" + out + "</td>"
        if str(adir[se][itype]) != "---":
          if "mc-2020/" not in td:
            sesum[se][itype] += adir[se][itype]
            sesum[se][itype+3] += long(adir[se][itype+3])

    html += "\n</tr>\n"
    
html += "</table>\n"  
#
htmltotal = ""
htmltotal += "<h2>All directory: SE # replicas (upper) and data size (TB, lower) </h2>\n"
htmltotal += "<table border='1'>\n"
htmltotal += "<tr bgcolor=\"palegreen\"><td>Directory</td><td>Date</td>\n"
for se in tse:
  ose = se.replace('dcache-se-desy.desy.de', 'desy-dcache')
  htmltotal += "<td>"+ ose + "</td>"
htmltotal += "\n"
htmltotal += "</tr>\n"

# adat = summary["mc-dbd.generated"]["date"]
adat = summary["mc-dbd/generated"]["date"]
datnow = adat[0].split('-')
htmltotal += "<tr bgcolor=\"wheat\">\n"
htmltotal += "<td>All</td><td align=\"right\">at "+datnow[0]+"</td>\n"
for se in tse:
  htmltotal += "<td align=\"right\">" + str(sesum[se][3]) + "<br>" + str(sesum[se][0]) + "</td>"
htmltotal += "\n</tr>\n"


for itype in [1, 2]:
  dateref = adat[itype].split('-')
  htmltotal += "<tr><td></td><td align=\"right\">+ from "+dateref[0]+"</td>\n"
  for se in tse:
    htmltotal += "<td align=\"right\">" + str(sesum[se][itype+3]) + "<br>" + str(sesum[se][itype]) + "</td>"
  htmltotal += "\n</tr>\n"
htmltotal += "</table>\n"


fout = open("plotsize.html", "w")
fout.write(htmltotal)
# fout.write(html)
fout.close()

fout = open("mcprod.html", "w")
header = ""
header += "<html>\n"
header += "<head><title>ILCDIRAC SE size monitor</title></head>\n"
header += "<body>\n"
header += "<h1>ILCDIRAC monitor</h1>\n"

tail = ""
tail += "<hr>\n"
now = datetime.datetime.now()
tail += "(created : " + now.strftime("%a %b %d %X JST %Y") + "\n" 
tail += ", <a html=\"previous\">previous</a>)\n"
tail += "</body>\n</html>\n"
fout.write(header)
fout.write(htmltotal)
fout.write(html)
fout.write(tail)
fout.close()



