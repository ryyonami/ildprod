#!/usr/bin/env python 

import pprint
import os
import json
import glob
import sys

def log2dict(infile):
  data = {}
  olddir = ""
  semode = False
  curdir = "unedef"

  for line in open(infile):
    items = line[:-1].split()
    # print >> sys.stderr,  line[:-1]
    if "FC:/> directory: " in line:
      curdir=items[2]  
    elif "Logical Size: " in line:
      asize=int(items[2].replace(',',''))
      logical={"size":asize, "files":items[4], "dirs":items[6]}
    elif "==============" in line:
      semode = True
      sesize = {}
    elif "--------------" in line:
      semode = False
    elif "   Total  " in line:
      itotal = int(items[1].replace(',',''))
      total = {"size":itotal, "replicas":items[2]}
    elif semode:
      asize=int(items[2].replace(',',''))
      # print >> sys.stderr, items[1], items[2], asize, semode
      sesize[items[1]] = {"ser":items[0], "se":items[1], "size":asize, "replicas":items[3]}
    elif "Query time" in line:
      query = items[2]
      data[curdir] = {"logical":logical, "sesize":sesize, "total":total, "query":query}


  return data 

# #######################################################33
if __name__ == "__main__":
  sizemon_data = os.environ["SIZEMON_DATA"]
  files = glob.glob(sizemon_data + "/getsize-20*.log")
  alldata = {}
  
  for f in files:
    fname = os.path.basename(f)
    fkey = fname.replace(".log","").split("-",1)[1]
    alldata[fkey] = log2dict(f)

  #  pprint.pprint(alldata)

  json.dump(alldata, open("fsize.json","w"))

  print "fsize.json was created."
