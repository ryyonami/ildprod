#!/bin/bash 
#
###HELP_BEGIN###
# A scrip to monitor ILD MC production. 
# This script is initiated by a cron script, start_exec.sh, in the starter directory,
# run at ${MCPROD_TOP} directory and execute script described in the file, cronexe.list.
#
# Command syntax is 
#     prodmon.sh <init_script> 
# 
#     <init_script> is h, help, -h, or --help to get help 
# 
# See help of starter/start_exec.sh for how to write the crontab file at the starter    
# host.  <init_script> should exist at the worker host. It is sourced at the begining 
# in order to initialize ILCDirac/ILDProd environments and cd to ${MCPROD_TOP} 
# directory, confirm that no other monitor scripts are running at the same host 
# by runtest_by_load.sh, then execute scripts listed in cronexe.list.
# In addition, status of production listed in the file, prodids.list, are checked 
# and create a summary log.
#
###HELP_END###

# ########################################################
# ########################################################
exec_cronexe_list()
{
  cronexe_list=$1
  cat ${cronexe_list} | while read f ; do
    (
      if [ ${f:0:1} != "#" ] ; then
        dirname=`dirname $f`
        execname=`basename $f`
        if [ "x${dirname}" != "x" ] ; then
          cd ${dirname}
        else
          execname=$f
        fi
        dstr=`date +%Y%m%d-%H%M%S`
        pathinfo=` pwd | cut -d"/" -f7-`
        echo "===========" 
        # echo "### Readin $f " 
        echo "${pathinfo} on ${dstr} at `hostname` : exec ${execname}" 
        echo "===========" 

        if [ "x${f:0:2}" == "x./" ] ; then
           ${f} 2>&1 
        else
          ${execname} 2>&1 
        fi
      else
        echo "===== Skipping : ${f} " 
      fi
      echo " " 
      echo " " 
    )
  done
  echo "========= All ${cronexe_list} complete ${dstr} " 
}

# ##################################################
# Create production summary information from IDs listed in prodids.list
# ##################################################
make_production_summary()
{
  prodids_list=$1

  dstr=`date +%Y%m%d-%H%M%S`
  # headtext="TransID Type Status F_Total F_Proc.(%) F_Proc. F_Unused J_Created J_Wait J_Run J_Done J_Fail J_Stalled"
  # head1=`echo ${headtext} | cut -d" " -f1-3`
  # head2=`echo ${headtext} | cut -d" " -f4-8`
  # head3=`echo ${headtext} | cut -d" " -f9-`
  # headline="${head1} > ${head2} > ${head3}"
  headline="Tr.ID Type > F_Tot. Proc% #Proc #Unused >J_Cre Wait Run Done >Fail Stalled"
  
  tempfile=temp-$$.log
  cat ${prodids_list} | while read fline ; do 
    (
      if [ ${fline:0:1} != "#" ] ; then 
        f=`echo ${fline} | cut -d":" -f1 `
        trans=`echo summaryTransformations $f | dirac-transformation-cli | sed -e "s/  */ /g" | grep MC`
        transb=`echo ${trans} | sed -e "s/MCSimulation_ILD/Sim/g" -e "s/MCReconstruction_Overlay_ILD/Ovl/g"`
        trans1=`echo ${transb} | cut -d" " -f1-3 | sed -e "s/ Active//"`
        trans2=`echo ${transb} | cut -d" " -f4-7`
        trans3=`echo ${transb} | cut -d" " -f8-11`
        trans4=`echo ${transb} | cut -d" " -f12-`
        transtext="${trans1} > ${trans2} >${trans3} >${trans4}"
        echo get $f | dirac-transformation-cli > ${tempfile}
        geti=`echo get $f | dirac-transformation-cli`
        maxtasks=`grep MaxNumberOfTasks ${tempfile} | sed -e "s/ //g" | cut -d":" -f2`
        trname=`grep TransformationName ${tempfile} | sed -e "s/ //g" | cut -d":" -f2 | sed -e "s/ILD-Opt_//"`
        echo "** ${trname} "
        echo "${transtext} | ${maxtasks} " 
      else
        echo " " 
        echo ${headline} 
      fi
    )
  done
  rm -f ${tempfile}
}  

# ##############################################################
# ##############################################################
# Start main part of prodmon.sh
# ##############################################################
# ##############################################################

# Print help
if [ -z "$1" -o  "$1" == "help" -o "$1" == "h" -o "$1" == "-h" -o "$1" == "--help" ] ; then
  sed -e '/###HELP_END###/,$d' -e '1,/###HELP_BEGIN###/d' -e "s/^# / /g" -e "s/^#//" $0
  exit
fi

echo "Executing $0" 
echo "init_script is $1"

cron_script=$0
init_script=$1
if [ -z "${init_script}" ] ; then 
  echo "ERROR: A script to initialize environment is not defined."  
  exit 1
fi

curdir=$(cd $(dirname $0) ; pwd )
echo "Worker directory  is ${curdir}"  
source ${init_script}

if [ -z "${MCPROD_TOP}" ] ; then 
  echo "ERROR: MCPROD_TOP environment parameter is not defined."
  exit 1
fi
if [ ! -e ${MCPROD_TOP} ] ; then 
  echo "ERROR: MCPROD_TOP, ${MCPROD_TOP}, does not exist."
  exit 1
fi

# ##################################################

cd ${MCPROD_TOP}

logdir=$( pwd )/logs
mkdir -p ${logdir}
dstr=$(date +%Y%m%d-%H%M%S)
logfile=${logdir}/prodmon-${dstr}.log

lastlog=`/bin/ls -tr logs/prodmon-2*.log 2>/dev/null | tail -1`
if [ "x${lastlog}" != "x" ] ; then  
   isdone=`tail ${lastlog} | grep "========= prodmon.sh ALL COMPLETED"`
   if [ "x${isdone}" == "x" ] ; then 
      lognotyet=${logdir}/notdone-prodmon-${dstr}.log
      echo "### End of initialization " | tee -a ${lognotyet}
      echo "### ${dstr} : Last prodmon.sh does not completed yet. Log file is ${lastlog}" | tee ${lognotyet}
      echo "" | tee -a ${lognotyet}
      echo "========= Current directory : `pwd` " | tee -a ${lognotyet}
      echo "Hostname `hostname` at ${dstr} " | tee -a ${lognotyet}
      echo "" | tee -a ${lognotyet}
      exit 2
   fi
fi

echo "" | tee -a ${logfile}
echo "### End of initialization " | tee -a ${logfile}
echo "========= Current directory : `pwd` " | tee -a ${logfile}
echo "Hostname `hostname` at ${dstr} " | tee -a ${logfile}
echo "" | tee -a ${logfile}

( cd ${logdir} && ln -sf prodmon-${dstr}.log prodmon-latest.log )

# runtest_by_load.sh || ( echo "SKIP: exit prodmon.sh due to too many processes in this host"  && exit )

# ############################################### 
# Exec scripts listed in cronexe.list 
# ############################################### 
if [ -e cronexe.list ] ; then 
  echo "Execute commands in cronexe.list"
  exec_cronexe_list cronexe.list 2>&1 | tee -a ${logfile}  
else
  echo "No cronexe.list in ${MCPROD_TOP}" | tee -a ${logfile}

fi

# ###############################################
# Create production status summary, if prodids.list exist
# ###############################################
echo "" | tee -a ${logfile}
dstr=$(date +%Y%m%d-%H%M%S)

translog=${logdir}/translog-${dstr}.log
if [ -e prodids.list ] ; then 
  echo "========= Start to print production status ${dstr} " | tee -a ${translog} | tee -a ${logfile}
  make_production_summary prodids.list 2>&1 | tee -a ${translog} | tee -a ${logfile}
  ( cd ${logdir} && ln -sf translog-${dstr}.log translog-latest.log )
else
  echo "No prodids.list in ${MCPROD_TOP}" | tee -a ${logfile}
fi



# cat addtask.log
if [ -e ${translog} ] ; then 
  echo " " | tee -a ${logfile}
  echo "+++ Updating nbtasks " | tee -a ${logfile}
  export LANG=C
  export LC_ALL=C
  ( sleep 5 && date && my_addtask.sh ) 2>&1 | tee addtask.log | tee -a ${logfile}

  echo "+++ end nbtasks update"
  echo " "
fi

echo " " | tee -a ${logfile} 
dstr=`date +%Y%m%d-%H%M%S`
echo "========= prodmon.sh ALL COMPLETED : ${cron_script} ${dstr} " | tee -a ${logfile}

