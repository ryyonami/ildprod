# Configure cron scripts

## Overview
Scripts to monitor progress of production tasks are prepared in a directory, `auto`, which contains 
two directories, `starter` and `worker`. Scripts in `starter` are executed at a starter host by 
cron; login to a worker host, execute scripts in `worker` directory, get log file and copied to 
a web directory for viewing by browser. 

In order to use them, 
- Prepare scripts to initialize environment parameters at a starter host and a worker host, respectively.
- Edit cron tab at a starter host

## Environment parameters necessary for script at starter host.

Their help will be obtained by following commands at the top of `ILDProd` directory.
```
% ./auto/starter/start_exec.sh

% ./auto/starter/start_exec_and_cp.sh
```

## Worker scripts

Two scripts for a worker host is developed.
- `auto/worker/prodmon.sh` : Monitor progress of production task, namely check log files of 
`gensplit` step, monitor status of transformation, execute sequences of dst-merged files 
creation, uploading log files and update elog data server.
- `auto/worker/errormon.sh` : Check log files of simulation and reconstruction jobs and create 
a summary.

Following is an example of cron tab definition at a starter host.
```
# For prodmon.sh 
0-59/15 * * * * ( source <init_starter> && <path_to_starter>/start_exec.sh prodmon.sh )

# Error chek monitor
1-59/30 * * * * ( source <init_starter> && <path_to_starter>/start_exec_and_cp.sh errormon.sh )

```
`<init_starter>` is the file to set environment parameters for starter script.
`<path_to_starter>` is the directory of starter script. 
