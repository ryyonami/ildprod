# A manual for the ILD production operator
A version in 4 December, 2020. Modification in progress for mc-2020 production(v03-02).

## Version information 
- From v03-00, interpretation of `process_type` of generator meta data, `evtclass`, and `evttype` are 
changed. `evtclass` is derived from the `fileurl` of generator meta data. The sub-directory of 
lowest directory tree level is used. `evtclass` is used for the lowest level sub-directory name
which are produced by production, including gensplit, sim, rec, dst... In the excel file, 
column `sortkey` is renamed to `subgroup` and `evttype` is added. `evttype` is directory used
by the production scripts.  In most cases, `evttype` is equal to the `process_type` of generator meta data,
but could be different depending on the production needs.

## Overview
ILD production task consists of several steps.

## Step 1. Preparation of generator data on dirac, etc.
### 1.1 Overview
ILD production relies on the following generator data, meta information, etc.
- Generator meta data and log files on DIRAC. 
 - Generator files in `/ilc/prod/ilc/mc-2020/generated` 
 - Meta file and log file in `/ilc/prod/ilc/mc-2020/generatorlog`. 
A script, `cron/syncdesy.sh`, at `ild01.ngt.ndu.ac.jp` rsyncs meta data at DESY-SRM to a local directory,
which is available https://ild01.ngt.ndu.ac.jp/CDS/desy-pnfs/mc-2020/generatorlog/.
An old method to do this by yourself may be found in [here](genmeta_old_howto.md).
The script is executed when an update is reported by the generator group.
- `genmetaByID.json` and `genmetaByFile.json`, json files collecting all generator meta data information. 
The cron script creates these files when new meta files are found and save at 
https://ild.ngt.ndu.ac.jp/CDS/files after checking the format by `genmeta-check.py` mentioned below.
- Then,  
 - (a) Replicate files ( generator, meta, and log files ), from DESY to KEK. If files are originally 
prepared at KEK, replicate to DESY first before `syncdesy.sh` mentioned above. 
 - (b) Set meta data of generator files in DIRAC catalog by `setILCGen_Metadata.py`. 
 - (c) Add meta data to [ELOG genmeta data base](https://ild.ngt.ndu.ac.jp/elog/genmeta/) by `elog-add-genmeta.py`.  

### 1.1 syncdesy.sh

For details, see comments in `syncdesy.sh`.

### 1.2 Replicate files

`dirac-dms-create-replication-request` has been used to replicate files.

### 1.4 Check downloaded data for correctness. 
A command to do this task is in `ILDProd/gensamples/genmeta-check.py`.
A web version of this program, https://ild.ngt.ndu.ac.jp/metacheck, can be used 
before registering meta file at the official location.

The list of known sub-directories is at `ild01.ngt.ndu.ac.jp:/home/www/html/mc-prod/files2017/genmeta/known-evttype.txt`.
Update the file when new sub-directory is created, otherwise you will see the waning message.

`genmeta-check.py` is executed when meta files are rsync-ed from DESY-SRM. The log file is available at 
https://ild.ngt.ndu.ac.jp/CDS/files/genmeta-check/. Note that the log file is created only when new files 
are obtained from DESY.

### 1.5 Set meta information to generator files on DIRAC catalog.
If this is a new sub-directory, set meta data to the generator sub-directory. A command would be 
```
dirac-dms-filecatalog-cli 
FC:/> meta set /ilc/prod/ilc Machine ilc
FC:/> meta set /ilc/prod/ilc/mc-2020/generated Datatype gen
FC:/> cd /ilc/prod/ilc/mc-2020/generated
FC:/> meta set 250-SetA Energy 250 MachineParams SetA
FC:/> meta set 250-SetA/<sub_directory> EvtClass <sub_directory>
```
Usually only last `meta set` is necessary. Others are necessary only when new directory is created.

Meta data of each files can be set by `setILCGen_Metadata.py` in `ILDProd/bin`.
Following is an example to set meta data.
```
setILCGen_Metadata.py --nodry <URL to a meta file or a local meta file>
setILCGen_Metadata.py --nodry -G "glob_string_to_search metafiles"
```

Further options will be obtained by 
```
setILCGen_Metadata.py -h
```
An option `--nodry` should present to actually register metadata.

### 1.6 Register meta data to ELOG
The script, `elog-add-genmeta.py`, in `ILDProd/bin` could be used to create an elog entry from a meta file.
The command usage is
```
   elog-add-genmeta.py <meta_file>
```
`elog-add-genmeta.py -h` displays more help information.

If <meta_file> begins with `http://` or `https://`, it is considered as a web accessible.

### 1.7 <i>This section is not updated.</i> Upload generator data and meta data to DIRAC, usually done by generator group 
Samples produced at SLAC are initially placed in SLAC web site. Generator files are added to DIRAC by 
Tim, someone in generator group, or by ourselves.  Meta data and job logs of generator samples 
produced at SLAC are prepared at SLAC web. They should be copied to the standard location on 
ILCDIRAC catalog with DESY-SRM as SE.  As long as KEKCC is concerned, `robot.txt` at SLAC web site 
disables a recursive downloading of whizard job outputs, thus following tool downloads only the meta file. 

To help these task, following scripts are prepared in `ILDProd/gensamples`
- `get_meta_from_slac.sh` : Download meta files from Tim''s web site. 
- `meta_ul_rep.sh` : Upload downloaded meta files to DIRAC ( and replicate to other sites )

Samples produced at DESY are stored in DIRAC by the generator WG. Meta data and job logs as well. 
If not ask a member of generator WG or copy by ourselves.
Samples produced at KEK are stored in DIRAC by the generator WG. Meta data and job logs should be as well.


## Step 2. Run batch jobs to estimate a required CPU times and data size produced.

### 2.1 Overview
These information are used to determine the number of events per jobs.

Tools are in `ILDProd/cpusize`.  The latest running example will be found in `/group/ilc/users/miyamoto/mcprod/201006-cpusize`.
Tools to run batch jobs are in `Overlay` directory. The job results are converted to an excel file, `prodpara.xlsx`,
by tools in `ProdPara`

Procedure to create `prodpara.xlsx` consists of following steps.

- Step in `Overlay` directory.
 - Prepare `runcond.json`, which contains parameters for test jobs. Parameters such as IP vertex smearing and offset 
and names of background files are described in `runcond.json`, which is created by `make_rundb.py`. Background files 
should be present in local host. Just one file is sufficient.

 - A list of input generator files, one for each processes, in `stdhep_list`

 - Create `version.py`, defining ILCSoft/ILDConfig version, and `mcprod_config.py` as a symbolic link to the one in `ILDProd/mcprod/v03-02`.

 - Edit `suball.sh` and submit jobs.

 - When job successfully finished, create `cpusize.json` by `cpusize.py`. `cpusize.py` scans `jobs-*` directory and create  a file, `cpusize.json` to summarize CPU and data size information.

- Step in `ProdPara` directory.
 - In a sub-directory, `nevgen`, create a file `newids-<some_thing>.txt`, which is a list of "I" prefixed process ID ( I123456, for example ) of new samples. For example, a command
```
   jsonread ../../Overlay/cpusize.json 6f -k | grep "I" | tee newids-250-SetA-6f.txt
```  
selects process IDs included in 6f category of a file, cpusize.json, and creates a newids file.

 - Run `prodpara.py` to create a file, `prodpara.xlsx`. `./prodpara.py -h` shows the help. For example, an excel file for higgs sample was created by ,
```
  ./prodpara.py --nevgen_json nevgen/nevlist.json --cpusize_json ../Overlay/cpusize.json \
   -s 1300.0 -f 250-higgs.xlsx higgs
```
  `-s` option defines a file size ( in GB ) of simulated file.  Adjust it properly to decide a proper number of events per job.  

  `subgroup` and `evtclass` of `prodpara.xlsx` are determined automatically. They should be reviewed and modify if necessary by `excel` before being used for production. 
  

### 2.2 Important columns of `prodpara.xlsx`
Excel column and its name, followed by its meaning. Values of those marked by `(*)` are mandatory.

A:`group(*)` | group

B:`subgroup(*)` | Those with same `subgroup` value are produced by the production 

C:`evtclass(*)` | evtclass is same as the sub-directory name of generated files,
                | which is derived from fileurl of generator meta file.
                | but define here for backward compatibility

D:`evttype(*)` | evttype is used for output sub-directory of gensplit/sim/rec/dst/... files
               | evttype = process_type of generator meta data, except some exceptions

E:`process_id(*)` | generators process ID

F:`process_name(*)` | process_name of generator meta data. Should be same as the `P` keyword of the file.

G:`process_type(*)` | process_type of generator meta data. Not used by the scripts, but present for easy checking with evtclass.

H:`pol(*)`        | polarization meta key e+ and e- combined. Smearing/offset parameter for IP position are decided by `e` and `p` 
key values in the file name. ( this feature may be modified in future ). No smearing/offset in the case of `e0.p0`.

I:`genname(*)` | Generator name meta key, which is used for the file name

J:`totev_gen` | Total number of events generated.

K:`totev_plan` | Total number of events planned for production.

L:`totev_submit(*)` | Total number of events which should be produced.

M:`nbevts_perjob(*)` | Number of events per job.

N:`nbjobs` | Number of jobs to produced.

AA:`nbfile_genuse(*)` | Number of generator files to be processed.

AB:`filename(*)` | 1st generator file. The number of digit of the file sequence metakey ( a number before stdhep ) should be same for the all files.

AC:`filedir(*)`  | Data directory of generator files.


## Step 3. Create production directories and scripts

ILD production consists of following 5 steps.
- 1_initProd : Initialize of production parameters
- 3_gensplit : Split generator files and upload to DIRAC
- 4_subprod : Start simulation and reconstruction production. Main part of the production.
- 6_dstmerge : Merge dst files.
- 7_logupload : Upload simulation and reconstruction job logs
A script, `init_production.sh`, is used to create sub-directories and files for them,
reading excel file created in Step 2, `prodpara.xlsx` for a example. 

Help of `init_production.sh` command is displayed by 
```
init_production.sh -h
``` 
Examples to run `init_production.sh` will be found in 
`ILDProd/mcprod/`*VERSION*`/initdir` 
and `ILDProd/mcprod/testprod/initdir`

### Format of production-list.txt file
This file, given by `--prodlist` argument, provides a key information to select processes from the excel file.
Each line of this file has the following format
```
<subgroup>[:<subdir>][/<procID1>,<procID2>,<procID3>,,,]
```
Note that values other than `<subgroup>` are options.  If only `<subgroup>` is given,
the rows with the same `subgroup` column value in the `procID` sheet is searched 
and used for the production. The sub-directories for the productions are created 
in a directory `<sort_key>`. Values after `<sort_key>` are options. 
If `:<sub_dir>` is present, the directory name will be `<subgroup><subdir>`. 
If `/<procID1>,<procID2>,...` are specified, the only processes with process 
`ID = <procID1>, <procID2>, ...` with the same `<subgroup>` are used for the production.

Other options, such as the output SE, banned sites, type of production, etc 
are arguments of `init_production.sh` script. All parameters are 
written in `1_prepProd/production.json` and subsequent production steps 
obtain production parameters and switches from this json file. So, 
the operator could edit a file, `production.json`, manually 
for non-standard setting.  

Only exception is the parameter, `GENSPLIT_DEFAULT_NPROCS`, which should be 
provided as the environment parameter when `init_production.sh` is executed.
It is used to define the number of processes to be executed simultaneously 
to upload gen-splitted files. The larger the number, faster the upload time 
but higher the chance to screw up SE.
`GENSPLIT_DEFAULT_NPROCS` could be modified file-by-file during the `3-gensplit` 
execution by editing ( adding ) `gensplit.opt` or `common.opt` for all files.
Both `gensplit.opt` and `common.opt` are sourced before actual start up of 
gensplit-ting and file upload.


### Production using the existing gensplit-ed files.
```
--reuse_gensplit <prodID of gensplited files> 
--elogid_old <elogID of gensplitted files>
--nw_perfile <nw_perfile used by the gensplit job>
```
`--nw_perfile` option is required when this information is different with the value given by 
the excel file.

## Step 4. Start production

Directories for production should be listed in `startprod.list`.

To Start production sequence, do `startprod.sh go 2>&1 >> startprod.log`.
If no argument is given to `startprod.sh`, a terminal input is promped.
`startprod.sh` read the first line of `startprod.list`, cd to the directory. 
execute production steps, **prepProd**, **gensplit** and **subprod**. 
The readiness of ILCDirac is checked before actually starting the sequence. 
The conditions for the readiness is defined in `startprod.opt`, which includes the following 
parameters.

```
max_job_insys    : Maximum number of jobs running and waiting in ILCDirac.
max_sim_transfer : maximum number of simulation transformation
DESYSRM_GBlimit  : SIM data size used at DESY-SRM SE.
KEKDISK_GBlimit  : SIM data size usec at KEK-DISK SE
sleep_before_next_loop: If above condition is not satisfied, sleep 30 min., then retry.
```

The prepProd step creates the entroy in Elog and directory to upload gensplit files in DIRAC catalog.
The gensplit steps split the generator file, upload to DIRAC and set meta data to them. It usually takes
a long time, 1 day or more depending on generator process. The subprod step, which creates 
ILCDirac transformation, starts 10 minutes after the begining of the gensplit in order for files 
being shown up in ILCDirac.

## Step 5. During the production

A cron job running at ild01.ngt.ndu.ac.jp ( called starter host ) is used to execute production monitor scripts
at KEKCC ( called worker host ). 
How to configure cron job is described [here](CronConfiguration.md).

Monitor progress of production by ILCDirac web tools or a monitor created by cron jobs, 
running at [https://ild.ngt.ndu.ac.jp/mc-prod/prodmon/index.html](https://ild.ngt.ndu.ac.jp/mc-prod/prodmon/index.html)

To avoid too much data traffic, the number of running jobs should be less than a few thousands. 
This can be editing `gensplit.opt` in `gensplit` step or `number of tasks` parameter of ILCDirac transformation.

### Key files to monitor production status in `ILDProd/mcprod/v02-00-01`.
- `cronexe.list` : A list of commands which are executed by cron script, `ILDProd/mcprod/cron/1min/doexec.sh`. This is used to 
-- Monitor progress of gensplit task
-- Execute sequence of dst-merge and production job log upload after completion of production.

- `active-prod-dir.list` : A list of *active* production directories. *active* means ILCDirac transformation status is *active*

- `addtask.dat` : If the number of input files matched to InputData Query of ILCDirac transformation, the number of tasks for that transformation is increased by a cron job. But the total number of tasks added is limited to `maxadd` specified by this file. This is used to avoid adding too many tasks to the production.

- `prodids.list` : A list of *active* production IDs 

- `startprod.list` : A production queue which will be started next by `startprod.sh`

- `startprod.done` : List of productions started so far.

- `error_chk.status.log` : Output of production error monitor.

- `errsum-all.log` : Summary of production error.

### Scripts to help production task.

Following are helper scripts for production task is prepared in `scripts` directory.

- `reset_file.sh` : Reset input file status of transformation so as to re-process stacked files. Be careful DO NOT TO 
duplicate recovery process which are initiated automatically by ILCDirac server.

- `done_production.sh` : When production is considered to be finished, use this script to (1) set Transformation status to *stop*, 
(2) edit `cronexe.list` to start dst-merge and log upload steps.
This scripts create a file, `prodstatus.log`, in `6_dstmerge` and `7_logupload` directories. This file is required
to initiate dstmerge and logupload tasks by `task_step6.sh` and `task_step7.sh` scripts.

## Step 6. DST-merge and log upload.

### Instruction for the DST-Merge task.

- A script, `task_step6.sh`, executes a set of scripts used to group DST files, create DIRAC user jobs to create DST-Merged files, 
and replicate them to relevant SEs.  A file, `prodstatus.log`, should exist in a directory, `6_dstmerge`, to initiate a merge DST scripts
by `task_step6.sh`. Usually, it is created by `done_production.sh`.

- `task_step6.sh` monitors the status of previous sub-step. If completed, execute the script for the next-step.
Usually following sub-steps takes a long time.  
-- Creating DST merge job scripts, to find all DST files and generator files used for the production. 
-- Waiting completion of DST merge job. 
-- Replication of DST-merge files. 

- Note that DST-merge job fails some time. The operator should monitor the status of the DIRAC user jobs and re-schedule failed jobs using the ILCDirac web interface if necessary.

- If DST-merge job failed, get the log of failed jobs and investigate the reason of job failure, fix the problem, and re-submit the job. 
A script, `dstm_subone.py`, in `scripts` directory is an example to submit job. Edit this file and submit the user job again.

- If sample is new EvtType, DetectorMode, or ILDConfig, directory meta data of dst-merged sample must be defined. This should be done after a creation of directories by merge dst jobs, or completion of dst merge step.  `dstm-meta-set.sh` can be used to set directory meta data. An example to do this is
```
dstm-meta-set.sh -s 500-TDR_ws np-light-higgs
```
Further help of this command is obtained by `dstm-meta-set.sh -h`.

## Step 7. Get jobout and upload to log directory.

### About this step.

- Job log is stored in a dirac directory which is not registered in DIRAC File catalog. In step 7, log files are downloaded from a DIRAC LOG directory, then upload them with DIRAC logical file name. 
- A script, task_step7.sh, does this task. A file, `production.done`, must present to start a sequence.
- To get all relevant job log, do `postprod-new.sh <prodID>`. All log information is obtained in a directory called `prod<prodID>`.

## Additional information

### Elog 
Elog entry is updated automatically in each production steps. But make sure that ELOG information is correct or add additional information when relevant.

### Confluence

### Production summary
Following web sites are prepared as production summary web sites; 
- https://ild.ngt.ndu.ac.jp/mc-prod/prodmon/summary-by-evttype.html
- https://ild.ngt.ndu.ac.jp/elog/opt-data

These web sites are updated by following procedures.

- `elog_summary.py` : This script reads [https://ild.ngt.ndu.ac.jp/elog/dbd-prod/](ELOG dbd-prod) data base
and create a json file for subsequent procedure.  An example procedure to do this ( for production of elogID 240 
which uses prodpara.xlsx for init_produciton script ) is as follows.
```
% mkdir -p elog-dbd-prod
% elog_summary.py 240 --prodpara prodpara.xlsx 
```
This creates `elog-dbd-prod-240.json` in `elog-dbd-prod`.  
Then copy `elog-dbd-prod-240.json` to `mc-prod/prodmon/elog-dbd-prod` directory of 
the web server. A cron script running on the server, `prod-summary/update_html.sh`
detects new file in this directory and recreate `summary-by-eventtype.html` if found.

Other script, `write-opt-data.py` reads `elogdbs.json` and are used for adding 
records to elog opt-data database. For example, 
```
     write-opt-data.py -i elogdbs.json
```
write all information to elog opt-data database.
