#!/bin/env python
"""
Modified from dirac-ilc-moving-transformation for moving ILD SIM data from DESY-SRM to KEK-SRM
simulation information is taken from files, simprod_*.json files in current directory

2021-12-7 Akiya Miyamoto 
 
"""
"""Create a transformation to move files from one storage element to another.

Example::

  dirac-ilc-moving-transformation <prodID> <TargetSEs> <SourceSEs> {GEN,SIM,REC,DST} -NExtraName
  dirac-ilc-moving-transformation --AllFor="<prodID1>, <prodID2>, ..." <TargetSEs> <SourceSEs> -NExtraName [-F]

Options:
   -A, --AllFor    list        Comma separated list of production IDs. For each prodID three moving productions are
                               created: ProdID/Gen, ProdID+1/SIM, ProdID+2/REC
   -F, --Forcemoving           Move GEN or SIM files even if they do not have descendents
   -N, --Extraname string      String to append to transformation name in case one already exists with that name
   -R, --GroupName <value>     TransformationGroup Name, by itself the group of the prodID
   -G, --GroupSize <value>     Number of Files per transformation task
   -x, --Enable                Enable the transformation creation, otherwise dry-run

:since:  Dec 4, 2015
:author: A. Sailer
"""
from pprint import pformat

from DIRAC.Core.Base import Script
from DIRAC import gLogger as LOG, exit as dexit
from DIRAC.TransformationSystem.Utilities.ReplicationTransformation import createDataTransformation

from ILCDIRAC.ILCTransformationSystem.Utilities.DataParameters import Params as _Params, checkDatatype, \
    getTransformationGroup


import json, os, glob

from ElogTools import putelog_text
 
__RCSID__ = "2041f2c6 (2021-03-24 14:47:12 +0100) Andre Sailer <andre.philippe.sailer@cern.ch>"


def registerSwitches(clip, script):
  """register additional switches for moving transformations."""
  script.registerSwitch("A:", "AllFor=", "Create usual set of moving transformations for prodID/GEN, prodID+1/SIM"
                        " prodID+2/REC", clip.setAllFor)
  script.registerSwitch("F", "Forcemoving", "Move GEN or SIM files even if they do not have descendents",
                        clip.setForcemoving)


def _createILDTrafo():
  """reads command line parameters, makes check and creates replication transformation."""
  clip = _Params()
  clip.flavour = 'Moving'
  clip.plugin = 'BroadcastProcessed'
  clip.groupSize = 10
  clip.registerSwitches(Script)
  registerSwitches(clip, Script)
  Script.parseCommandLine()
  args = Script.getPositionalArgs()

  # if not clip.checkSettings(Script)['OK']:
  #   LOG.error("ERROR: Missing settings")
  #   return 1
  simprods = glob.glob("simprod_*.json")
  LOG.always("There are %d simprod files in current directory" % len(simprods))
  for index, simprod in enumerate(simprods):
    LOG.always("Creating moving transformation for SIM files defined in %s" % simprod)

    """ get parameters from simprod_.json file """

    jsondata = json.load(open(simprod))
    prodID = jsondata["status"]["ProdID"]
    srcse = jsondata["sim"]["SE"]
    elogid = jsondata["conf"]["__ELOG_ID"]
    procname = jsondata["conf"]["__ELOG_PROCNAME"]

    #if srcse != "DESY-SRM":
    #  LOG.error("Source SE of SIM data is %s and not DESY-SRM" % srcse)
    #  return 1

    clip.targetSE = 'KEK-SRM'
    clip.sourceSE = 'DESY-SRM'
    clip.datatype = 'SIM'
    clip.metaKey = 'ProdID'
    clip.metaValues = [prodID]
    clip.extraname = "_".join([clip.datatype, str(elogid), procname])
    datatype = clip.datatype

    if clip.forcemoving:
      LOG.notice('Forced moving: setting plugin to "Broadcast"')
      clip.plugin = 'Broadcast'
    retData = checkDatatype(prodID, datatype)
    if not retData['OK']:
      LOG.error("ERROR: %s" % retData['Message'])
      print(retData)
      return 1
    tGroup = getTransformationGroup(prodID, clip.groupName)
    parDict = dict(flavour=clip.flavour,
                   targetSE=clip.targetSE,
                   sourceSE=clip.sourceSE,
                   metaKey=clip.metaKey,
                   metaValue=prodID,
                   extraData={'Datatype': datatype},
                   extraname=clip.extraname,
                   plugin=clip.plugin,
                   groupSize=clip.groupSize,
                   tGroup=tGroup,
                   enable=clip.enable,
                   )

    LOG.notice('Parameters: %s' % pformat(parDict))
    resCreate = createDataTransformation(**parDict)
    if not resCreate['OK']:
      LOG.error('Failed to create the transformation', resCreate['Message'])
      return 1
    else:
      if resCreate['Value'] != None:
        atrans = resCreate['Value'].getTransformation()
        if atrans['OK']:
          transname=atrans['Value']['TransformationName']
          transid=atrans['Value']['TransformationID']
        else:
          transname = "unknown"
          transid = "unknown"

        msg = "Created moving transformation of SIM files from DESY-SRM to KEK-SRM. Transformation_id=%s" % str(transid)
        msg += " name=%s" % transname
        putelog_text(elogid, {"msg_file":"", "message":msg})
        LOG.notice(msg)

  return 0


if __name__ == '__main__':
  dexit(_createILDTrafo())
