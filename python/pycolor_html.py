
class pycolor:
    BLACK = ''
    RED = '<font color=\"red\">'
    GREEN = '<font color=\"green\">'
    YELLOW = '<font color=\"yellow\">'
    BLUE = '<font color=\"blue\">'
    PURPLE = '<font color=\"purple\">'
    CYAN = '<font color=\"cyan\">'
    WHITE = '<font color=\"white\">'

    DARK_GRAY = '<font color=\"darkgray\">'
    LIGHT_RED = '<font color=\"orange\">'
    LIGHT_GREEN = '<font color=\"lightgreen\">'
    BROWN = '<font color=\"brown\">'
    LIGHT_BLUE = '<font color=\"lightblue\">'
    LIGHT_PURPLE = '<font color=\"blueviolet\">'
    LIGHT_CYAN = '<font color=\"lightcyan\">'
    LIGHT_GRAY = '<font color=\"lightgray\">'


    END = '</font>'
    BOLD = ''
    UNDERLINE = ''
    INVISIBLE = ''
    REVERCE = ''

    c = {"black":BLACK, "red":RED, "green":GREEN, \
            "yellow":YELLOW, "blue":BLUE, "purple":PURPLE, \
            "cyan":CYAN, "white":WHITE, \
            "end":END, \
            "dark gray":DARK_GRAY, "light red":LIGHT_RED, "light green":LIGHT_GREEN, \
            "brown":BROWN, "light blue":LIGHT_BLUE, "light purple":LIGHT_PURPLE, \
            "light cyan":LIGHT_CYAN, "light gray":LIGHT_GRAY, 
            "bold":BOLD, "underline":UNDERLINE,  \
            "invisible":INVISIBLE, "reverce":REVERCE}

    def __init__(self):
        ''' Initialize '''
        ''' print "Hello "  ''' 

    def cdump(self):
        ''' print colored text '''
        for k, v in sorted(self.c.items()):
            print( v + k + self.c["end"] )

if __name__ == "__main__":
    pyc = pycolor()
    pyc.cdump()
