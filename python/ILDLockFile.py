#
# get gensplitID
#

import fcntl
import os
import datetime
import time

_gensplit_id="/group/ilc/users/miyamoto/optprod/gensplit/queue/splitid"
_gensplit_lockfile="/group/ilc/users/miyamoto/optprod/gensplit/queue/lock"
_gensplit_history="/group/ilc/users/miyamoto/optprod/gensplit/history"

def get_gensplit_id(**args):
    ''' getn gensplit id from the stanard place. '''

    dryrun = True if "dry" in args else False

    # Make sure lock file does not exist
    removeOK = False
    for i in range(0, 10):
      if os.path.exists(_gensplit_lockfile):
          print( "waiting " + _gensplit_lockfile + " is removed." )
          time.sleep(2)
      else:
          removeOK = True
          break         

    if not removeOK:
        return -2

    if not os.path.exists(_gensplit_lockfile):
        flock = open(_gensplit_lockfile,"w")
        flock.write("gensplit_lock")
        flock.close()

    done = False
    loopcount = 0
    quitloop = False
    flockno = open(_gensplit_lockfile)
    while not quitloop:
        loopcount += 1
        try:
            fcntl.flock(flockno, fcntl.LOCK_EX | fcntl.LOCK_NB )
        except IOError:
            print( "waiting " + _genfile_lockfile + " is unlocked." )
            time.sleep(2)
            done = False
            if loopcount > 10:
                quitloop = True
            continue

        fidline = open(_gensplit_id).readlines()
        splitid = long(fidline[0].replace("\n","")) + 1
        # print "next id is "+str(splitid)
        fidline = open(_gensplit_id, "w")
        fidline.write(str(splitid)+"\n")  # write new splitid
        fidline.close()
    
        # Upload history file.
        now = datetime.datetime.now()
        dtstr = "%4.4d%2.2d%2.2d-%2.2d%2.2d%2.2d" % (now.year, now.month, now.day, 
    		now.hour, now.minute, now.second)
        logname = os.getlogin()
    
        message = "added by gen_split_id in ILDTools" if "message" not in args else args["message"]
        outstr = "%6.6d %s | sub by %s : %s\n" % ( splitid, dtstr, logname, message )
        fhist = open(_gensplit_history, "a")
        fhist.write(outstr)
        fhist.close()
    
        done = True
        quitloop = True

    fcntl.flock(flockno, fcntl.LOCK_UN)
    flockno.close()
    os.remove(_gensplit_lockfile)

    if not done:
       return -1
    
    return splitid


# ===========================================================
if __name__ == "__main__":
    print( get_gensplit_id(message="Test messgae", dry=False ) )
