#
# Get access to ILD tools
#

import os
import time
import json
import urllib
from urllib2 import urlopen
import ssl
import argparse

IDSERVER = "https://ild.ngt.ndu.ac.jp/cgi-bin/serial_numbers"

# ###########################################################
def call_ildidtool(**args):

    if hasattr(ssl, '_create_unverified_context'):
        ssl._create_default_https_context = ssl._create_unverified_context

    if "idtype" not in args or args["idtype"] not in ["gensplit", "processid"]:
       emsg = "ERROR: idtype not specified or it is neither gensplit nor processid "
       ret = {"OK":False, "Message":emsg}
       return ret

    if "func" not in args or args["func"] not in ["next", "history"]:
       emsg = "ERROR: func not specified or it is neither next nor history"
       ret = {"OK":False, "Message":emsg}
       return ret

    if args["func"] == "next":
       username=os.environ["LOGNAME"]
       idnumkey_file = os.environ["IDNUMBER_KEY_FILE"]
       idnumkey = open(idnumkey_file).read().replace("\n","")
       logtext = urllib.quote(args["message"]) if "message" in args else urllib.quote("not specified.")

       url = IDSERVER + "/idtool?type=%s&func=%s&numberofids=%s&username=%s&numberkey=%s&logtext=%s" % \
         ( args["idtype"], args["func"], args["numberofids"], username, idnumkey, logtext )
 
    elif args["func"] == "history":

       url = IDSERVER + "/idtool?type=%s&func=%s&max=%s" % \
         ( args["idtype"], args["func"], args["maxline"] )

    else:
       emsg = "ERROR: func, " + args["func"] + " is not defined."
       ret = {"OK": False, "Message": emsg}
       return ret


    ##
    result = None
    # print "URL="+url
    # if result == None: 
    #    return {"OK":False, "Message":"debug"}

    try:
       result = urlopen( url ).read()
    except ValueError :
       ret = {"OK":False, "Message": "ERROR: Failed to access " + url }
       return ret 

    res = {"OK":True, "Message":result, "ERROR":[], "HISTORY":[]}
    for line in result.split("\n"):
       if ":" in line:
          (idline, linetext) = line.split(":",1)
          if idline == "ERROR":
             res["ERROR"].append(linetext)
          elif idline == "HISTORY":
             res["HISTORY"].append(linetext)
          elif idline == "LASTID":
             res["LASTID"] = linetext
          elif idline == "NEXTID_FROM":
             res["NEXTID_FROM"] = linetext
          elif idline == "NEXTID_TO":
             res["NEXTID_TO"] = linetext
    
    return  res

# ##########################################################
def run_cmd():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter, 
    description="\n".join(["ILD serial number tool for gensplit and processID. Following functions are available.",
         "    next : get the ID number(s) next to the last one.",
         "        To use next, the environment parameter IDNUMBER_KEY_FILE should be defined.",
         "        It is a path to the key file and the same key should be stored in the file defined by the server program.",
         "    history : Show the history of pass numbers.",
         "        URL to get history info. is %s/idtool?type=<idtype>" % IDSERVER ]))    
    parser.add_argument("idtype", help="idtype, a type of ID numbers", choices=["gensplit", "processid"])
    parser.add_argument("func", help="function", choices=["next", "history"])
    parser.add_argument("-m", "--message", help="A short message to be included in history. Mandattory when next function is used.",
                              dest="message", action="store", default="None")
    parser.add_argument("-l", "--line", help="Number of history to be obtained.", 
                              dest="maxline", action="store", default="10")
    parser.add_argument("-n", help="Number of IDs to get. Multiple sequential numbers are obtained.",
                              dest="numids", action="store", default="1")

    args = parser.parse_args()
    idtype = args.idtype
    func = args.func
    # username = args.username
    message = args.message
    maxline = int(args.maxline)
    numids = int(args.numids)

    ret = call_ildidtool(idtype=idtype, func=func, message=message, 
                         maxline=maxline, numberofids=str(numids) )

    if ret["OK"] == False:
       print( ret["Message"] )
    else:
       for k in ["ERROR", "HISTORY"]:
          if len(ret[k]) > 0:      
              print( "\n".join(ret[k]) )

       for k in ["LASTID", "NEXTID_FROM", "NEXTID_TO"]:
          if k in ret:
              print( k + "=" + ret[k] )

# ##########################################################
def get_nextid(idtype="", logtext="dummy"):
  
    ret = call_ildidtool(idtype=idtype, func="next", message=logtext, numberofids="1")
    if ret["OK"] == False:
        print( ret["Message"] )
        return {"OK":False}
    else:
        return {"OK":True, "NEXTID":ret["NEXTID_TO"]}
 
# ##########################################################
if __name__ == "__main__":
    run_cmd()
