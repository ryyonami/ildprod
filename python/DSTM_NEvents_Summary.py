#!/bin/env python 

# Scan DTM job output in ILD-DSTM-* 
# and found the number of events of dst-merged files.

import os
import glob
import pprint

#######################################################################
def create_dstm_nevents_summary():
    ''' Create a file, summarizing the nevents of dstm file '''
    ret = {"OK":True}
    filemeta = {}
    smfiles = glob.glob("ILD-DSTM-*/*/setfilemeta.cli")
    nevtotal = 0L
    
    for f in smfiles:
       metainfo = {}
       # print "reading setmetafile %s" % f
       for line in open(f):
         # print line
         if "meta set" in line:           
           nwords = line.replace("\n","").split()
           if len(nwords) < 5 :
              print( "ERROR : Not enough words in the following line:" )
              print( line )
              print( "Input file was $f" )
              print( "Can not proceed any more" )
              exit(20)   

           (smeta, sset, fname, name, value) = line.replace("\n","").split()
           metainfo[name] = value
           if "filename" not in metainfo:
              metainfo["filename"] = os.path.basename(fname)
              metainfo["dirname"] = os.path.dirname(fname)
       filemeta[metainfo["filename"]] = metainfo

    # Create summary table base on the production ID
    summary = {}
    for k, v in filemeta.items():
       fkeys = v["filename"].split(".")
       (dstmprod, lser) = fkeys[-2].rsplit("_",1)
       filekey = ".".join(fkeys[:-3]+["*",dstmprod+"_*",fkeys[-1]])
       # print(" k="+k + " dstmprod="+dstmprod+" lser="+lser)
       # pprint.pprint(v)
       prodid=dstmprod.split("_")[2]
       gen_process_id = v["GenProcessID"]
       if filekey not in summary:
          summary[filekey] = {"GenProcessName":v["GenProcessName"], "GenProcessID":v["GenProcessID"],
			      "NumberOfEvents":long(v["NumberOfEvents"]), "NumberOfDSTMs":1}
          # summary[filekey]["ProdID"] = v["ProdID"]
          summary[filekey]["ProdID"] = prodid
          summary[filekey]["dirname"] = v["dirname"]
          summary[filekey]["filekey"] = filekey
       else:
          summary[filekey]["NumberOfEvents"] += long(v["NumberOfEvents"])
          summary[filekey]["NumberOfDSTMs"] += 1


    # pprint.pprint(summary)

    dirold = ""
    outbuf = []
    nfound = 0
    for k, v in sorted(summary.items()):
       nfound += 1
       if v["dirname"] != dirold:
          outbuf.append("")
          outbuf.append("## Directory : "+v["dirname"])
          outbuf.append("    ProcID  NFile   NEvents  Filename")
          dirold = v["dirname"]

       outbuf.append("    %6d" % int(v["GenProcessID"]) + " %5d" % int(v["NumberOfDSTMs"]) + " %10d" % long(v["NumberOfEvents"]) +  "  %s" % v["filekey"] ) 
       nevtotal += long(v["NumberOfEvents"])

    if nfound < 1:
       ret["OK"] = False
       ret["message"] = "No DST-Merged files found."


    outf = "dstm-nevents-summary.txt"
    fout = open(outf,"w")
    fout.write("\n".join(outbuf))
    fout.close() 
    ret["message"] = "DST-merged file summary is written to "+outf
    ret["summary_file"] = outf
    ret["nevtotal"] = nevtotal
    return ret

# ###########################################################
if __name__ == "__main__":
   ret = create_dstm_nevents_summary()
   print( ret["message"] )
