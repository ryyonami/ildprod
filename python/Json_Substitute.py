#!/bin/env python 
# Substitute values in  %{ } and ${ } in json objects.
#

import os
import json
import pprint
import string

# ########################################################
class TemplatePercent(string.Template):
   delimiter = "%"
   def __init__(self, template):
     super(TemplatePercent, self).__init__(template)

# ########################################################
def Json_Substitute(inp):
    ''' expand %{ } and ${ } variable in inp and return '''

    newv = inp

    envkeys = os.environ.keys()
    envdict = {}
    for env in envkeys:
        envdict[env] = os.environ[env]

    for key, value in inp.items():
      # print str(key)+"==="+str(value)

      if isinstance(value, str):
          if "${" in value:
              orig = string.Template(value)
              conv = orig.substitute(**envdict)
              newv[key] = conv

    # pprint.pprint(newv)
    newv2 = newv
    # Convert only string object
    doRetry = True
    nloop = 0
    while doRetry:
        doRetry = False
        newv = newv2
        nloop += 1
        for key, value in newv.items():
            if isinstance(value, str):
                if "%{" in value:
                  orig = TemplatePercent(value)
                  table = {}
                  for k, v in newv.items():
                      if not isinstance(v, dict):
                          table[k] = v
                  conv = orig.safe_substitute(**table)
                  newv2[key] = conv
                  # print value + " was converted to " + newv2[key]
                  if "%{" in conv and nloop < 5 :
                     doRetry = True
    
    return newv2
