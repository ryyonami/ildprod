#!/usr/bin/env python 

import os
import pprint
import json
from datetime import datetime
from ILCDIRAC.Core.Utilities.FilenameEncoder           import decodeFilename, makeFilename
import copy
import pycolor
import sys
pyc=pycolor.pycolor()

if "MCPROD_CONFIG_FILE" not in os.environ:
    print( "ERROR FATAL : MCPROD_CONFIG_FILE environmnet parameter is not defined." )
    exit(-1)

_configfile = os.environ["MCPROD_CONFIG_FILE"]
if not os.path.exists(_configfile):
    print( "ERROR FATAL : configfile " + _configfile + " not exist" )
    exit(-1)
if sys.version_info.major == 2:
    execfile(_configfile)
else:
    exec(open(_configfile).read())
print( "Config file " + _configfile + " was executed by PreGenSplit.py" )


# #########################################################################################
def prepareConfFile(**args):
  ''' Create a json and conf file containing parameters for the production job '''

  pprint.pprint(args)  

  if "thisprod_config" in args:
     if os.path.exists(args["thisprod_config"]):
         if sys.version_info.major == 2:
             execfile(args["thisprod_config"])
         else:
             exec(open(args["thisprod_config"]).read())
         print("Parameters defined by "+_configfile+" was overwritten by "+args["thisprod_config"])
     else:
         print("### ERROR "+args["thisprod_config"]+ " does not exist, though it is given by argument.")
         exit(0)

  doTest = False if "doTest" not in args else args["doTest"]

  if doTest:
    testdata = "/ilc/prod/ilc/ild/test" if "PRODTESTDATA" not in os.environ else os.environ["PRODTESTDATA"]
    topdir="%s/%s" % (testdata, MCProd_MCOpt_DirTape)  # Do not include "/" at the end
    # split_nw_per_file = 10
    # split_max_read = 100  # nb. of reads include non-event record ( 1 header record at the begining of file )
  else:
    topdir = "/ilc/prod/ilc/%s" % MCProd_MCOpt_DirTape

  split_nw_per_file = args["nw_per_file"]  if "nw_per_file" in args else -1 # Need to be defined by caller
  split_max_read = 0 if "max_read" not in args else args["max_read"]  # 0 to read up to EOF
  topdir=topdir if "topdir" not in args else args["topdir"]  # Do not include "/" at the end

  evtclass = args["evtclass"]
  evttype  = args["evttype"]
# evtclass is same as the sub-directory name of generated files,
#    which is derived from fileurl of generator meta file.
# evttype is used for sub-directory of gensplit/sim/rec/dst/... files
#    evttype = process_type of generator meta data, except some exceptions


  splitdir = args["splitdir"] if "splitdir" in args else "split003"
  split_prodid = args["splitid"] if "splitid" in args else "500031"
  split_nb_files = args["nbfiles"] if "nbfiles" in args else "0"
  split_nseq_from = args["split_nseq_from"] if "split_nseq_from" in args else "1"
  split_skip_nevents = args["split_skip_nevents"] if "split_skip_nevents" in args else "0"

  energy="-500" if "energy" not in args else args["energy"]
  machinePara="unknown" if "machinePara" not in args else args["machinePara"] 
  if "energy" not in args:
     print( "ERROR : energy is not found in the input arguments to prepareConfFile" )
     exit(64)
  if "machinePara" not in args:
     print( "ERROR : machinePara is not found in the input arguments to prepareConnnnfFile" ) 
     exit(64)

  energyMachinePara='-'.join([energy, machinePara])
  
  split_selected_file = 1 if "selected_file" not in args else args["selected_file"]
  generator = "whizard-1.95" if "generator" not in args else args["generator"]
  se_for_data = args["se_for_data"] if "se_for_data" in args else "DESY-SRM"
  se_for_sim = args["se_for_sim"] if "se_for_sim" in args else ""
  se_for_rec = args["se_for_rec"] if "se_for_rec" in args else ""
  se_for_dstmerged = args["se_for_dstmerged"] if "se_for_dstmerged" in args else ""
  se_for_gensplit = args["se_for_gensplit"] if "se_for_gensplit" in args else "KEK-DISK"
  se_for_dstm_replicates = args["se_for_dstm_replicates"] if "se_for_dstm_replicates" in args else "KEK-DISK"
  se_for_logfiles = args["se_for_logfiles"] if "se_for_logfiles" in args else "DESY-SRM"


  # doTest = True
  diskdir = topdir.replace("/"+MCProd_MCOpt_DirTape,"/"+MCProd_MCOpt_DirDisk)

  conf={
  #
  "__ENERGY":energy,
  "__ENERGY_MACHINEPARA":energyMachinePara,
  "__ENERGY_MACHINEPARA_FOR_STDHEP":"0"+energyMachinePara,
  #
  "__EVTCLASS":evtclass,
  "__EVTTYPE":evttype,
  "__DSTMERGED_LOCALBASE":"",
  "__STDHEP_UPLOAD_SE":se_for_gensplit,
  "__SPLIT_NW_PER_FILE":split_nw_per_file,
  "__SPLIT_MAX_READ":split_max_read,
  "__SPLIT_NB_FILES":split_nb_files,
  "__SPLIT_NSEQ_FROM":split_nseq_from,    # Sequence number of first file.
  "__SPLIT_SKIP_NEVENTS":split_skip_nevents, # Number of events to skip before output
  "__STDHEP_GENERATOR":generator,  # Generator meta key of original file
  "__SPLIT_SELECTED_FILE":split_selected_file, 
  
  "__DIRECTORY_SIM":topdir+"/ild/",
  "__DIRECTORY_REC":topdir+"/ild/",
  "__DIRECTORY_STDHEPSPLIT":diskdir+"/ild/gensplit",
  "__DIRECTORY_DST":diskdir+"/ild/",
  "__DIRECTORY_SIMLOG":diskdir+"/ild/simlog",  
  "__DIRECTORY_RECLOG":diskdir+"/ild/reclog", 
  "__DIRECTORY_DSTMLOG":diskdir+"/ild/dstmlog", 
  "__DIRECTORY_DSTMERGED":topdir+"/ild/",
  "__PRODID_FOR_GENSPLIT":str(split_prodid)
  }
  conf["__DSTM_REPLICATES"] = se_for_dstm_replicates
  conf["__SIM_SE"] = se_for_data if se_for_sim == "" else se_for_sim
  conf["__REC_SE"] = se_for_data if se_for_rec == "" else se_for_rec
  conf["__DSTMERGED_SE"] = se_for_data if se_for_dstmerged == "" else se_for_dstmerged
  conf["__LOG_SE"] = se_for_logfiles

  conf["__SIM_NBTASKS"] = int(args["sim_nbtasks"]) if "sim_nbtasks" in args else -1
  conf["__REC_NBTASKS"] = int(args["rec_nbtasks"]) if "rec_nbtasks" in args else -1
  conf["__SIM_BANNED_SITES"] = args["sim_banned_sites"] if "sim_banned_sites" in args else ""
  conf["__REC_BANNED_SITES"] = args["rec_banned_sites"] if "rec_banned_sites" in args else ""

  conf["__WORKDIR"] = os.getcwd()
  conf["__STDHEP_UPLOAD_DIR"] = "/".join([conf["__DIRECTORY_STDHEPSPLIT"],
                                    energyMachinePara, evttype, splitdir])

  if "thisprod_config" in args:
     conf["__THISPROD_CONFIG"] = args["thisprod_config"]

  return conf

# #########################################################################################
def make_stdhepsplit(conf, list_file_of_input, datadir="", sTest=False):
  ''' Create a directory for stdhep split.  This is called by init_production '''
  # make sure input file 
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  if not os.path.exists(list_file_of_input):
    print( FAIL+'ERROR '+ENDC + 
           " "+list_file_of_input+" does not exist." )
    print( " It is a list of full path of generator files.")
    exit(2)

  
  # Make sure energy-machinepara and evetclass in datadir is consistent with those in the file name
  if datadir != "":
    if conf["__ENERGY_MACHINEPARA"] not in datadir:
       print( "ERROR : __ENERGY_MACHINEPARA(%s) is not consistent with datadir(%s)" % (conf["__ENERGY_MACHINEPARA"], datadir) )
       print( "Probably error in filenames/datapath in the excel file " )
       exit(3)

  # Start create script
  now = datetime.now()

  # allfiles = flist.readlines()
  # dirname = os.path.dirname(allfiles[0])  # Check dirname of first file.
  if len(datadir) > 0:  # Use datadir in the argument, if given
     dirname = datadir
     print( "STDHEP_SRC_LFNBASE="+dirname )
     dirname_clean = dirname.replace("lfn:/grid/ilc/","/ilc/").replace("lfn:/ilc/","/ilc/")
     conf["__STDHEP_SRC_LFNBASE"] = dirname_clean
     conf["__STDHEP_LOCALBASE"] = dirname_clean.replace('/ilc/prod/', '/group/ilc/grid/storm/prod/')
  else:
     conf["__STDHEP_SRC_LFNBASE"] = ""
     conf["__STDHEP_LOCALBASE"] = ""

  params = {}

  params["max_read"] = conf["__SPLIT_MAX_READ"]
  params["nseq_from"] = conf["__SPLIT_NSEQ_FROM"]  # Sequence number of first file.
  params["skip_nevents"] = conf["__SPLIT_SKIP_NEVENTS"] 
  params["stdhep_generator"] = conf["__STDHEP_GENERATOR"]
  params["nw_per_file"] = conf["__SPLIT_NW_PER_FILE"]
  params["nb_files"] = conf["__SPLIT_NB_FILES"]


  conf["__EPOL"] = "undef"
  conf["__PPOL"] = "undef"

  polconv = {"L":"W", "R":"W", "W":"W", "B":"B", "0":"0"}
  polini = ""

  # Format of each line is
  # Filename,[options] options is comma separated list of file dependant key=value 
  # if datadir=="", entries from list_file_of_input contains the full-path to the input files.
  flist = open(list_file_of_input)
  for afile in flist:
    fullpath = afile.replace(" ","").replace("\n","")
    inputs = fullpath.split(',')
    aparams = copy.deepcopy(params)
    if len(inputs) > 1:
       fullpath = inputs[0]
       for opt in inputs[1:]:
          (k,v) = opt.replace(" ","").split("=")
          aparams[k] = v
    dirname = os.path.dirname(fullpath)
    filename = os.path.basename(fullpath)

    print( dirname )
    print( filename )
    # Make sure file exits in the path used by gensplit
    if datadir != "":
        gensplit_path = "/".join([conf["__STDHEP_LOCALBASE"], filename]) 
    elif fullpath.startswith("/ilc/user/"):
        gensplit_path = fullpath.replace("/ilc/user/", "/group/ilc/grid/storm/user/")
    else:
        gensplit_path = fullpath.replace("/ilc/prod/ilc/", "/group/ilc/grid/storm/prod/ilc/")
    print( conf["__STDHEP_LOCALBASE"] )
    print( gensplit_path )
    if not os.path.exists(gensplit_path):
      print( FAIL + "Fatal ERROR" + ENDC + " : wrong input file path " + gensplit_path )
      exit(2)    

    ftoken = decodeFilename(filename)
    procname = ftoken['P']
    (enemachine, dummy) = filename.split('.', 1) 
    # ftoken['E'] = enemachine[1:]  # Use input string as it is
    if "e" in ftoken and "p" in ftoken:
        epol = ftoken['e']
        ppol = ftoken['p']
    else:
        print( pyc.RED + "Pol keywords not found in file. 0 polarization is used." + pyc.END )
        epol="0"
        ppol="0"    

    if conf["__EPOL"] == "undef":
      conf["__EPOL"] = epol
    if conf["__PPOL"] == "undef":
      conf["__PPOL"] = ppol
    
    polnow = "%s%s" % ( polconv[epol], polconv[ppol] )
    if polini == "":
       polini = polnow
    elif polini != polnow:
       print( FAIL + "### ERROR : Invalid beam-pol combination" + ENDC )
       print( "Beam pol type of first file : " + polini )
       print( "Beam pol type of errorneous file : " + polnow +
              " File is " + filename )
       print( FAIL + "### program will terminate by error. " + ENDC )
       exit (-1)

  return conf
  # print "### prepStdhep.py was created."
  
# ###############################################################
def set_dir_and_meta(conf, script="set_dir_and_meta.sh"):
  ''' A script to set directory structure and meta data '''
  (ene, machinepara) = conf["__ENERGY_MACHINEPARA"].split('-')
  stdhep_upload_dir = conf["__STDHEP_UPLOAD_DIR"]
  ( basedir, energyMachinePara, evttype, splitdir ) = stdhep_upload_dir.rsplit('/',3)

  if basedir != conf["__DIRECTORY_STDHEPSPLIT"]:
    print( "## ERROR  : Something is wrong in decoding __STDHEP_UPLOAD_DIR " )
    exit(10)
  tempfile = "/".join([conf["__STDHEP_UPLOAD_DIR"], script]) 

  cmd = ["#!/bin/bash",
         "dirac-dms-add-file "+ tempfile + " " + script + ' ' + 
	      conf["__STDHEP_UPLOAD_SE"], 
         "cat > dirmeta.cli <<EOF ",
         "meta set " + conf["__DIRECTORY_STDHEPSPLIT"] + " Datatype gensplit ",
         "meta set " + "/".join([basedir, energyMachinePara]) + 
               " Energy " + conf["__ENERGY"] + " MachineParams " + machinepara, 
         "meta set " + "/".join([basedir, energyMachinePara, evttype]) +  
               " EvtType " + evttype, 
         "meta set " + "/".join([basedir, energyMachinePara, evttype, splitdir]) +
               " ProdID " + str(conf["__PRODID_FOR_GENSPLIT"]), 
         "EOF", 
         "cat dirmeta.cli | dirac-dms-filecatalog-cli ",
         "dirac-dms-remove-files " + tempfile, 
         "rm -fv dirmeta.cli"
        ]

  fout = open(script, "w")
  fout.write('\n'.join(cmd))
  fout.close()
  os.chmod(script,0755)

  print( '### a script to create directory and se directory meta was created in '+script )

# ################################################################################
if __name__ == '__main__':

  '''
  Following slines are not valid ( 2019-09-19, v03-00 )
  '''

  '''
  splitdir = "split003"
  split_prodid = "500031"
  split_nw_per_file = 100
  pprint.pprint(args)

  evtclass = args["evtclass"]
  splitdir = args["splitdir"]
  split_nw_per_file = args["nw_per_file"]

  doTest = False if doTest not in args else args["doTest"]
  energy="500" if "energy" not in args else args["energy"]
  machinePara="TDR_ws" if "machinePara" not in args else args["machinePara"]
  energyMachinePara='-'.join([energy, machinePara])

  topdir="/ilc/prod/ilc/mc-opt" if "topdir" not in args else args["topdir"]  # Do not include "/" at the end
  split_max_read = 0 if "max_read" not in args else args["max_read"]  # 0 to read up to EOF
  split_selected_file = 1 if "selected_file" not in args else args["selected_file"]
  generator = "whizard-1.95" if "generator" not in args else args["generator"]

  '''


  # Create a file containing basic production parameters 
  # do_test=False

  # conf_default = prepareConfFile(doTest=do_test)

  # params = {"evtclass":"6f_ttbar", "splitdir":"split001", "nw_per_file":200, "energy":"500"}
  # conf_default = prepareConfFile(evtclass="6f_ttbar", splitdir="split001", 
  #                nw_per_file=200, energy="500")


  # list_of_genfiles = "input_genfiles.list"

  # conf = make_stdhepsplit(conf_default, list_of_genfiles)

  # set_dir_and_meta(conf)


  # confjson="production.json"
  # json.dump(conf, open(confjson, "w"))
  # print "### Configuration file is written in "+confjson


