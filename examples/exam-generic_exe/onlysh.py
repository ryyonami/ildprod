from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin
from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC

simfile="LFN:/ilc/prod/ilc/mc-dbd/ild/sim/500-TDR_ws/6f_ttbar/ILD_o1_v05/v01-14-01-p00/sv01-14-01-p00.mILD_o1_v05.E500-TDR_ws.I37623.P6f_bbcyyc.eR.pL-00001.slcio"
setting_file="LFN:/ilc/user/a/amiyamot/software/Settings/marlinSettings-v01-17-09_500.tar.gz"

infile="/ilc/user/a/amiyamot/testjob/rv02-01-sv02-01.mILD_l5_o1_v02_nobg.E1-calib.I1110082.n1.ddst_20200318-134900.slcio"
infile="/ilc/user/a/amiyamot/testjob/sv02-02.mILD_l5_v05.E1-calib.I1110082.n1.dsim_20200910-110948.slcio"


outdst = "toto-7.dst.slcio" #% i
outrec = "toto-7.rec.slcio" #% i

d= DiracILC(True,"repo.rep")

###In case one wants a loop: comment the folowing.
#for i in range(2):
j = UserJob()
j.setJobGroup("TestSimple")
j.setName("BashExample")#%i)
# j.setInputSandbox([setting_file,"mypre.sh", "mypost.sh"])
j.setInputData([infile])
j.setInputSandbox(["mypre.sh", "mypost.sh"])
# j.setDestination(["LCG.DESY.de"])
j.setCPUTime(5)

################################################
appre = GenericApplication()
appre.setScript("/bin/bash")
appre.setArguments("mypre.sh \"This is input\"")
res=j.append(appre)
if not res['OK']:
  print( res['Message'] )
  exit(1)

################################################
#ma = Marlin()
#ma.setDebug()
#ma.setVersion("ILCSoft-01-17-09")
#ma.setSteeringFile("marlin_stdreco.xml")
#ma.setGearFile("GearOutput.xml")
#ma.setInputFile(simfile)
#ma.setOutputDstFile(outdst)
#ma.setOutputRecFile(outrec)

#res = j.append(ma)
#if not res['OK']:
#    print res['Message']
#    exit(1)
  
################################################
appost = GenericApplication()
appost.setScript("/bin/bash")
appost.setArguments("mypost.sh \"This is at the end of job\"")
res=j.append(appost)
if not res['OK']:
  print( res['Message'] )
  exit(1)


# j.setOutputData([outdst,outrec],"myprod2/test","PNNL-SRM")
# j.setOutputSandbox(["*.log","*.xml","*.sh","TaggingEfficiency.root","PfoAnalysis.root"])
j.setOutputSandbox(["*.log","*.xml","*.sh"])
j.dontPromptMe()
#j.submit(d, mode="local")
j.submit(d)

