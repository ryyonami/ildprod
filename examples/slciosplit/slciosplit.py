from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import DDSim
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from ILCDIRAC.Interfaces.API.NewInterface.Applications     import SLCIOSplit
from datetime import datetime


localjob=True
# localjob=False

input_file = "LFN:/ilc/user/a/amiyamot/samples/whizard2_test_sample.slcio"
output_file = "test_slcio_split.slcio"

d= DiracILC(True,"repo.rep")

###In case one wants a loop: comment the folowing.
#for i in range(2):
j = UserJob()
j.setJobGroup("SLCIOSplit_Example")
j.setName("SLCIOSplit")  #%i)
j.setInputSandbox(input_file)
j.setDestination("LCG.KEK.jp")

##Split
nbevtsperfile = 3
split = SLCIOSplit()
split.setInputFile(input_file)
split.setOutputFile(output_file)
split.setNumberOfEventsPerFile(nbevtsperfile)

res = j.append(split)
if not res['OK']:
    print res['Message']
    exit(1)
  
j.setOutputSandbox(["*"])
j.dontPromptMe()

if localjob :
  j.submit(d, mode="local")

else:
  simdir="testjob"
#   j.setOutputData(simfile,simdir,"PNNL-SRM")
  res=j.submit(d)
  if res['OK']:
    print "Dirac job, "+str(res["Value"])+", was submitted."
  else:
    print "Failed to submit dirac job. "
    print res
