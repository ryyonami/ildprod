# A test for multiple-overlay files.


from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin, OverlayInput
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
import os
from datetime import datetime

simdir  = "/ilc/prod/ilc/mc-2020/ild/sim/250-SetA/2f_leptonic_eL_pR/ILD_l5_v02/v02-02/00015166/000"
simfile = "sv02-02.mILD_l5_v02.E250-SetA.I500006.P2f_z_l.eL.pR.n000_001.d_sim_00015166_65.slcio"
simpath = "/".join([simdir, simfile])
ilcsoft_version = "v02-02"
rec_detector = "ILD_l5_o1_v02"

marlin_version = "ILCSoft-02-02-01_cc7"
## Overlay parameters
baseovlpath = "/ilc/prod/ilc/mc-2020/ild/sim/250-SetA"
ovl_version = "v02-02"
ovldata = {"250-SetA":{"BgOverlayWW":{"evttype":"aa_lowpt_WW", "ProdID":15330, "expBG":0.125649543},
                       "BgOverlayWB":{"evttype":"aa_lowpt_WB", "ProdID":15325, "expBG":0.297459204}, 
                       "BgOverlayBW":{"evttype":"aa_lowpt_BW", "ProdID":15324, "expBG":0.29722665}, 
                       "BgOverlayBB":{"evttype":"aa_lowpt_BB", "ProdID":15323, "expBG":0.829787685},
                       "PairBgOverlay":{"evttype":"seeablepairs", "ProdID":15322, "expBG":1.0}}}


#  Get number of events and energy from Dirac catalog
from DIRAC.Resources.Catalog.FileCatalogClient import FileCatalogClient
fc = FileCatalogClient()
res = fc.getFileUserMetadata(simpath)
if not res["OK"]:
    print( "### ERROR : Failed to get meta data of " + srcpath )
    exit(16)

simmeta = res["Value"]
numberOfSignalEvents = int(simmeta["NumberOfEvents"])
energy = simmeta["Energy"]
sim_detectorModel = simmeta["DetectorModel"]
e_key = "-".join([energy, simmeta["MachineParams"]])

print(" nbevents="+str(numberOfSignalEvents)+ " energy="+str(energy))
now = datetime.now()
outdst = "marlin_overlay.v02-02.dst.%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
outrec = "marlin_overlay.v02-02.rec.%s.slcio" % now.strftime("%Y%m%d-%H%M%S")


# ######################################################################3
# Create Marin job
# ######################################################################3

d = DiracILC(True,"repo.rep")

j = UserJob()
j.setJobGroup("Tutorial")
j.setName("MarlinOverlayExample")
j.setInputData(simpath)
j.setILDConfig("v02-02")
j.setOutputSandbox(["*.log","MarlinStdReco.xml","MarlinStdRecoParsed.xml","marlin*.xml", "*.sh"])
# j.setBannedSites(banned_sites)
# j.dontPromptMe()

BXOverlay = 1
for ovl_process in ovldata[e_key].keys():
  print( "### OverlayInput ... "+ovl_process )
  ovl = ovldata[e_key][ovl_process]
  ovlapp = OverlayInput()
  ovlpath = "%s/%s/%s/%s/%8.8d" % \
	( baseovlpath, ovl["evttype"], sim_detectorModel, ovl_version, ovl["ProdID"]) 
  print( "    OverlayPath ... " + ovlpath )
  ovlapp.setMachine("ilc_dbd")
  # ovlapp.setEnergy(energy)
  # ovlapp.setDetectorModel(sim_detectorModel)
  ovlapp.setProcessorName(ovl_process)  
  ovlapp.setBkgEvtType(ovl["evttype"])
  ovlapp.setPathToFiles(ovlpath)
  ovlapp.setGGToHadInt(ovl["expBG"])
  ovlapp.setBXOverlay(BXOverlay)
  ovlapp.setNumberOfSignalEventsPerJob(numberOfSignalEvents)
  res = j.append(ovlapp)
  if not res['OK']:
    print( res['Message'])
    exit(1)


## Marlin step
ma = Marlin()
ma.setDebug(True)
ma.setVersion(marlin_version)
ma.setDetectorModel(rec_detector)
ma.setSteeringFile("MarlinStdReco.xml")
cliargs = " --constant.RunOverlay=true --constant.CMSEnergy=%s" % energy
cliargs += " --constant.DetectorModel=%s " % rec_detector
cliargs += " --global.MaxRecordNumber=10 "

ma.setExtraCLIArguments(cliargs)

ma.setInputFile(simpath)
ma.setOutputDstFile(outdst)
ma.setOutputRecFile(outrec)
j.setOutputData([outdst,outrec],"myprod2/test","KEK-DISK")

res = j.append(ma)
if not res['OK']:
    print( res['Message'] )
    exit(1)
  
# j.submit(d, mode='local')
j.submit(d)
