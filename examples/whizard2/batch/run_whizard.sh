#!/bin/bash
# A script run Whizard jobs
# 

# source ~/work/whizard2/whizard2-gcc49.setup 2.6.4
source /cvmfs/clicdp.cern.ch/software/WHIZARD/2.6.3/x86_64-slc6-gcc7-opt/setup.sh
which whizard
which gfortran
# newpath=`echo $PATH | sed -e "s|/usr/local/texlive/p2011|/group/ilc/soft/utils64/texlive/p2011|"`
# export PATH=${newpath}

# ####################################
# Whizard run of one process
# ####################################

proc="P2f_qqbar"

/usr/bin/time whizard -r ${proc}.sin > run_whizard.log 2>&1  
#       /usr/bin/time whizard -r ${proc}.sin 2>&1 | tee run_whizard.log   
#       /usr/bin/time whizard ${proc}.sin > run_whizard.log 2>&1  

