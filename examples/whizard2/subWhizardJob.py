
from DIRAC.Core.Base import Script
Script.parseCommandLine()

# ######################################
def subWhizard2():

    from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
    from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
    from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin, Whizard2
    
    dIlc = DiracILC()
    
    outputfile="E500.P2f_bB.GWhizard2.I20021.e0.p0.n001.slcio"

    job = UserJob()
    job.setInputSandbox(["pythia6-parameters.sin", "P2f_qqbar.sin"])
    job.setOutputSandbox(['*.log', '*.sh', '*.py', '*.xml'])
    job.setOutputData( [outputfile] )
    job.setJobGroup( "myWhiz2" )
    job.setName( "MyWhizard2" )
    
    # job.setInputData( '/ilc/user/u/username/slcio/input.slcio' )


    whiz = Whizard2()
    whiz.setVersion("2.6.3")
    whiz.setNumberOfEvents(30)
    whiz.setEvtType("P2f_bB")
    whiz.setProcessVariables("P2f_bB")
    # whiz.setRandomSeed(15)
    whiz.setSinFile("P2f_qqbar.sin")
    whiz.setOutputFile(outputfile)
    job.append(whiz)

    #marl = Marlin ()
    #marl.setVersion("ILCSoft-01-17-09")
    
    #marl.setInputFile( ['input.slcio'] )
    #marl.setSteeringFile("clic_ild_cdr_steering.xml")
    #marl.setGearFile("clic_ild_cdr.gear")
    #marl.setNumberOfEvents(3)
    
    # job.append(marl)
    # job.submit(dIlc, mode="local")

    job.submit(dIlc)

# ######################################
if __name__ == "__main__":

    subWhizard2()
