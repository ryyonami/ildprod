#!/usr/bin/env python 
# 
#  Run ddsim and marlin using python 
#

import os
import sys
import subprocess
import datetime
import pprint
import glob
import argparse
import json

def getCPUSize():

  # ======== analize run result ================
#   ddsimtime=open("time-ddsim.log").readlines()[0].split()
#   marlintime=open("time-marlin.log").readlines()[0].split()

#  sim={"elaspsed":ddsimtime[5], "user":ddsimtime[8], "sys":ddsimtime[11]}
#  rec={"elaspsed":marlintime[5], "user":marlintime[8], "sys":marlintime[11]}

  cpusize={}

  for line in open("resource.log"):
    (key, value) = line[:-1].split('=')
    cpusize[key] = value

  for slcio_file in glob.glob("*.slcio"):
    if slcio_file.find("aa_lowpt_-80e-_+30e+_000") < 0 \
	and slcio_file.find("gpigs-filtered-pairs-mod") < 0:
      fsize = os.path.getsize(slcio_file)
      ftype = "sim"
      if slcio_file.find("d_rec") > 0:
        ftype = "rec"
      if slcio_file.find("d_dst") > 0:
        ftype = "dst"

      cpusize[ftype] = fsize
  
  xevt = float(cpusize["numevents"])
  cpusize["sim_cpu_ev_sec"] = ( float(cpusize["ddsim_sys"]) + float(cpusize["ddsim_user"]) )/xevt
  cpusize["rec_cpu_ev_sec"] = ( float(cpusize["marlin_sys"]) + float(cpusize["marlin_user"]) )/xevt
  cpusize["sim_size_ev_MB"] =   float(cpusize["sim"])*1.E-6 / xevt  
  cpusize["rec_size_ev_MB"] =   float(cpusize["rec"])*1.E-6 / xevt  
  cpusize["dst_size_ev_MB"] =   float(cpusize["dst"])*1.E-6 / xevt  
  xintlum = 500.0
  xevtot = xintlum*( float( cpusize["xsect"] ) +1.0 )
  cpusize["sim_cpu_hour"] = cpusize["sim_cpu_ev_sec"] * xevtot / 3600.0
  cpusize["rec_cpu_hour"] = cpusize["rec_cpu_ev_sec"] * xevtot / 3600.0
  cpusize["sim_size_GB"] = cpusize["sim_size_ev_MB"] * xevtot * 1.E-3
  cpusize["rec_size_GB"] = cpusize["rec_size_ev_MB"] * xevtot * 1.E-3
  cpusize["dst_size_GB"] = cpusize["dst_size_ev_MB"] * xevtot * 1.E-3
  cpusize["total_#events"] = int(xevtot)

  return cpusize


if __name__ == "__main__":

  wdir=os.getcwd()
  # energy = "500"
  allinfo = {}
  infotype = {}
  alldata = {}
  alldata["sim_cpu_days"] = 0
  alldata["rec_cpu_days"] = 0
  alldata["sim_size_GB"] = 0
  alldata["rec_size_GB"] = 0
  alldata["dst_size_GB"] = 0
  alldata["total_#events"] = 0
  for jdir in glob.glob("jobs-*"):
    os.chdir(jdir)
    print >>sys.stderr, "###"+jdir
    for ptype in glob.glob("*/*/I*"):
      (ene, prtype, subdir) = ptype.split('/')
      # os.chdir(ptype)
      # (ene, prtype) = ptype.split('/')
      os.chdir(ene+"/"+prtype)
      (energy, machine ) = ene.split('-')
      print >> sys.stderr, "###"+ptype
      atype = {}
      atype["sim_cpu_days"] = 0
      atype["rec_cpu_days"] = 0
      atype["sim_size_GB"] = 0
      atype["rec_size_GB"] = 0
      atype["dst_size_GB"] = 0
      atype["total_#events"] = 0

      for pid in glob.glob("I*"):
        os.chdir(pid)
        if os.path.exists("resource.log"):
          print >> sys.stderr, pid
          arec = getCPUSize()
          arec["process_type"] = prtype
          arec["process_id"] = pid
          arec["emergy"] = energy
          arec["machinePara"] = machine
          atype["sim_cpu_days"] += arec["sim_cpu_hour"]/24.0 
          atype["rec_cpu_days"] += arec["rec_cpu_hour"]/24.0 
          atype["sim_size_GB"] += arec["sim_size_GB"] 
          atype["rec_size_GB"] += arec["rec_size_GB"] 
          atype["dst_size_GB"] += arec["dst_size_GB"] 
          atype["total_#events"] += arec["total_#events"]

          allinfo[pid] = arec
          atype[pid] = arec
        
        os.chdir('..')

      alldata[prtype] = atype
      alldata["sim_cpu_days"] += atype["sim_cpu_days"]
      alldata["rec_cpu_days"] += atype["rec_cpu_days"]
      alldata["sim_size_GB"] += atype["sim_size_GB"] 
      alldata["rec_size_GB"] += atype["rec_size_GB"] 
      alldata["dst_size_GB"] += atype["dst_size_GB"] 
      alldata["total_#events"] += atype["total_#events"]
      os.chdir('../..')
    os.chdir(wdir)
  alldata["all"] = allinfo

  json.dump(alldata, open("cpusize.json","w"))
      
