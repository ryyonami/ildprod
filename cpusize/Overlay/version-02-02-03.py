# ILCSoft directory and version
SOFTDIR="/cvmfs/ilc.desy.de/sw/x86_64_gcc82_centos7"
SOFT_VERSION="v02-02-03"

# ILDConfig directory and version
ILDCONFIG_DIR="/cvmfs/ilc.desy.de/sw/ILDConfig"
ILDCONFIG_SIM="v02-02-03"
ILDCONFIG_REC="v02-02-03"
ILDCONFIG_FOR_OVERLAY="v02-02-03"

# ILDCONFIG_DIR="/sw/ilc/ilcsoft/gcc493/ILDConfig"
# ILDCONFIG_SIM="head"
# ILDCONFIG_REC="head"

# A script to setup python environment in batch environment.
INIT_PYTHON="~/bin/init_python27.sh"

# Genmeta file. Now obtained from GENMETA_JSON in runsimrec.py

# Files from ILDConfig
HighLevelReco="/".join([ILDCONFIG_DIR, ILDCONFIG_REC, "StandardConfig/production/HighLevelReco"])
PandoraSettings="/".join([ILDCONFIG_DIR, ILDCONFIG_REC, "StandardConfig/production/PandoraSettings"])

# 

