# ILCSoft directory and version
SOFTDIR="/cvmfs/ilc.desy.de/sw/x86_64_gcc49_sl6"
# SOFT_VERSION="v01-19-05"
SOFT_VERSION="v02-00-01"

# SOFTDIR="/cvmfs/clicdp.cern.ch/iLCSoft/builds/2018-03-02"
# SOFT_VERSION="x86_64-slc6-gcc62-opt"

# ILDConfig directory and version
ILDCONFIG_DIR="/cvmfs/ilc.desy.de/sw/ILDConfig"
# ILDCONFIG_SIM="v01-19-05-p01"
# ILDCONFIG_REC="v01-19-05-p01"
ILDCONFIG_SIM="v02-00-01"
ILDCONFIG_REC="v02-00-01"
ILDCONFIG_FOR_OVERLAY="v02-00-01"

# ILDCONFIG_DIR="/sw/ilc/ilcsoft/gcc493/ILDConfig"
# ILDCONFIG_SIM="head"
# ILDCONFIG_REC="head"

# A script to setup python environment in batch environment.
INIT_PYTHON="~/bin/init_python27.sh"

# Genmeta file. Now obtained from GENMETA_JSON in runsimrec.py

# Files from ILDConfig
HighLevelReco="/".join([ILDCONFIG_DIR, ILDCONFIG_REC, "StandardConfig/production/HighLevelReco"])
PandoraSettings="/".join([ILDCONFIG_DIR, ILDCONFIG_REC, "StandardConfig/production/PandoraSettings"])

