#!/bin/env python 

import os
import json
import glob

# newidfile="newids.txt"

if "GENMETA_JSON" not in os.environ:
    print("FATAL ERROR: GENMETA_JSON environment parameter is not defined.")
    exit(0)
# genmetaByID="/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta/genmetaByID.json"
genmetaByID=os.environ["GENMETA_JSON"]

genmeta = json.load(open(genmetaByID))



if __name__ == "__main__":
  
    jsonout = {}
    for newidfile in glob.glob("newids-*.txt"):
      print "Reading from " + newidfile
      for line in open(newidfile):
        pid=line[1:].replace("\n","")
        if pid not in genmeta:
            print "pid %s not in genmeta file." % pid
            continue
        ameta = genmeta[pid]
        # print "process_name="+ameta["process_names"]
        nev = ameta["total_number_of_events"]
        if "s_" in ameta["process_names"]:
            if "_pm13_p" in ameta["process_names"] and "_prnd" not in ameta["process_names"] :
                nev = 1000
            elif "s_pm" in ameta["process_names"]:
                nev = 100000
            else:
               for pid in ["22", "111", "130", "310"] :
                  if "s_"+pid+"_p" in ameta["process_names"]:
                     nev = 20000
        if "flavortag" in ameta["process_type"] and line[0:3] == "I41":
            nev = 500000
        xsect = ameta["cross_section_in_fb"]
        proctype = ameta["process_type"]
        fileurl = ameta["fileurl"]
        if "higgs" in fileurl:
            proctype = "higgs"

        jsonout[line[:-1]] = { "nev":nev, "prtype":proctype, "xsect":xsect, "process_names":ameta["process_names"]}

    json.dump(jsonout, open("nevlist-new.json","w"))
    print "Nevlist from newids-*.txt is dumped to nevlist-new.json"

    # Merge to json file.
    nevlist_json = json.load(open("nevlist-dbdall.json"))
    nevlist_json.update(jsonout)
    json.dump(nevlist_json, open("nevlist.json","w"))
    print "nevlist-new.json and nevlist-dbd-all.json are merged and written to nevlist.json"



