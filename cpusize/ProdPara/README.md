## ProdPara

This directory is for a tool to decide production parameters, like 
process grouping, number of events per job, etc.

## Step 1

Create `cpusize.json`, by running job and a script, `cpusize.py` in `../Overlay` directory.

## Step 2

In order to produced all generated files defined in genmetaByID.json, `nevlist.json` 
can be produced by 

- Create a list of process IDs in a file named `newids-*.txt`. `*` can be any string.
- Then, run `./nevlist-from-newids.py`. A file `nevlist.json` is created.

In another case, read comments of README.md.

<!--
Create a file, `nevgen/nevlist-dbdall.json`. The key is a process_id. 
For each process, defines `nev` ( number of events to produce ), 
`prtype` ( production class, which is a word after `../Overlay/jobs-*`, 
and cross section of each process.

In the case of calibration jobs and backround files, 
a file `nevlist-new.json` was created using `nevlist-from-newids.py`.
`nevlist-from-newids.py` read a list of process_id files and decide 
number of events to produce depending on the file name.

- `newids-full.txt` : All events available will be produced, reading the 
generator meta information in `genmetaByID.json`.
- `newids-uds.txt`, `newids-single.txt` : Treated similarly as `newids-full.txt`
- `newids-*.txt` : Any files matched is treated as newids-full.txt. 

Then, re-create `nevlist-dbdall.json` by adding `nevlist-new.json` 
to the original file in `/group/ilc/users/miyamoto/mcprod/170921-cpusize/Result1-summary/nevlist-dbdall.json` 
A command to merge 2 json files is, 

```
	jsonmerge file1 file2 merged-file
```
-->

## Step 3 : Re-create `prodpara.xlsx`

`prodpara.py` does re-create a file, `prodpara.xlsx`.

All process IDs present in `cpusize.json` are eligible for production
except those found in a file, `omit_process_id.txt`.
If a process_id is in `cpusize.json`, but not found in `nevgen/nevlist-dbdall.json`, 
an error message is printed and it is not included in production.

## Command examples

- Create a `prodpara.xlsx` file for the production of 4f and 6f processes at 500 GeV.    
```
./prodpara.py \
  --nevgen_json /group/ilc/users/miyamoto/mcprod/180523-cpusize/cpusize/ProdPara/nevgen/nevlist.json \
  --cpusize_json /group/ilc/users/miyamoto/mcprod/180523-cpusize/cpusize/Overlay/cpusize.json \
  -f prodpara.xlsx  4f,6f
  
./prodpara.py --nevgen_json nevgen/nevlist.json --cpusize_json ../Overlay/cpusize.json -s 1300.0 -f SM-podpara.xlsx 3f,4f,aa_4f

```



